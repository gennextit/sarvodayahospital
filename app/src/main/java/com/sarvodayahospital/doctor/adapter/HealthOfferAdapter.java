package com.sarvodayahospital.doctor.adapter;

/**
 * Created by Abhijit on 29-Sep-16.
 */

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.HealthOffer;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import okhttp3.HttpUrl;

public class HealthOfferAdapter extends ArrayAdapter<HealthOffer> {
    private List<HealthOffer> list;

    private Activity context;
//	public ImageLoader imageLoader;

    public HealthOfferAdapter(Activity context, int textViewResourceId, List<HealthOffer> list) {
        super(context, textViewResourceId, list);
        this.context = context;
        this.list = list;
//		imageLoader = new ImageLoader(context.getApplicationContext());
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    class ViewHolder {
        private final ProgressBar pbImage;
        // TextView tvName;
        ImageView ivImage;

        public ViewHolder(View v) {
            // tvName = (TextView)
            // v.findViewById(R.id.tv_slot_profile_selector_name);
            pbImage = (ProgressBar) v.findViewById(R.id.progressBar);
            ivImage = (ImageView) v.findViewById(R.id.iv_slot_profile_selector);

        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if (v == null) {
            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.slot_gallery_view, parent, false);
            holder = new ViewHolder(v);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        String imgUrl = list.get(position).getUrl();
        if (!TextUtils.isEmpty(imgUrl)) {
            HttpUrl parsed = HttpUrl.parse(imgUrl);
            imgUrl = parsed.toString();
            if (!imgUrl.equals("")) {
                final ViewHolder finalHolder = holder;
                Picasso.get().load(imgUrl)
                        .placeholder(R.drawable.ic_gallary_bg)
                        .error(R.drawable.ic_gallary_bg)
                        .into(holder.ivImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                finalHolder.pbImage.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                finalHolder.pbImage.setVisibility(View.GONE);
                            }
                        });
            }
        }
        return v;
    }

}
