package com.sarvodayahospital.doctor.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sarvodaya.patient.R;
import com.sarvodaya.patient.healthCheckup.PackageDetailsActivity;
import com.sarvodayahospital.beans.HealthPackage;

import java.util.List;

public class HealthPackageAdapter extends ArrayAdapter<HealthPackage>{

	List<HealthPackage> packageList;
	private Activity activity;
    private LayoutInflater inflater;
    int resource;
    
	public HealthPackageAdapter(Activity activity, int resource, List<HealthPackage> packageList) {
    	super(activity, resource, packageList);
    	this.activity = activity;
    	this.resource = resource;
    	this.packageList = packageList;		
    }
	
	public class ViewHolder {
		ImageView pic;
		TextView name;
		public LinearLayout slot;
	}
	
	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		  if (inflater == null)
	            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		  if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(resource, parent, false);
			holder.slot = (LinearLayout) view.findViewById(R.id.ll_slot);
			holder.pic = (ImageView) view.findViewById(R.id.pic);
			holder.name = (TextView) view.findViewById(R.id.name);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		final HealthPackage item = packageList.get(position);
		
		holder.pic.setImageResource(R.drawable.health_care_image);
		holder.name.setText(item.getPackageName());
		holder.slot.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startPackageDetailActivity(holder.pic,holder.name,item.getPackageID(),item.getPackageName());
			}
		});
		return view;
	}

	private void startPackageDetailActivity(ImageView ivPic, TextView tvTitle, String packageID, String packageName) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Intent intent1 = new Intent(activity, PackageDetailsActivity.class);
			intent1.putExtra("package_id", packageID);
			intent1.putExtra("package_name", packageName);
			Pair pImage = Pair.create(ivPic, activity.getResources().getString(R.string.transition_image));
			Pair pText = Pair.create(tvTitle, activity.getResources().getString(R.string.transition_title));
			ActivityOptions options = ActivityOptions
					.makeSceneTransitionAnimation(activity,pImage,pText);
			activity.startActivity(intent1, options.toBundle());
			return;
		}
		Intent intent1 = new Intent(activity, PackageDetailsActivity.class);
		intent1.putExtra("package_id", packageID);
		intent1.putExtra("package_name", packageName);
		activity.startActivity(intent1);
	}

}
