package com.sarvodayahospital.doctor.adapter;


import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarvodaya.patient.R;
import com.sarvodaya.patient.appointment.BookAppointmentActivity;
import com.sarvodayahospital.beans.TimeAvailable;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.Const;

import java.util.List;

public class SelectAppointmentAdapter extends BaseAdapter {
	private final ImageView profile_pic;
	private Context mContext;
	private List<TimeAvailable> appointmentSlot;
	private View previousView;
	private String day;
	private Activity activity;
	private ConnectionDetector cd;
	// flag for Internet connection status
	private Boolean isInternetPresent = false;
	// Alert Dialog Manager
	private AlertDialogManager alert = new AlertDialogManager();
	private final String doctor_id, doctor_name,doctor_dept,doctor_pic;

	public SelectAppointmentAdapter(Context c, List<TimeAvailable> appointmentSlot, String doctor_id
			, String doctor_name, String doctor_dept, String doctor_pic
			, String day, Activity activity, ImageView profile_pic) {
		mContext = c;
		this.appointmentSlot = appointmentSlot;
		this.doctor_id = doctor_id;
		this.doctor_name = doctor_name;
		this.doctor_dept = doctor_dept;
		this.doctor_pic = doctor_pic;
		this.day = day;
		this.profile_pic=profile_pic;
		this.activity = activity;
		// Check if Internet present
		cd = new ConnectionDetector(c);
	}
	
	public int getCount() {
		return appointmentSlot.size();
	}

	public Object getItem(int position) {
		return appointmentSlot.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}
	
	public class ViewHolder {
		public TextView startDate;
		public TextView endDate;
	}
	
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		final ViewHolder viewHolder;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater vi = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.grid_select_appointment, null);
			viewHolder = new ViewHolder();
			viewHolder.startDate = (TextView) v.findViewById(R.id.start_date);
			viewHolder.endDate = (TextView) v.findViewById(R.id.end_date);
			v.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) v.getTag();
		}
		
		viewHolder.startDate.setText(appointmentSlot.get(position).getStartTime());
		viewHolder.endDate.setText(appointmentSlot.get(position).getEndTime());
		if(appointmentSlot.get(position).getStatus().contentEquals("Available")) {
			v.setBackgroundResource(R.drawable.circle_day_green);
		} else if(appointmentSlot.get(position).getStatus().contentEquals("Unavailable")) {
			v.setBackgroundResource(R.drawable.circle_day_grey);
		} else if(appointmentSlot.get(position).getStatus().contentEquals("Booked")) {
			v.setBackgroundResource(R.drawable.circle_day_pink);
		}
		
		v.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				if(appointmentSlot.get(position).getStatus().contentEquals("Available")) {
					isInternetPresent = cd.isConnectingToInternet();
					if (!isInternetPresent) {
						// Internet Connection is not present
						alert.showAlertDialog(activity, "Internet Connection Error",
								"Please connect to working Internet connection", false);
						// stop executing code by return
						return;
					}
					
					setSelected(arg0, "Green");
					startBookAppointmentActivity(doctor_id,doctor_name,doctor_dept,doctor_pic,appointmentSlot.get(position).getStartTime(),day);

				} 
			}
		});
		return v;
	}

	private void startBookAppointmentActivity(String doctorId,String drName,String drDept,String drPic, String startTime, String day) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Intent intent = new Intent(activity, BookAppointmentActivity.class);
			intent.putExtra(Const.DOCTOR_ID, doctorId);
			intent.putExtra(Const.DOCTOR_NAME, drName);
			intent.putExtra(Const.DOCTOR_DEPARTMENT, drDept);
			intent.putExtra(Const.DOCTOR_PIC, drPic);
			intent.putExtra("start_time", startTime);
			intent.putExtra("day", day);
			ActivityOptions options = ActivityOptions
					.makeSceneTransitionAnimation(activity, profile_pic,activity.getResources().getString(R.string.transition_profile));
			activity.startActivity(intent, options.toBundle());
			return;
		}
		Intent intent = new Intent(activity, BookAppointmentActivity.class);
		intent.putExtra(Const.DOCTOR_ID, doctorId);
		intent.putExtra(Const.DOCTOR_NAME, drName);
		intent.putExtra(Const.DOCTOR_DEPARTMENT, drDept);
		intent.putExtra(Const.DOCTOR_PIC, drPic);
		intent.putExtra("start_time", startTime);
		intent.putExtra("day", day);
		activity.startActivity(intent);
	}

	public View setSelected(View view, String color) {
		if (previousView != null) {
			previousView.setBackgroundResource(R.drawable.list_select_appointment_background);
		}
		previousView = view;
		view.setBackgroundResource(R.drawable.circle_day_orange);
		return view;
	}
}
