package com.sarvodayahospital.doctor.adapter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sarvodaya.patient.R;
import com.sarvodaya.patient.appointment.DateSelectionActivity;
import com.sarvodaya.patient.appointment.DoctorProfileActivity;
import com.sarvodayahospital.beans.Doctor;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.CircleTransform;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.Const;
import com.sarvodayahospital.util.RoundImage;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AllDoctorAdapter extends ArrayAdapter<Doctor>{
	private Activity activity;
    private LayoutInflater inflater;
    private List<Doctor> doctorList;
    int resource;
    RoundImage roundedImage;
    ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
    public AllDoctorAdapter(Activity activity, int resource, List<Doctor> doctorList) {
    	super(activity, resource, doctorList);
    	this.activity = activity;
    	this.resource = resource;
    	this.doctorList = doctorList;
    	// Check if Internet present
		cd = new ConnectionDetector(activity.getApplicationContext());
				
    }
 
    public class ViewHolder {
    	ImageView pic;
		TextView name;
		TextView department;
		TextView location;
		ImageView calendar;
		public LinearLayout slot;
	}

	

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		  if (inflater == null)
	            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		  if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(resource, parent, false);
			holder.slot = (LinearLayout) view.findViewById(R.id.ll_slot);
			holder.pic = (ImageView) view.findViewById(R.id.pic);
			holder.name = (TextView) view.findViewById(R.id.name);
			holder.department = (TextView) view.findViewById(R.id.department);
			holder.location = (TextView) view.findViewById(R.id.location);
			holder.calendar = (ImageView) view.findViewById(R.id.calendar);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		
		if(doctorList.get(position).getPicId() != null) {
			Picasso.get().load(doctorList.get(position).getPicId()).transform(new CircleTransform())
					.placeholder(R.mipmap.all_doctors_pic)
					.error(R.mipmap.all_doctors_pic)
					.into(holder.pic);
	    } else {
			holder.pic.setImageResource(R.mipmap.all_doctors_pic);
		}
		
		
		holder.name.setText(doctorList.get(position).getDoctorName());
		holder.department.setText(doctorList.get(position).getDepartment().replace("&amp;", "&"));
		holder.location.setText(doctorList.get(position).getLocation());
		holder.calendar.setImageResource(R.drawable.all_doctors_calendar);
		// Listen for ListView Item Click
		holder.calendar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				isInternetPresent = cd.isConnectingToInternet();
			    if (!isInternetPresent) {
					// Internet Connection is not present
					alert.showAlertDialog(activity, "Internet Connection Error",
							"Please connect to working Internet connection", false);
					// stop executing code by return
					return;
				}
				String picId=doctorList.get(position).getPicId();
				if(picId==null){
					picId="";
				}
				startCalenderActivity(holder.pic,doctorList.get(position).getDoctorId(),
                        doctorList.get(position).getDoctorName(),
						doctorList.get(position).getDepartment(),
						picId);


			}
		});
		holder.slot.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				isInternetPresent = cd.isConnectingToInternet();
				if (!isInternetPresent) {
					// Internet Connection is not present
					alert.showAlertDialog(activity, "Internet Connection Error",
							"Please connect to working Internet connection", false);
					// stop executing code by return
					return;
				}
				String picId=doctorList.get(position).getPicId();
				if(picId==null){
					picId="";
				}
				startProfileActivity(holder.pic,doctorList.get(position).getDoctorId(),doctorList.get(position).getDoctorName(),
						picId);
			}
		});
		return view;
	}

	private void startCalenderActivity(ImageView pic, String doctorId,String doctorName,String department,String doctorPic) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Intent intent1 = new Intent(activity, DateSelectionActivity.class);
			intent1.putExtra(Const.DOCTOR_ID, doctorId);
			intent1.putExtra(Const.DOCTOR_NAME, doctorName);
			intent1.putExtra(Const.DOCTOR_DEPARTMENT, department);
			intent1.putExtra(Const.DOCTOR_PIC, doctorPic);
			ActivityOptions options = ActivityOptions
					.makeSceneTransitionAnimation(activity, pic,activity.getResources().getString(R.string.transition_profile));
			activity.startActivity(intent1, options.toBundle());
			return;
		}
		Intent intent = new Intent(activity, DateSelectionActivity.class);
		intent.putExtra(Const.DOCTOR_ID, doctorId);
		intent.putExtra(Const.DOCTOR_NAME, doctorName);
        intent.putExtra(Const.DOCTOR_DEPARTMENT, department);
        intent.putExtra(Const.DOCTOR_PIC, doctorPic);
		activity.startActivity(intent);
	}

	private void startProfileActivity(ImageView ivShopIcon,String doctorId,String doctorName,String doctorPic) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Intent intent1 = new Intent(activity, DoctorProfileActivity.class);
			intent1.putExtra(Const.DOCTOR_ID, doctorId);
			intent1.putExtra(Const.DOCTOR_NAME, doctorName);
			intent1.putExtra(Const.DOCTOR_PIC, doctorPic);
			ActivityOptions options = ActivityOptions
					.makeSceneTransitionAnimation(activity, ivShopIcon,activity.getResources().getString(R.string.transition_profile));
			activity.startActivity(intent1, options.toBundle());
			return;
		}
		Intent intent1 = new Intent(activity, DoctorProfileActivity.class);
		intent1.putExtra(Const.DOCTOR_ID, doctorId);
		intent1.putExtra(Const.DOCTOR_NAME, doctorName);
		intent1.putExtra(Const.DOCTOR_PIC, doctorPic);
		activity.startActivity(intent1);
	}
}
