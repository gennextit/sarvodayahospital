package com.sarvodayahospital.doctor.adapter;

import java.util.List;

import com.sarvodaya.patient.nav.GoogleMapActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.HospitalLocation;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ConnectionDetector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GetDirectionsAdapter extends ArrayAdapter<HospitalLocation>{
	List<HospitalLocation> locationList;
	private Activity activity;
    private LayoutInflater inflater;
    int resource;
    ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
	public GetDirectionsAdapter(Activity activity, int resource, List<HospitalLocation> locationList) {
    	super(activity, resource, locationList);
    	this.activity = activity;
    	this.resource = resource;
    	this.locationList = locationList;		
    	// Check if Internet present
    	cd = new ConnectionDetector(activity.getApplicationContext());
    }
	
	public class ViewHolder {
    	TextView name;
		TextView location;
		ImageView direction;
	}
	
	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		  if (inflater == null)
	            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		  if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(resource, parent, false);
			holder.name = (TextView) view.findViewById(R.id.name);
			holder.location = (TextView) view.findViewById(R.id.location);
			holder.direction = (ImageView) view.findViewById(R.id.direction);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		
		
		
		holder.name.setText(locationList.get(position).getName());
		holder.location.setText(locationList.get(position).getLocation());
		holder.direction.setImageResource(R.drawable.direction_arrow);
		// Listen for ListView Item Click
		holder.direction.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				isInternetPresent = cd.isConnectingToInternet();
			    if (!isInternetPresent) {
					// Internet Connection is not present
					alert.showAlertDialog(activity, "Internet Connection Error",
							"Please connect to working Internet connection", false);
					// stop executing code by return
					return;
				}
				Intent intent = new Intent(activity, GoogleMapActivity.class);
				intent.putExtra("name", locationList.get(position).getName());
            	intent.putExtra("location", locationList.get(position).getLocation());
            	intent.putExtra("latitude", locationList.get(position).getLalitute());
            	intent.putExtra("longitude", locationList.get(position).getLongitude());
				activity.startActivity(intent);
			}
		});
		return view;
	}
}
