package com.sarvodayahospital.doctor.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.FAQ;


public class FaqAdapter extends ArrayAdapter<FAQ>{

	List<FAQ> faqList;
	private Activity activity;
    private LayoutInflater inflater;
    int resource;
    
	public FaqAdapter(Activity activity, int resource, List<FAQ> faqList) {
    	super(activity, resource, faqList);
    	this.activity = activity;
    	this.resource = resource;
    	this.faqList = faqList;		
    }
	
	public class ViewHolder {
		TextView question;
		TextView answer;
	}
	
	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		final ViewHolder holder;
		  if (inflater == null)
	            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		  if (view == null) {
			holder = new ViewHolder();
			view = inflater.inflate(resource, parent, false);
			holder.question = (TextView) view.findViewById(R.id.question);
			holder.answer = (TextView) view.findViewById(R.id.answer);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		
		
		holder.question.setText(faqList.get(position).getQuestion());
		String answer = faqList.get(position).getAnswer().replace("&#x00b4;", "'");
		answer = answer.replace("&#x2018;", "\"");
		answer = answer.replace("&#x2019;", "\"");
		holder.answer.setText(answer);
		return view;
	}

}
