package com.sarvodayahospital.beans;

import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class HealthTips {
	@SerializedName("tip")
	public String tip;
	
	@SerializedName("detail")
	public String detail;

	@SerializedName("image_url")
	public String image_url;

	
	
	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	public static Type getJsonArrayType() {
		return new TypeToken<Collection<HealthTips>>() {
		}.getType();
	}

}
