package com.sarvodayahospital.beans;

public class HospitalLocation {

	private String name;
	private String location;
	private Double lalitute;
	private Double longitude;
	
	public HospitalLocation(String name, String location, Double latitude, Double longitude) {
		this.name = name;
		this.location = location;
		this.lalitute = latitude;
		this.longitude = longitude;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getLalitute() {
		return lalitute;
	}
	public void setLalitute(Double lalitute) {
		this.lalitute = lalitute;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
}
