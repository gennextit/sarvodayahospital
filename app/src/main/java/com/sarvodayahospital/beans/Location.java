package com.sarvodayahospital.beans;

import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class Location {

	@SerializedName("Country")
	public String country;
	
	@SerializedName("City")
	public String city;
	
	@SerializedName("Location")
	public String location;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public static Type getJsonArrayType() {
		return new TypeToken<Collection<Location>>() {
		}.getType();
	}
}
