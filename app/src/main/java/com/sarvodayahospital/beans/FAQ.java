package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class FAQ implements Serializable {

	@SerializedName("question")
	public String question;
	
	@SerializedName("answer")
	public String answer;

	public FAQ(String question, String answer) {
		this.question = question;
		this.answer = answer;
	}
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	public static Type getJsonArrayType() {
		return new TypeToken<Collection<FAQ>>() {
		}.getType();
	}

}
