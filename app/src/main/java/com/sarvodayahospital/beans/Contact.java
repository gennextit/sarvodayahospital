package com.sarvodayahospital.beans;

public class Contact {

	//private variables
    int _id;
    String name;
    String phone_number;
    
	String patient_id;
    String title;
    String address;
    String age;
    String country;
    String city;
    String location;
    String gender;
    // Empty constructor
    public Contact(){
         
    }
    // constructor
    public Contact(int id, String name, String phone_number, String patient_id){
        this._id = id;
        this.name = name;
        this.phone_number = phone_number;
        this.patient_id = patient_id;
    }
     
    // constructor
    public Contact(String name, String phone_number, String patient_id, String title, String address, String age, String country, String city, String location, String gender){
        this.name = name;
        this.phone_number = phone_number;
        this.patient_id = patient_id;
        this.title = title;
        this.address = address;
        this.age = age;
        this.country = country;
        this.city = city;
        this.location = location;
        this.gender = gender;
    }
    // getting ID
    public int getID(){
        return this._id;
    }
     
    // setting id
    public void setID(int id){
        this._id = id;
    }
     
    // getting name
    public String getName(){
        return this.name;
    }
     
    // setting name
    public void setName(String name){
        this.name = name;
    }
     
    // getting phone number
    public String getPhoneNumber(){
        return this.phone_number;
    }
     
    // setting phone number
    public void setPhoneNumber(String phone_number){
        this.phone_number = phone_number;
    }
    
	public String getPatientId() {
		return patient_id;
	}
	public void setPatientId(String patient_id) {
		this.patient_id = patient_id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
    
}
