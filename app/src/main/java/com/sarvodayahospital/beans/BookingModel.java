package com.sarvodayahospital.beans;

/**
 * Created by Abhijit-PC on 30-Mar-17.
 */

public class BookingModel {

    private String output;
    private String outputMsg;
    private DoctorDetailModel drDetail;


    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMsg() {
        return outputMsg;
    }

    public void setOutputMsg(String outputMsg) {
        this.outputMsg = outputMsg;
    }
}
