package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class AppointmentByDoctor implements Serializable{

	@SerializedName("App_ID")
	public String appointmentId;
	
	@SerializedName("Appointment_Date")
	public String appointmentDate;
	
	@SerializedName("Appointment_Time")
	public String appointmentTime;

	public String getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getAppointmentTime() {
		return appointmentTime;
	}

	public void setAppointmentTime(String appointmentTime) {
		this.appointmentTime = appointmentTime;
	}
	
	public static Type getJsonArrayType() {
		return new TypeToken<Collection<AppointmentByDoctor>>() {
		}.getType();
	}
}
