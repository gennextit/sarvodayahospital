package com.sarvodayahospital.beans;

public class TimeAvailable {

	public String startTime;
	public String endTime;
	public String status;
	
	public TimeAvailable(String startTime, String endTime, String status) {
		this.status = status;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
