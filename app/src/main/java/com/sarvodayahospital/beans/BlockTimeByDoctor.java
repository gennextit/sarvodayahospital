package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class BlockTimeByDoctor implements Serializable {
	@SerializedName("Doctor_ID")
	public String doctorId;
	
	@SerializedName("DoctorName")
	public String doctorName;
	
	@SerializedName("Date")
	public String blockDate;
	
	@SerializedName("From_Time")
	public String fromTime;
	
	@SerializedName("To_Time")
	public String toTime;

	public String getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getBlockDate() {
		return blockDate;
	}

	public void setBlockDate(String blockDate) {
		this.blockDate = blockDate;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	

	public static Type getJsonArrayType() {
		return new TypeToken<Collection<BlockTimeByDoctor>>() {
		}.getType();
	}

}
