package com.sarvodayahospital.beans;

/**
 * Created by Admin on 11/21/2017.
 */

public class NotificationModel {

    private boolean isUpdate;
    private String showPopup;
    private String title;
    private String description;
    private String imageUrl;
    private String startDate;
    private String endDate;
    private String sosImageUrl;
    private String sosNumber;
    private String hpNumber;

    public String getHpNumber() {
        return hpNumber;
    }

    public void setHpNumber(String hpNumber) {
        this.hpNumber = hpNumber;
    }

    public String getSosNumber() {
        return sosNumber;
    }

    public void setSosNumber(String sosNumber) {
        this.sosNumber = sosNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSosImageUrl() {
        return sosImageUrl;
    }

    public void setSosImageUrl(String sosImageUrl) {
        this.sosImageUrl = sosImageUrl;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean update) {
        isUpdate = update;
    }

    public String getShowPopup() {
        return showPopup;
    }

    public void setShowPopup(String showPopup) {
        this.showPopup = showPopup;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
