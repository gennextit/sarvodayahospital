package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class OTP implements Serializable{

	@SerializedName("OTP")
	public String otp;

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public static Type getJsonArrayType() {
		return new TypeToken<Collection<OTP>>() {
		}.getType();
	}

}
