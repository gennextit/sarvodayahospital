package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class HealthPackage implements Serializable {

	@SerializedName("PackageID")
	public String packageID;
	
	@SerializedName("PackageName")
	public String packageName;
	
	@SerializedName("Rate")
	public String rate;
	
	@SerializedName("PkgService")
	public String pkgService;

	@SerializedName("ItemType")
	public String itemType;


	public String getPackageID() {
		return packageID;
	}

	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getPkgService() {
		return pkgService;
	}

	public void setPkgService(String pkgService) {
		this.pkgService = pkgService;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public static Type getJsonArrayType() {
		return new TypeToken<Collection<HealthPackage>>() {
		}.getType();
	}
}
