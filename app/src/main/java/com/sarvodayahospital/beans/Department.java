package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class Department implements Serializable {

	@SerializedName("Department")
	public String department;

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	public static Type getJsonArrayType() {
		return new TypeToken<Collection<Department>>() {
		}.getType();
	}

}
