package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class Slot implements Serializable{

	@SerializedName("slot_id")
	public String slotId;

	@SerializedName("StartTime")
	public String startTime;
	
	@SerializedName("EndTime")
	public String endTime;
	
	@SerializedName("status")
	public String status;
	
	public Slot(String slotId, String startTime, String endTime, String status) {
		this.slotId = slotId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.status = status;
	}

	public String getSlotId() {
		return slotId;
	}

	public void setSlotId(String slotId) {
		this.slotId = slotId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static Type getJsonArrayType() {
		return new TypeToken<Collection<Slot>>() {
		}.getType();
	}

}
