package com.sarvodayahospital.beans;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sarvodaya.patient.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import okhttp3.HttpUrl;


public class HealthOfferNewAdapter extends RecyclerView.Adapter<HealthOfferNewAdapter.ViewHolder> {
    private Activity activity;
    private List<HealthOffer> list;

    public HealthOfferNewAdapter(Activity activity, List<HealthOffer> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.slot_gallery_view, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String imgUrl=list.get(position).getUrl();
        if(!TextUtils.isEmpty(imgUrl)) {
//		Picasso.with(activity).load(.replaceall(" ", "%20")).into(holder.pic);
//			try {
            HttpUrl parsed = HttpUrl.parse(imgUrl);
//				URL url = new URL(imgUrl);
            imgUrl = parsed.toString();
//			} catch (MalformedURLException e) {
//				imgUrl = "";
//			}
            if (!imgUrl.equals("")) {
                Picasso.get().load(imgUrl).into(holder.ivImage);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivImage;
        LinearLayout slot;

        public ViewHolder(View v) {
            super(v);
            ivImage = (ImageView) v.findViewById(R.id.iv_slot_profile_selector);
            slot = (LinearLayout) v.findViewById(R.id.ll_slot);
        }

    }


}