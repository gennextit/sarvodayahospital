package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class AppointmentWithPayment implements Serializable{
	@SerializedName("AppointmentID")
	public String appointmentId;
	
	@SerializedName("ReceiptNo")
	public String receiptNo;
	
	@SerializedName("PaymentRefNo")
	public String paymentRefNo;
	
	@SerializedName("TransactionRefNo")
	public String transactionRefNo;

	public String getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getPaymentRefNo() {
		return paymentRefNo;
	}

	public void setPaymentRefNo(String paymentRefNo) {
		this.paymentRefNo = paymentRefNo;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	
	public static Type getJsonArrayType() {
		return new TypeToken<Collection<AppointmentWithPayment>>() {
		}.getType();
	}

}
