package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class HealthOffer implements Serializable {

	@SerializedName("title")
	public String title;
	
	@SerializedName("url")
	public String url;

	@SerializedName("text")
	public String text;
	
	public HealthOffer(String title, String url, String text) {
		this.title = title;
		this.url = url;
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public static Type getJsonArrayType() {
		return new TypeToken<Collection<HealthOffer>>() {
		}.getType();
	}
}
