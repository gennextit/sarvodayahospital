package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class TransactionHistory implements Serializable{

	@SerializedName("AppID")
	public String appID;

	@SerializedName("AppDate")
	public String appDate;
	
	@SerializedName("AppTime")
	public String appTime;

	@SerializedName("Doctor_ID")
	public String doctorId;

	@SerializedName("DoctorName")
	public String doctorName;
	
	@SerializedName("ReceiptNo")
	public String receiptNo;
	
	@SerializedName("Amount")
	public Double amount;

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public String getAppDate() {
		return appDate;
	}

	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}

	public String getAppTime() {
		return appTime;
	}

	public void setAppTime(String appTime) {
		this.appTime = appTime;
	}

	public String getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public static Type getJsonArrayType() {
		return new TypeToken<Collection<TransactionHistory>>() {
		}.getType();
	}
}
