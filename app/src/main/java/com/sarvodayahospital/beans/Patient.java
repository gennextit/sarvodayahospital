package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class Patient implements Serializable{

	@SerializedName("Patient_Id")
	public String patientId;

	@SerializedName("Title")
	public String title;

	@SerializedName("PName")
	public String patientName;

	@SerializedName("Address")
	public String address;

	@SerializedName("Country")
	public String country;

	@SerializedName("City")
	public String city;
	
	@SerializedName("Locality")
	public String locality;

	@SerializedName("Age")
	public String age;

	@SerializedName("Phone")
	public String phone;

	@SerializedName("Mobile")
	public String mobile;
	
	@SerializedName("Landline")
	public String landline;


	@SerializedName("Gender")
	public String gender;
	
	@SerializedName("Relation")
	public String relation;
	
	@SerializedName("RelationName")
	public String relationName;
	
	
	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getRelationName() {
		return relationName;
	}

	public void setRelationName(String relationName) {
		this.relationName = relationName;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLandline() {
		return landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public static Type getJsonArrayType() {
		return new TypeToken<Collection<Patient>>() {
		}.getType();
	}

}
