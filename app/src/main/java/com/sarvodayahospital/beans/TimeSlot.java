package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class TimeSlot implements Serializable{

	@SerializedName("StartTime")
	public String startTime;

	@SerializedName("EndTime")
	public String endTime;

	@SerializedName("AvgTime")
	public String avgTime;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getAvgTime() {
		return avgTime;
	}

	public void setAvgTime(String avgTime) {
		this.avgTime = avgTime;
	}

	public static Type getJsonArrayType() {
		return new TypeToken<Collection<TimeSlot>>() {
		}.getType();
	}
}
