package com.sarvodayahospital.beans;

import com.google.gson.annotations.SerializedName;

public class DaysAvailable {

	@SerializedName("Day")
	public String day;
	
	@SerializedName("StartTime")
	public String startTime;
	
	@SerializedName("EndTime")
	public String endTime;
	
	public DaysAvailable(String day, String startTime, String endTime) {
		this.day = day;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
}
