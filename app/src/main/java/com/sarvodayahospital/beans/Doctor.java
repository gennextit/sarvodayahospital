package com.sarvodayahospital.beans;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Doctor implements Serializable {

	@SerializedName("doctor_id")
	public String doctorId;

	@SerializedName("doctor_name")
	public String doctorName;

	@SerializedName("department")
	public String department;

	@SerializedName("location")
	public String location;

	@SerializedName("pic_id")
	public String picId;
	
	public Doctor(String doctorId, String doctorName, String department, String location, String picId) {
		this.doctorId = doctorId;
		this.doctorName = doctorName;
		this.department = department;
		this.location = location;
		this.picId = picId;
	}
	
	
	public String getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPicId() {
		return picId;
	}

	public void setPicId(String picId) {
		this.picId = picId;
	}
}
