package com.sarvodayahospital.beans;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class TempDoctor implements Serializable{
	
	@SerializedName("Doctor_ID")
	public String doctorId;
	
	@SerializedName("DoctorName")
	public String doctorName;
	
	@SerializedName("Designation")
	public String designation;
	
	@SerializedName("Department")
	public String department;
	
	@SerializedName("Specialization")
	public String specialization;
	
	@SerializedName("Degree")
	public String degree;
	
	@SerializedName("Day")
	public String day;
	
	@SerializedName("StartTime")
	public String startTime;
	
	@SerializedName("EndTime")
	public String endTime;
	
	@SerializedName("IsImageUploaded")
	public String image;
	
	@SerializedName("ImageURL")
	public String imageURL;
	
	@SerializedName("DoctorFee")
	public String doctorFee;
	
	
	public String getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getDoctorFee() {
		return doctorFee;
	}

	public void setDoctorFee(String doctorFee) {
		this.doctorFee = doctorFee;
	}

	public static Type getJsonArrayType() {
		return new TypeToken<Collection<TempDoctor>>() {
		}.getType();
	}
}
