package com.sarvodayahospital.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Abhijit on 26-Oct-16.
 */

public class PatientApp {

    public static final String COMMON = "sh";

    public static final String PATIENT_ID = "patientId" + COMMON;
    public static final String PATIENT_NAME = "PName" + COMMON;
    public static final String PATIENT_MOBILE = "mobile" + COMMON;




    public static void setPatrintData(Context context, String patientId,String patientName,String patientMobile) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(patientId!=null)
            editor.putString(PATIENT_ID, patientId);
        if(patientName!=null)
            editor.putString(PATIENT_NAME, patientName);
        if(patientMobile!=null)
            editor.putString(PATIENT_MOBILE, patientMobile);
        editor.commit();

    }

    public static String LoadPref(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public static String getPatientId(Context context) {
        return LoadPref(context, PATIENT_ID);
    }

    public static String getPatientName(Context context) {
        return LoadPref(context, PATIENT_NAME);
    }

    public static String getPatientMobile(Context context) {
        return LoadPref(context, PATIENT_MOBILE);
    }
}
