package com.sarvodayahospital.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.sarvodayahospital.beans.PatientModel;

/**
 * Created by Abhijit on 01-Feb-17.
 */
public class AppUser {

    public static final String COMMON="sp";
    private static final String PROFILE_IMAGE="pImage"+COMMON;
    private static final String PCT_ID = "pct_id";
    private static final String PCT_NAME = "pct_name";
    private static final String PCT_TITLE = "pct_title";
    private static final String PCT_COUNTRY = "pct_country";
    private static final String PCT_CITY = "pct_city";
    private static final String PCT_LOCATITY = "pct_localtiy";
    private static final String PCT_AGE = "pct_age";
    private static final String PCT_PHONE = "pct_phone";
    private static final String PCT_MOBILE = "pct_mobile";
    private static final String PCT_GENDER = "pct_gender";
    private static final String SLT_PATIENT_ID = "sltPctId";
    private static final String SLT_PATIENT_DETAIL = "member";
    private static final String NEVER_SHOW_AGAIN = "nsagain"+COMMON;
    private static final String SOS_START_DATE = "s_start_date"+COMMON;
    private static final String SOS_END_DATE = "s_end_date"+COMMON;
    private static final String SOS_IMAGE_URL = "sos_image_url"+COMMON;
    private static final String SOS_NUMBER = "sos_number"+COMMON;
    private static final String HP_NUMBER = "hp_number"+COMMON;


    public static void setHPNumber(Context context,String value) {
        Utility.SavePref(context,HP_NUMBER,value);
    }

    public static String getHPNumber(Context context) {
       return Utility.LoadPref(context,HP_NUMBER);
    }

    public static void setSosNumber(Context context,String value) {
        Utility.SavePref(context,SOS_NUMBER,value);
    }

    public static String getSosNumber(Context context) {
       return Utility.LoadPref(context,SOS_NUMBER);
    }

    public static void setSosImageUrl(Context context,String value) {
        Utility.SavePref(context,SOS_IMAGE_URL,value);
    }

    public static String getSosImageUrl(Context context) {
       return Utility.LoadPref(context,SOS_IMAGE_URL);
    }

    public static void setStartDate(Context context,String value) {
        Utility.SavePref(context,SOS_START_DATE,value);
    }

    public static String getStartDate(Context context) {
       return Utility.LoadPref(context,SOS_START_DATE);
    }

    public static void setEndDate(Context context,String value) {
        Utility.SavePref(context,SOS_END_DATE,value);
    }

    public static String getEndDate(Context context) {
       return Utility.LoadPref(context,SOS_END_DATE);
    }

    public static void setProfileImage(Context context,String profileImage) {
        Utility.SavePref(context,PROFILE_IMAGE,profileImage);
    }

    public static String getProfileImage(Context context) {
       return Utility.LoadPref(context,PROFILE_IMAGE);
    }

    public static void setSelectedPatientId(Context context,String patientId) {
        Utility.SavePref(context,SLT_PATIENT_ID,patientId);
    }

    public static String getSelectedPatientId(Context context) {
        return Utility.LoadPref(context,SLT_PATIENT_ID);
    }

     public static void setSelectedPatientDetail(Context context,String patientDetail) {
        Utility.SavePref(context,SLT_PATIENT_DETAIL,patientDetail);
    }

    public static String getSelectedPatientDetail(Context context) {
        return Utility.LoadPref(context,SLT_PATIENT_DETAIL);
    }

    public static void setNeverShowAgain(Context context, String title) {
        Utility.SavePref(context,NEVER_SHOW_AGAIN,title);
    }

    public static String getNeverShowAgain(Context context) {
        return Utility.LoadPref(context,NEVER_SHOW_AGAIN);
    }

//    public static PatientModel getPatientDetail(Context context){
//        PatientModel model = null;
//        if(context!=null){
//            model=new PatientModel();
//            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//            model.setPatientId(sharedPreferences.getString(PCT_ID, ""));
//            model.setTitle(sharedPreferences.getString(PCT_TITLE, ""));
//            model.setPatientName(sharedPreferences.getString(PCT_NAME, ""));
//            model.setCountry(sharedPreferences.getString(PCT_COUNTRY, ""));
//            model.setCity(sharedPreferences.getString(PCT_CITY, ""));
//            model.setLocality(sharedPreferences.getString(PCT_LOCATITY, ""));
//            model.setAge(sharedPreferences.getString(PCT_AGE, ""));
//            model.setPhone(sharedPreferences.getString(PCT_PHONE, ""));
//            model.setMobile(sharedPreferences.getString(PCT_MOBILE, ""));
//            model.setGender(sharedPreferences.getString(PCT_GENDER, ""));
//        }
//        return model;
//    }

//    public static void setPatientDetail(Context context,PatientModel model){
//        if(context!=null) {
//            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putString(PCT_ID, model.getPatientId());
//            editor.putString(PCT_TITLE, model.getTitle());
//            editor.putString(PCT_NAME, model.getPatientName());
//            editor.putString(PCT_COUNTRY, model.getCountry());
//            editor.putString(PCT_CITY, model.getCity());
//            editor.putString(PCT_LOCATITY, model.getLocality());
//            editor.putString(PCT_AGE, model.getAge());
//            editor.putString(PCT_PHONE, model.getPhone());
//            editor.putString(PCT_MOBILE, model.getMobile());
//            editor.putString(PCT_GENDER, model.getGender());
//            editor.apply();
//        }
//    }
}
