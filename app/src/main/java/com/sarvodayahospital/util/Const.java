package com.sarvodayahospital.util;

/**
 * Created by Admin on 7/8/2017.
 */

public class Const {
    public static final String DOCTOR_ID = "doctor_id";
    public static final String DOCTOR_NAME = "doctor_name";
    public static final String DOCTOR_PIC = "doctor_pic";
    public static final String DOCTOR_DEPARTMENT = "doctor_department";
    public static final String APP_DATE = "appDate";
    public static final String DAY = "day";
    public static final String RATING_STATUS = "rating_status";

}
