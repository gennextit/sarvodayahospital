package com.sarvodayahospital.util;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class RequestBuilder {
    public static RequestBody Default(String userId) {
        return new FormBody.Builder()
                .addEncoded("userId", userId)
                .build();
    }
    public static RequestBody ErrorText(String errorText) {
        return new FormBody.Builder()
                .addEncoded("errorText", errorText)
                .build();
    }


    public static RequestBody LoadDepartmentWiseDoctor(String departmentName) {
        return new FormBody.Builder()
                .addEncoded("DepartmentName", departmentName)
                .build();
    }

    public static RequestBody getPaymentSuccess(String patientId, String doctorId, String appDate
            , String appTime, String source, String deseaseDesc, String paymentMode, String amount
            , String paymentRefNo, String transactionRefNo) {
        return new FormBody.Builder()
                .addEncoded("PatientId", patientId)
                .addEncoded("DoctorId", doctorId)
                .addEncoded("AppDate", appDate)
                .addEncoded("AppTime", appTime)
                .addEncoded("Source", source)
                .addEncoded("DeseaseDesc", deseaseDesc)
                .addEncoded("PaymentMode", paymentMode)
                .addEncoded("Amount", amount)
                .addEncoded("PaymentRefNo", paymentRefNo)
                .addEncoded("TransactionRefNo", transactionRefNo)
                .build();
    }
}
