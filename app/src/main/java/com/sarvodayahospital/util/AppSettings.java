package com.sarvodayahospital.util;

public class AppSettings {
//	public static final String WEB_SERVICE_URL =  "http://103.43.4.141/mobileApp/mobileapp.asmx/";
	public static final String WEB_SERVICE_URL =  "http://services.sarvodayahospital.com/mobileApp/mobileapp.asmx/";
	//public static final String WEB_SERVICE_URL =  "http://103.43.4.141/mobileApp_Demo/mobileapp.asmx/";
	public static final String HOSPITAL_URL =  "http://www.sarvodayahospital.com/mobapp/index.php/Content/";
	public static final String REPORT_SERVER =  "http://www.gennextit.com/errorlog/index.php/LogError/setErrorLog"; //errorText

    public static final String LAB_REPORTS = "http://services.sarvodayahospital.com/sarvodayashrc/Design/online_lab/";
	public static final String ONLINE_LABS = "http://services.sarvodayahospital.com/sarvodaya/Design/online_lab/";
	public static final String APP_UPDATES_AVAILABILITY = "http://www.sarvodayahospital.com/mobapp/index.php/version";

	public static final String LoadDoctorByDoctorId = WEB_SERVICE_URL + "LoadDoctorByDoctorId";
	public static final String LoadPatientData = WEB_SERVICE_URL + "LoadPatientData";
	public static final String TERM_AND_COND = "http://sarvodayahospital.com/terms/termsandconditions.htm";
	public static final String ABOUT_US = "http://sarvodayahospital.com/";

	//[{"patient_id":"340727","Title":"Mr.","PatientName":"ABHIJIT Rao","Country":"","City":"","Locality":"","Age":"0 yrs","Phone":"","Mobile":"8765790292","Gender":"MALE"}]
}

