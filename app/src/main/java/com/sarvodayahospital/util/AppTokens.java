package com.sarvodayahospital.util;

public class AppTokens {
	
	public static final String COMMON = "Sarvodaya";

	public static final String APP_USER = "appuser" + COMMON;


	// special character to prefix the otp. Make sure this character appears
	// only once in the sms
	public static final String SessionIntro = "SessionIntro" + COMMON;
	public static final String SessionSignup = "SessionSignup" + COMMON;
	public static final String SessionProfile = "SessionProfile" + COMMON;
	//public static final String JsonData = "JsonData" + COMMON;


	public static final String DOWNLOAD_DIRECTORY_NAME = COMMON;
	public static final String FolderDirectory = COMMON;

}
