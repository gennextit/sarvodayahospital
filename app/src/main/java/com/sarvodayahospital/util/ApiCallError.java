package com.sarvodayahospital.util;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sarvodaya.patient.R;


public class ApiCallError extends DialogFragment {
    private String errorMessage;
    private TextView tvDescription;
    private Dialog dialog;
    private String mTitle;
    private String mMessage;
    private ErrorListener mListener;
    private ErrorTaskListener mFlagListener;
    private int task;
    private String url;

    public interface ErrorListener {
        void onErrorRetryClick();

        void onErrorCancelClick();
    }

    public interface ErrorTaskListener {
        void onErrorRetryClick(int task,String url);

        void onErrorCancelClick(int task,String url);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static ApiCallError newInstance(String errorMessage, ErrorListener listener) {
        ApiCallError fragment = new ApiCallError();
        fragment.mListener = listener;
        fragment.errorMessage = errorMessage;
        return fragment;
    }

    public static ApiCallError newInstance(String title, String message, String errorMessage, ErrorListener listener) {
        ApiCallError fragment = new ApiCallError();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        fragment.errorMessage = errorMessage;
        return fragment;
    }

    public static ApiCallError newInstance(String errorMessage, int task,String url, ErrorTaskListener listener) {
        ApiCallError fragment = new ApiCallError();
        fragment.mFlagListener = listener;
        fragment.task = task;
        fragment.url = url;
        fragment.errorMessage = errorMessage;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        RelativeLayout button1 = (RelativeLayout) v.findViewById(R.id.relative_view_ok);
        RelativeLayout button2 = (RelativeLayout) v.findViewById(R.id.relative_view_cancel);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        TextView tvOK = (TextView) v.findViewById(R.id.txt_ok);
        TextView tvCancel = (TextView) v.findViewById(R.id.txt_cancel);
        if (mTitle == null) {
            mTitle = getString(R.string.server_time_out_tag);
            mMessage = getString(R.string.server_time_out_msg);
        }
        tvTitle.setText(mTitle);
        tvDescription.setText(mMessage);
        tvOK.setText("Retry");
        tvCancel.setText("Cancel");
        tvTitle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (errorMessage != null) {
                    tvDescription.setText(errorMessage);
                }
            }
        });

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onErrorRetryClick();
                } else if (mFlagListener != null) {
                    mFlagListener.onErrorRetryClick(task,url);
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onErrorCancelClick();
                } else if (mFlagListener != null) {
                    mFlagListener.onErrorCancelClick(task,url);
                }
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
