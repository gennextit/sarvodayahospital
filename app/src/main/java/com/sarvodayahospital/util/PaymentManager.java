package com.sarvodayahospital.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Abhijit-PC on 10-Apr-17.
 */

public class PaymentManager {

    private static final String COMMON = "shpm";
    private static final String PATIENT_ID = "PatientId" + COMMON;
    private static final String DOCTOR_ID = "DoctorId" + COMMON;
    private static final String APP_DATE = "AppDate" + COMMON;
    private static final String APP_TIME = "AppTime" + COMMON;
    private static final String SOURCE = "Source" + COMMON;
    private static final String DESEASE_DESC = "DeseaseDesc" + COMMON;
    private static final String AMOUNT = "Amount" + COMMON;
    private static final String TRANSACTION_REF_NO = "TransactionRefNo" + COMMON;
    private static final String USER_PHONE_NO = "userPhoneNo" + COMMON;

    private String PatientId;
    private String DoctorId;
    private String AppDate;
    private String AppTime;
    private String Source;
    private String DeseaseDesc;
    private String Amount;
    private String TransactionRefNo;
    private String userPhoneNo;

    public static void set(Context context, String patientId, String doctorId, String appDate, String appTime
            , String source, String deseaseDesc, String amount, String transactionRefNo, String userPhoneNo) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(PATIENT_ID, patientId==null?"":patientId);
            editor.putString(DOCTOR_ID, doctorId==null?"":doctorId);
            editor.putString(APP_DATE, appDate==null?"":appDate);
            editor.putString(APP_TIME, appTime==null?"":appTime);
            editor.putString(SOURCE, source==null?"":source);
            editor.putString(DESEASE_DESC, deseaseDesc==null?"":deseaseDesc);
            editor.putString(AMOUNT, amount==null?"":amount);
            editor.putString(TRANSACTION_REF_NO, transactionRefNo==null?"":transactionRefNo);
            editor.putString(USER_PHONE_NO, userPhoneNo==null?"":userPhoneNo);
            editor.apply();
        }
    }

    public static PaymentManager get(Context context) {
        if (context != null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            PaymentManager model = new PaymentManager();
            model.setPatientId(sharedPreferences.getString(PATIENT_ID, ""));
            model.setDoctorId(sharedPreferences.getString(DOCTOR_ID, ""));
            model.setAppDate(sharedPreferences.getString(APP_DATE, ""));
            model.setAppTime(sharedPreferences.getString(APP_TIME, ""));
            model.setSource(sharedPreferences.getString(SOURCE, ""));
            model.setDeseaseDesc(sharedPreferences.getString(DESEASE_DESC, ""));
            model.setAmount(sharedPreferences.getString(AMOUNT, ""));
            model.setTransactionRefNo(sharedPreferences.getString(TRANSACTION_REF_NO, ""));
            model.setUserPhoneNo(sharedPreferences.getString(USER_PHONE_NO, ""));
            return model;
        }
        return null;
    }

    public String getUserPhoneNo() {
        return userPhoneNo;
    }

    public void setUserPhoneNo(String userPhoneNo) {
        this.userPhoneNo = userPhoneNo;
    }

    public String getPatientId() {
        return PatientId;
    }

    public void setPatientId(String patientId) {
        PatientId = patientId;
    }

    public String getDoctorId() {
        return DoctorId;
    }

    public void setDoctorId(String doctorId) {
        DoctorId = doctorId;
    }

    public String getAppDate() {
        return AppDate;
    }

    public void setAppDate(String appDate) {
        AppDate = appDate;
    }

    public String getAppTime() {
        return AppTime;
    }

    public void setAppTime(String appTime) {
        AppTime = appTime;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getDeseaseDesc() {
        return DeseaseDesc;
    }

    public void setDeseaseDesc(String deseaseDesc) {
        DeseaseDesc = deseaseDesc;
    }
  public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

     public String getTransactionRefNo() {
        return TransactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        TransactionRefNo = transactionRefNo;
    }
}

//    String url = AppSettings.WEB_SERVICE_URL
//            + "GenerateAppointmentWithPayment?PatientId=" + "&DoctorId=" +
//            "&AppDate=" + "&AppTime=" + "&Source=MA" + "&DeseaseDesc=" + "&PaymentMode=" + "&Amount="
//            + "&PaymentRefNo=" + "&TransactionRefNo=";
