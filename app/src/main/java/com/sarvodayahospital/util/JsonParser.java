package com.sarvodayahospital.util;

import android.content.Context;
import android.text.TextUtils;

import com.sarvodayahospital.beans.BookingModel;
import com.sarvodayahospital.beans.NotificationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Created by Abhijit-PC on 25-Mar-17.
 */

public class JsonParser {

    public static String ERRORMESSAGE = "";

    public static NotificationModel parseAppUpdates(Context context, String response, int clientVersionCode) {
        Boolean res = false;
        float serverVerCode = 0;
        NotificationModel model = new NotificationModel();
        model.setUpdate(false);
        try {
            JSONObject resObj = new JSONObject(response);
            if (resObj != null) {
                String verCode = resObj.optString("androidVersion");
                String changeDescription = resObj.optString("changeDescription");
                model.setShowPopup(resObj.optString("showPopup"));
                model.setTitle(resObj.optString("title"));
                model.setDescription(resObj.optString("description"));
                model.setImageUrl(resObj.optString("imageUrl"));
                model.setStartDate(resObj.optString("startDate"));
                model.setEndDate(resObj.optString("endDate"));
                model.setSosImageUrl(resObj.optString("sosImageUrl"));
                model.setSosNumber(resObj.optString("sosNumber"));
                model.setHpNumber(resObj.optString("hpNumber"));
                // for testing purpose
//                model.setStartDate("");
//                model.setEndDate("");
//                model.setSosImageUrl("http://sarvodayahospital.com/image/sos.png");
                if (!TextUtils.isEmpty(model.getSosNumber())) {
                    AppUser.setSosNumber(context, model.getSosNumber());
                }
                if (!TextUtils.isEmpty(model.getStartDate())) {
                    AppUser.setStartDate(context, model.getStartDate());
                }
                if (!TextUtils.isEmpty(model.getEndDate())) {
                    AppUser.setEndDate(context, model.getEndDate());
                }
                if (!TextUtils.isEmpty(model.getSosImageUrl())) {
                    AppUser.setSosImageUrl(context, model.getSosImageUrl());
                }
                if (!TextUtils.isEmpty(model.getHpNumber())) {
                    AppUser.setHPNumber(context, model.getHpNumber());
                }
//                model.setShowPopup("YES");
//                model.setTitle("New Update new");
//                model.setDescription("There is a new update available on playstore.");
//                model.setImageUrl("https://cdn.dribbble.com/users/1387536/screenshots/3185925/attachments/678208/3333.jpg");
                if (!TextUtils.isEmpty(verCode)) {
                    serverVerCode = Float.parseFloat(verCode);
                }
                if (serverVerCode > clientVersionCode) {
                    model.setUpdate(true);
                    res = true;
                } else {
                    model.setUpdate(false);
                    res = false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            model.setUpdate(false);
        }
        return model;
    }

    public static BookingModel defaultParser(String response) {
        return null;
    }

    public static BookingModel parseLoadDrByDrId(String response) {
        BookingModel jsonModel = new BookingModel();
        jsonModel.setOutput("ERROR-101");
        jsonModel.setOutputMsg("ERROR-101 No msg available");
        if (response.contains("[")) {
            String finalResult = response.substring(response.indexOf('['), response.indexOf(']') + 1);
            try {
                JSONArray mainArray = new JSONArray(finalResult);
                for (int i = 0; i < mainArray.length(); i++) {
                    JSONObject mainObject = mainArray.getJSONObject(i);
                    if (!TextUtils.isEmpty(mainObject.optString("ErrorCode"))) {
                        jsonModel.setOutput("failure");
                        jsonModel.setOutputMsg(mainObject.getString("ErrorMsg"));
                    } else if (!TextUtils.isEmpty(mainObject.optString("Doctor_ID"))) {
                        jsonModel.setOutput("success");

                    }
                }
            } catch (JSONException e) {
                ERRORMESSAGE = e.toString();
                return null;
            }
        } else {
            ERRORMESSAGE = response;
            return null;
        }
        return jsonModel;
    }
}
