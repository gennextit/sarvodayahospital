package com.sarvodayahospital.util;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Abhijit on 13-Dec-16.
 */

public class ApiCall {
    //GET network request
    public static String GET(String url){
        String res = null;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            res= response.body().string();
        } catch (IOException e) {
            return e.toString();
        }
        return res;
    }

    //POST network request
    public static String POST(String url, RequestBody body){
         return POST(url,body,0);
    }

    public static String POST(String url, RequestBody body, long maxTimeout){
        String res = null;

        OkHttpClient client;
        if(maxTimeout ==0) {
            client = new OkHttpClient();
        }else{
            client = getHttpClient(maxTimeout).build();
        }

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            res=response.body().string();
        } catch (IOException e) {
            return e.toString();
        }
        return res;
    }

    private static OkHttpClient.Builder getHttpClient(long maxTimeout) {
        return new OkHttpClient.Builder()
                .connectTimeout(maxTimeout, TimeUnit.SECONDS)
                .readTimeout(maxTimeout, TimeUnit.SECONDS)
                .writeTimeout(maxTimeout, TimeUnit.SECONDS);
    }
}