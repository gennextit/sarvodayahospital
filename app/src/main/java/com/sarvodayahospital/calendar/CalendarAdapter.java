package com.sarvodayahospital.calendar;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.sarvodaya.patient.appointment.TimeSelectionActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.CalendarDays;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.Const;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CalendarAdapter extends BaseAdapter {
	private final ImageView profile_pic;
	private Context mContext;

	private GregorianCalendar month;
	int firstDay;
	int maxWeeknumber;
	int maxP;
	int calMaxP;
	int lastWeekDay;
	int leftDays;
	int mnthlength;
	String itemvalue, curentDateString;
	DateFormat df;
	private final String doctor_id, doctor_name,doctor_dept,doctor_pic;

	private ArrayList<String> items;
	public static List<String> dayString;
	public static Map<String,String> availableString, bookedString;
	private View previousView;
	List<CalendarDays> calendarDaysList;
	Activity activity;
	
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
	public CalendarAdapter(Context c, List<CalendarDays> calendarDaysList, String doctor_id
			, String doctor_name, String doctor_dept, String doctor_pic
			, Activity activity, GregorianCalendar mon, ImageView profile_pic) {
		mContext = c;
		this.calendarDaysList = calendarDaysList;
		this.doctor_id = doctor_id;
		this.doctor_name = doctor_name;
		this.doctor_dept = doctor_dept;
		this.doctor_pic = doctor_pic;
		this.activity = activity;
		this.profile_pic=profile_pic;
		this.month=mon;
		
		// Check if Internet present
		cd = new ConnectionDetector(c);
		
				
	}
	
	public class ViewHolder {
		public TextView dayView;
		public String color;
	}

	public void setItems(ArrayList<String> items) {
		for (int i = 0; i != items.size(); i++) {
			if (items.get(i).length() == 1) {
				items.set(i, "0" + items.get(i));
			}
		}
		this.items = items;
	}

	public int getCount() {
		return calendarDaysList.size();
	}

	public Object getItem(int position) {
		return 0;
	}

	public long getItemId(int position) {
		return 0;
	}

	// create a new view for each item referenced by the Adapter
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		final ViewHolder viewHolder;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater vi = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.calendar_item, null);
			viewHolder = new ViewHolder();
			viewHolder.dayView = (TextView) v.findViewById(R.id.date);
			v.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) v.getTag();
		}
		
		if(calendarDaysList.get(position).getDay().contentEquals("")) {
			
		} else {
			viewHolder.dayView.setText(calendarDaysList.get(position).getDay());
			
			if(calendarDaysList.get(position).getStatus().contentEquals("Green")) {
				viewHolder.dayView.setTextColor(Color.WHITE);
				v.setBackgroundResource(R.drawable.circle_green);
				viewHolder.color = "green";
			} else if(calendarDaysList.get(position).getStatus().contentEquals("Grey")) {
				viewHolder.dayView.setTextColor(Color.parseColor("#9a9a9a"));
				v.setBackgroundResource(R.drawable.circle_grey);
				viewHolder.color = "grey";
			} else if(calendarDaysList.get(position).getStatus().contentEquals("Blue")) {
				viewHolder.dayView.setTextColor(Color.WHITE);
				v.setBackgroundResource(R.drawable.circle_blue);
				viewHolder.color = "grey";
			}
		}
		
		
		v.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				if(viewHolder.color != null && viewHolder.color.contentEquals("green")) {
					isInternetPresent = cd.isConnectingToInternet();
					if (!isInternetPresent) {
						// Internet Connection is not present
						alert.showAlertDialog(activity, "Internet Connection Error",
								"Please connect to working Internet connection", false);
						// stop executing code by return
						return;
					}
					System.out.println("Green");
					setSelected(arg0, "Green");
					String day=calendarDaysList.get(position).getDay();
					startSelectAppointmentActivity(doctor_id,doctor_name,doctor_dept,doctor_pic,day,android.text.format.DateFormat.format("yyyy-MM", month) + "-" + day);

				} else if(viewHolder.color != null && viewHolder.color.contentEquals("pink")) {
					System.out.println("Pink");
				} else if(viewHolder.color != null && viewHolder.color.contentEquals("grey")) {
					System.out.println("Grey");
				}
			}
		});
		return v;
	}

	private void startSelectAppointmentActivity(String doctorId,String drName,String drDept,String drPic, String day, String appDate) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Intent intent = new Intent(activity, TimeSelectionActivity.class);
			intent.putExtra(Const.DOCTOR_ID, doctorId);
			intent.putExtra(Const.DOCTOR_NAME, drName);
			intent.putExtra(Const.DOCTOR_DEPARTMENT, drDept);
			intent.putExtra(Const.DOCTOR_PIC, drPic);
			intent.putExtra(Const.APP_DATE, appDate);
			intent.putExtra(Const.DAY, day);
			ActivityOptions options = ActivityOptions
					.makeSceneTransitionAnimation(activity, profile_pic,mContext.getResources().getString(R.string.transition_profile));
			activity.startActivity(intent, options.toBundle());
			return;
		}
		Intent intent = new Intent(mContext, TimeSelectionActivity.class);
		intent.putExtra(Const.DOCTOR_ID, doctorId);
		intent.putExtra(Const.DOCTOR_NAME, drName);
		intent.putExtra(Const.DOCTOR_DEPARTMENT, drDept);
		intent.putExtra(Const.DOCTOR_PIC, drPic);
		intent.putExtra(Const.APP_DATE, appDate);
		intent.putExtra(Const.DAY, day);
		activity.startActivity(intent);
	}

	public View setSelected(View view) {
		if (previousView != null) {
			previousView.setBackgroundResource(R.drawable.list_item_background);
		}
		previousView = view;
		view.setBackgroundResource(R.drawable.circle_orange);
		return view;
	}
	
	public View setSelected(View view, String color) {
		if (previousView != null) {
			previousView.setBackgroundResource(R.drawable.list_item_green_background);
		}
		previousView = view;
		view.setBackgroundResource(R.drawable.circle_orange);
		return view;
	}

	

}