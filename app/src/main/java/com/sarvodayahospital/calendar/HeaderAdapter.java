package com.sarvodayahospital.calendar;

import java.util.ArrayList;
import java.util.List;

import com.sarvodaya.patient.R;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class HeaderAdapter extends BaseAdapter {
	private Context mContext;
	public static List<String> dayString;
	
	public HeaderAdapter(Context c) {
		mContext = c;
		HeaderAdapter.dayString = new ArrayList<String>();
		dayString.add("Sun");
		dayString.add("Mon");
		dayString.add("Tue");
		dayString.add("Wed");
		dayString.add("Thu");
		dayString.add("Fri");
		dayString.add("Sat");
	}
	
	public class ViewHolder {
		public TextView dayView;
	}
	
	public int getCount() {
		return dayString.size();
	}

	public Object getItem(int position) {
		return dayString.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}
	
	// create a new view for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		final ViewHolder viewHolder;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater vi = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.header_item, null);
			viewHolder = new ViewHolder();
			viewHolder.dayView = (TextView) v.findViewById(R.id.date);
			v.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) v.getTag();
		}
		viewHolder.dayView.setTextColor(Color.BLACK);
		viewHolder.dayView.setText(dayString.get(position));
		return v;
	}
}
