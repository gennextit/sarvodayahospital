package com.sarvodayahospital.dialog;

import com.sarvodaya.patient.MainActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.util.AppUser;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * 
 * Create custom Dialog windows for your application Custom dialogs rely on
 * custom layouts wich allow you to create and use your own look & feel.
 * 
 * Under GPL v3 : http://www.gnu.org/licenses/gpl-3.0.html
 * 
 * @author vDoIt
 * 
 */
public class CustomDialogCallEmergency extends DialogFragment implements
		android.view.View.OnClickListener {

	public Activity c;
	Context mContext;
	public RelativeLayout relativeLayoutOK, relativeLayoutCancel;

	public static CustomDialogCallEmergency newInstance(Activity a, Context mContext) {
		CustomDialogCallEmergency dialog=new CustomDialogCallEmergency();
		dialog.c = a;
		dialog.mContext = mContext;
		return dialog;
	}

//	public CustomDialogCallEmergency(Activity a, Context mContext) {
//		this.c = a;
//		this.mContext = mContext;
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			   Bundle savedInstanceState) {
		View dialogView = inflater.inflate(R.layout.dialog_call_emergency_no, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		relativeLayoutOK = (RelativeLayout) dialogView.findViewById(R.id.relative_view_ok);
		relativeLayoutCancel = (RelativeLayout) dialogView.findViewById(R.id.relative_view_cancel);

		relativeLayoutOK.setOnClickListener(this);
		relativeLayoutCancel.setOnClickListener(this);
		return dialogView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.relative_view_ok:
			dismiss();
			String mNumber;
			String skSosNo= AppUser.getSosNumber(getActivity());
			if(!TextUtils.isEmpty(skSosNo)){
				mNumber=skSosNo;
			}else{
				mNumber="105959";
			}
			Uri number = Uri.parse("tel:"+mNumber);
			Intent callIntent = new Intent(Intent.ACTION_CALL, number);
			startActivity(callIntent);
			break;
		case R.id.relative_view_cancel:
			dismiss();
			break;
		default:
			break;
		}

	}
}
