package com.sarvodayahospital.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sarvodaya.patient.R;

import java.util.ArrayList;


public class PopupDialog extends DialogFragment {
    private static final boolean DEFAULT_CANCELABLE_STATUS = true;
    private TextView tvDescription;
    private Dialog dialog;
    private String mTitle;
    private String mMessage;
    private DialogListener mListener;
    private DialogTaskListener mTaskListener;
    private int task;
    private boolean cancelableStatus;
    private ArrayList<String> data;

    public interface DialogListener {
        void onOkClick(DialogFragment dialog);

        void onCancelClick(DialogFragment dialog);
    }

    public interface DialogTaskListener {
        void onOkClick(DialogFragment dialog, int task,ArrayList<String> data);

        void onCancelClick(DialogFragment dialog, int task,ArrayList<String> data);
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static PopupDialog newInstance(String title, String message, DialogListener listener) {
        PopupDialog fragment = new PopupDialog();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        fragment.cancelableStatus = DEFAULT_CANCELABLE_STATUS;
        return fragment;
    }

    public static PopupDialog newInstance(String title, String message, Boolean cancelable, DialogListener listener) {
        PopupDialog fragment = new PopupDialog();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.mListener = listener;
        fragment.cancelableStatus = cancelable;
        return fragment;
    }

    public static PopupDialog newInstance(int task, ArrayList<String> data, String title, String message, DialogTaskListener listener) {
        PopupDialog fragment = new PopupDialog();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.task = task;
        fragment.data = data;
        fragment.mTaskListener = listener;
        fragment.cancelableStatus = DEFAULT_CANCELABLE_STATUS;
        return fragment;
    }

    public static PopupDialog newInstance(int task,String title, String message, Boolean cancelable, DialogTaskListener listener) {
        PopupDialog fragment = new PopupDialog();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.task = task;
        fragment.mTaskListener = listener;
        fragment.cancelableStatus = cancelable;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        RelativeLayout button1 = (RelativeLayout) v.findViewById(R.id.relative_view_ok);
        RelativeLayout button2 = (RelativeLayout) v.findViewById(R.id.relative_view_cancel);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        TextView tvOK = (TextView) v.findViewById(R.id.txt_ok);
        TextView tvCancel = (TextView) v.findViewById(R.id.txt_cancel);

        tvTitle.setText(mTitle);
        tvDescription.setText(mMessage);
        tvOK.setText("Ok");
        tvCancel.setText("Cancel");
        setCancelable(cancelableStatus);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onOkClick(PopupDialog.this);
                }
                if (mTaskListener != null) {
                    mTaskListener.onOkClick(PopupDialog.this, task,data);
                }

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (mListener != null) {
                    mListener.onCancelClick(PopupDialog.this);
                }
                if (mTaskListener != null) {
                    mTaskListener.onCancelClick(PopupDialog.this, task,data);
                }
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
