package com.sarvodayahospital.dialog;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.sarvodaya.patient.R;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomDialogCancelAppointment extends DialogFragment implements
	android.view.View.OnClickListener{

	public Activity c;
	Context mContext;
	public RelativeLayout relativeLayoutOK, relativeLayoutCancel;
	String doctorId, appId,  appTime;
	Date appDate;
	public String finalResult;
	ProgressDialog dialog;
	private IntimateTransactionHistory mCallback;
	private ProgressDialog progressDialog;

	public CustomDialogCancelAppointment(Activity a, Context mContext, String doctorId, String appId, Date appDate, String appTime, IntimateTransactionHistory mCallback) {
		this.c = a;
		this.mContext = mContext;
		this.doctorId = doctorId;
		this.appId = appId;
		this.appDate = appDate;
		this.appTime = appTime;
		this.mCallback = mCallback;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			   Bundle savedInstanceState) {
		View dialogView = inflater.inflate(R.layout.dialog_cancel_appointment, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		relativeLayoutOK = (RelativeLayout) dialogView.findViewById(R.id.relative_view_ok);
		relativeLayoutCancel = (RelativeLayout) dialogView.findViewById(R.id.relative_view_cancel);
		
		relativeLayoutOK.setOnClickListener(this);
		relativeLayoutCancel.setOnClickListener(this);
		return dialogView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.relative_view_ok:
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String date = formatter.format(appDate);
			String time = "";
			if(appTime.contains("AM")) {
				time = appTime.substring(0, 5);
			} else {
				if(appTime.substring(0,2).contentEquals("12")) {
					Integer hours = Integer.parseInt(appTime.substring(0,2));
					time = hours.toString() + ":" + appTime.subSequence(3, 5);
				} else {
					Integer hours = Integer.parseInt(appTime.substring(0,2)) + 12;
					time = hours.toString() + ":" + appTime.subSequence(3, 5);
				}
			}
			String reason = "Cancelled%20from%20mobile";
			
			HttpAsyncTaskCancelAppointment asyncTask = new HttpAsyncTaskCancelAppointment(new AsyncResponse() {
				@Override
				public void processFinish(String output) {
					if(output.contentEquals("Appointment cancelled")) {
						mCallback.callLoadMemeber();
						dismiss();
					} else {
						Toast.makeText(mContext, "Error, please contact admin.", Toast.LENGTH_LONG).show();
						dismiss();
					}
					
				}
	        });
			asyncTask.execute(AppSettings.WEB_SERVICE_URL
					+ "CancelAppointmentByPatient?Doctor_ID=" + doctorId 
					+ "&AppNo=" + appId + "&AppDate=" + date + "&AppTime=" + time
					+ "&CancelReason=" + reason);
			break;
		case R.id.relative_view_cancel:
			dismiss();
			break;
		default:
			break;
		}
		
	}

//	public static String GET(String url) {
//		InputStream inputStream = null;
//		String result = "";
//		try {
//
//			// create HttpClient
//			HttpClient httpclient = new DefaultHttpClient();
//
//			// make GET request to the given URL
//			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
//
//			// receive response as inputStream
//			inputStream = httpResponse.getEntity().getContent();
//
//			// convert inputstream to string
//			if (inputStream != null)
//				result = convertInputStreamToString(inputStream);
//			else
//				result = "Did not work!";
//
//		} catch (Exception e) {
//			Log.d("InputStream", e.getLocalizedMessage());
//		}
//
//		return result;
//	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}
	
	public interface AsyncResponse {
	    void processFinish(String output);
	}
	
	private class HttpAsyncTaskCancelAppointment extends AsyncTask<String, Void, String> {
		public AsyncResponse delegate= null;
		
		public HttpAsyncTaskCancelAppointment(AsyncResponse asyncResponse) {
	        delegate = asyncResponse;//Assigning call back interfacethrough constructor
	    }
		
		@Override
        protected void onPreExecute() {
			showProgressDialog(c, "Loading data, Keep patience!",Gravity.CENTER);
//            dialog= new ProgressDialog(c);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		@Override
		protected String doInBackground(String... urls) {
			return  ApiCall.GET(urls[0]);
//			return GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
 				System.out.println(result);
 				if(result.contains("Appointment cancelled")) {
 					finalResult = "Appointment cancelled";
 				} else {
 					finalResult = "Error";
 				}
 				delegate.processFinish(finalResult);
			} catch (Exception pe) {
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
	}
	
	// Container Activity must implement this interface
	public interface IntimateTransactionHistory {
		public void callLoadMemeber();
	}

	public void showProgressDialog(Activity context, String Msg, int gravity) {
		if (progressDialog == null) {
			progressDialog = new ProgressDialog(context);
			progressDialog.setMessage(Msg);
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(false);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //>= API 21progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
				progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation,c.getTheme()));
			} else {
				progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
			}
			if (gravity == Gravity.BOTTOM) {
				progressDialog.getWindow().setGravity(Gravity.BOTTOM);
			}
			progressDialog.show();

		}
	}

	public void hideProgressDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
