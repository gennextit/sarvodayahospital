package com.sarvodayahospital.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvodaya.patient.DatabaseHandler;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.Contact;

import java.util.List;

public class CustomAddPaymentDetails extends Dialog implements
		android.view.View.OnClickListener {

	private final double totalamount;
	public Activity c;
	Context mContext;
	public RelativeLayout relativeLayoutOK, relativeLayoutCancel;
	public EditText nameEditText, emailEditText, addressEditText, cityEditText, stateEditText, pincodeEditText;
	private IntimatePayment mCallback;
	private String countryEditText="IND";
	private TextView btnOk;

	public CustomAddPaymentDetails(Activity a, Context mContext, IntimatePayment mCallback, double totalamount) {
		super(a);
		this.c = a;
		this.mContext = mContext;
		this.mCallback = mCallback;
		this.totalamount=totalamount;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_payment_details);
		relativeLayoutOK = (RelativeLayout) findViewById(R.id.relative_view_ok);
		relativeLayoutCancel = (RelativeLayout) findViewById(R.id.relative_view_cancel);
		btnOk = (TextView) findViewById(R.id.txt_ok);
		nameEditText = (EditText) findViewById(R.id.nameText);
		emailEditText = (EditText) findViewById(R.id.emailText);
		addressEditText = (EditText) findViewById(R.id.addressText);
		cityEditText = (EditText) findViewById(R.id.cityText);
		stateEditText = (EditText) findViewById(R.id.stateText);
		pincodeEditText = (EditText) findViewById(R.id.pincodeText);
		
	
		DatabaseHandler db = new DatabaseHandler(mContext);
		List<Contact> contactList = db.getAllContacts();
		Contact contact = contactList.get(0);
		
		nameEditText.setText(contact.getName());
		btnOk.setText("Pay Rs."+String.valueOf(totalamount));
		if(contact.getAddress() != null) {
			addressEditText.setText(contact.getAddress());
			cityEditText.setText(contact.getCity());
		}
		relativeLayoutOK.setOnClickListener(this);
		relativeLayoutCancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.relative_view_ok:
			if(nameEditText.getText().toString().length() == 0) {
				Toast toast = Toast.makeText(c,
						"Enter name", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
				nameEditText.requestFocusFromTouch();
		        InputMethodManager lManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE); 
		        lManager.showSoftInput(nameEditText, 0);
			} else if(addressEditText.getText().toString().length() == 0) {
				Toast toast = Toast.makeText(c,
						"Enter address", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
				addressEditText.requestFocusFromTouch();
				InputMethodManager lManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
				lManager.showSoftInput(addressEditText, 0);
			} else if(cityEditText.getText().toString().length() == 0) {
				Toast toast = Toast.makeText(c,
						"Enter city" +
								"", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
				cityEditText.requestFocusFromTouch();
				InputMethodManager lManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
				lManager.showSoftInput(cityEditText, 0);
			} else if(stateEditText.getText().toString().length() == 0) {
				Toast toast = Toast.makeText(c,
						"Enter state", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
				stateEditText.requestFocusFromTouch();
				InputMethodManager lManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
				lManager.showSoftInput(stateEditText, 0);
			} else if(pincodeEditText.getText().toString().length() == 0) {
				Toast toast = Toast.makeText(c,
						"Enter pincode", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
				pincodeEditText.requestFocusFromTouch();
				InputMethodManager lManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
				lManager.showSoftInput(pincodeEditText, 0);
			} else if(emailEditText.getText().toString().length() == 0) {
				Toast toast = Toast.makeText(c,
						"Enter email", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
				emailEditText.requestFocusFromTouch();
		        InputMethodManager lManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE); 
		        lManager.showSoftInput(emailEditText, 0);
			} else {
				mCallback.sendDetails(nameEditText.getText().toString().trim(), emailEditText.getText().toString().trim(),
						addressEditText.getText().toString().trim(), cityEditText.getText().toString().trim(), 
						stateEditText.getText().toString().trim(), countryEditText,
						pincodeEditText.getText().toString().trim());
				dismiss();
			}
			break;
		case R.id.relative_view_cancel:
			dismiss();
			break;
		default:
			break;
		}
		
	}
	
	// Container Activity must implement this interface
	public interface IntimatePayment {
		public void sendDetails(String name, String email, String address, String city, String state, String country, String pincode);
	}
}
