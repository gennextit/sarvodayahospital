package com.sarvodayahospital.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sarvodaya.patient.R;


public class PopupAlert extends DialogFragment {
    private static final String DEFAULT_POSITIVE_BUTTON = "Ok";
    private static final String DEFAULT_NEGATIVE_BUTTON = "Dismiss";
    private TextView tvDescription;
    private Dialog dialog;
    private String mTitle;
    private String mMessage;
    private String renameOk;
    private String renameDismiss;
    private Fragment popupFragment;

    public void onCreate(Bundle state) {
        super.onCreate(state);
        setRetainInstance(true);
    }

    public static PopupAlert newInstance(String message) {
        PopupAlert fragment = new PopupAlert();
        fragment.mTitle = "Alert";
        fragment.mMessage = message;
        fragment.renameOk = DEFAULT_POSITIVE_BUTTON;
        fragment.renameDismiss = DEFAULT_NEGATIVE_BUTTON;
        return fragment;
    }

    public static PopupAlert newInstance(String message, Fragment popupFragment) {
        PopupAlert fragment = new PopupAlert();
        fragment.mTitle = "Alert";
        fragment.mMessage = message;
        fragment.popupFragment = popupFragment;
        fragment.renameOk = DEFAULT_POSITIVE_BUTTON;
        fragment.renameDismiss = DEFAULT_NEGATIVE_BUTTON;
        return fragment;
    }

    public static PopupAlert newInstance(String title, String message) {
        PopupAlert fragment = new PopupAlert();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.renameOk = DEFAULT_POSITIVE_BUTTON;
        fragment.renameDismiss = DEFAULT_NEGATIVE_BUTTON;
        return fragment;
    }

    public static PopupAlert newInstance(String title, String message, String renameOk) {
        PopupAlert fragment = new PopupAlert();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.renameOk = renameOk;
        fragment.renameDismiss = DEFAULT_NEGATIVE_BUTTON;
        return fragment;
    }

    public static PopupAlert newInstance(String title, String message, String renameOk, String renameDismiss) {
        PopupAlert fragment = new PopupAlert();
        fragment.mTitle = title;
        fragment.mMessage = message;
        fragment.renameOk = renameOk;
        fragment.renameDismiss = renameDismiss;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.popup_alert, null);
        dialogBuilder.setView(v);
        RelativeLayout button1 = (RelativeLayout) v.findViewById(R.id.relative_view_ok);
        RelativeLayout button2 = (RelativeLayout) v.findViewById(R.id.relative_view_cancel);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        TextView tvOK = (TextView) v.findViewById(R.id.txt_ok);
        TextView tvCancel = (TextView) v.findViewById(R.id.txt_cancel);

        tvTitle.setText(mTitle);
        tvDescription.setText(mMessage);
        tvOK.setText(renameOk);
        tvCancel.setText(renameDismiss);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if(popupFragment!=null){
                    getFragmentManager().popBackStack();
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

        return dialog;
    }
}
