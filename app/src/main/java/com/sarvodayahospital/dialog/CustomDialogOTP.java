package com.sarvodayahospital.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.OTP;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.DefaultHttpClient;

public class CustomDialogOTP extends Dialog implements
	android.view.View.OnClickListener {
	
	public Activity c;
	Context mContext;
	public LinearLayout relativeLayoutConfirm, relativeLayoutCancel, relativeLayoutResend;
	String otp;
	public EditText otpEditText;
	private IntimateAddMember mCallback; 
	ProgressDialog dialog;
	String mobile;
	private ProgressDialog progressDialog;

	public CustomDialogOTP(Activity a, Context mContext, String otp, IntimateAddMember mCallback, String mobile) {
		super(a);
		this.c = a;
		this.mContext = mContext;
		this.otp = otp;
		this.mCallback = mCallback;
		this.mobile = mobile;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_otp);
		relativeLayoutConfirm = (LinearLayout) findViewById(R.id.relative_view_confirm);
		relativeLayoutCancel = (LinearLayout) findViewById(R.id.relative_view_cancel);
		relativeLayoutResend = (LinearLayout) findViewById(R.id.relative_view_resend);
		otpEditText = (EditText) findViewById(R.id.otp);
		
		relativeLayoutConfirm.setOnClickListener(this);
		relativeLayoutCancel.setOnClickListener(this);
		relativeLayoutResend.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.relative_view_confirm:
			if(otpEditText.getText().toString().length() == 0) {
				Toast toast = Toast.makeText(c,
						"Enter OTP.", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
				toast.show();
			} else {
				if(otpEditText.getText().toString().contentEquals(otp)) {
					mCallback.callAddMember();
					dismiss();
				} else {
					Toast toast = Toast.makeText(c,
							"Wrong OTP.", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
					toast.show();
				}
			}
			break;
		case R.id.relative_view_cancel:	
			dismiss();
			break;
		case R.id.relative_view_resend:	
			String otpURL = AppSettings.WEB_SERVICE_URL + "GenerateOTP?Phone=" + mobile;
			new HttpAsyncGenerateOTP().execute(otpURL);
			break;
		
		default:
			break;
		}
		
	}
	
	// Container Activity must implement this interface
	public interface IntimateAddMember {
		public void callAddMember();
	}
	
//	public static String GET(String url) {
//		InputStream inputStream = null;
//		String result = "";
//		try {
//
//			// create HttpClient
//			HttpClient httpclient = new DefaultHttpClient();
//
//			// make GET request to the given URL
//			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
//
//			// receive response as inputStream
//			inputStream = httpResponse.getEntity().getContent();
//
//			// convert inputstream to string
//			if (inputStream != null)
//				result = convertInputStreamToString(inputStream);
//			else
//				result = "Did not work!";
//
//		} catch (Exception e) {
//			Log.d("InputStream", e.getLocalizedMessage());
//		}
//
//		return result;
//	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}

	private class HttpAsyncGenerateOTP extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(c, "Sending OTP again, Keep patience!",Gravity.CENTER);
//            dialog= new ProgressDialog(mContext);
//            dialog.setCancelable(false);
//            dialog.setMessage("Sending OTP again, Keep patience!");
//            dialog.show();
                             
        }@Override
		protected String doInBackground(String... urls) {

			return  ApiCall.GET(urls[0]);
//			return GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if(result.contains("Patient_Id")) {
					
				} else {
					JSONParser parser = new JSONParser();
	 				String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
					Object obj = parser.parse(finalResult);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					List<OTP> otpList = sGson.fromJson(array.toString(), OTP.getJsonArrayType());
					otp = otpList.get(0).getOtp();
					otpEditText.setText("");
					
				}
			} catch (Exception pe) {
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
	}

	public void showProgressDialog(Activity context, String Msg, int gravity) {
		if (progressDialog == null) {
			progressDialog = new ProgressDialog(context);
			progressDialog.setMessage(Msg);
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(false);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //>= API 21progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
				progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation,mContext.getTheme()));
			} else {
				progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
			}
			if (gravity == Gravity.BOTTOM) {
				progressDialog.getWindow().setGravity(Gravity.BOTTOM);
			}
			progressDialog.show();

		}
	}

	public void hideProgressDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
