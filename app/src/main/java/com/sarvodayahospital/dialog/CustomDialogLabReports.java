package com.sarvodayahospital.dialog;

import com.sarvodaya.patient.MainActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.util.AppSettings;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

public class CustomDialogLabReports extends DialogFragment implements
	android.view.View.OnClickListener {
	public Activity c;
	Context mContext;
	public RelativeLayout relativeLayoutCancel, relative_view_sector_8, relative_view_sector_19;
	
//	public CustomDialogLabReports(Activity a, Context mContext) {
//		this.c = a;
//		this.mContext = mContext;
//	}
	public static CustomDialogLabReports newInstance(Activity a, Context mContext) {
		CustomDialogLabReports dialog=new CustomDialogLabReports();
		dialog.c = a;
		dialog.mContext = mContext;
		return dialog;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			   Bundle savedInstanceState) {
		View dialogView = inflater.inflate(R.layout.dialog_lab_reports, container, false);
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		relativeLayoutCancel = (RelativeLayout) dialogView.findViewById(R.id.relative_view_cancel);
		relative_view_sector_8 = (RelativeLayout) dialogView.findViewById(R.id.relative_view_sector_8);
		relative_view_sector_19 = (RelativeLayout) dialogView.findViewById(R.id.relative_view_sector_19);
		
		relativeLayoutCancel.setOnClickListener(this);
		relative_view_sector_8.setOnClickListener(this);
		relative_view_sector_19.setOnClickListener(this);
		return dialogView;
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.relative_view_sector_8:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppSettings.LAB_REPORTS));
			startActivity(browserIntent);
			dismiss();
			break;
		case R.id.relative_view_sector_19:
			Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(AppSettings.ONLINE_LABS));
			startActivity(browserIntent1);
			dismiss();
			break;
		case R.id.relative_view_cancel:
			dismiss();
			break;
		default:
			break;
		}
		
	}


}
