package com.sarvodayahospital.dialog;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.sarvodaya.patient.R;

public class VersionUpdateDialog extends DialogFragment implements View.OnClickListener{
  ;
    public RelativeLayout relativeLayoutOK, relativeLayoutCancel;

    public static VersionUpdateDialog newInstance() {
        VersionUpdateDialog dialog=new VersionUpdateDialog();
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View dialogView = inflater.inflate(R.layout.dialog_version_update, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        relativeLayoutOK = (RelativeLayout) dialogView.findViewById(R.id.relative_view_ok);
        relativeLayoutCancel = (RelativeLayout) dialogView.findViewById(R.id.relative_view_cancel);

        relativeLayoutOK.setOnClickListener(this);
        relativeLayoutCancel.setOnClickListener(this);
        return dialogView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relative_view_ok:
                dismiss();
                Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.sarvodaya.patient"));
                startActivity(browserIntent1);
                break;
            case R.id.relative_view_cancel:
                dismiss();
                break;
            default:
                break;
        }

    }
}
