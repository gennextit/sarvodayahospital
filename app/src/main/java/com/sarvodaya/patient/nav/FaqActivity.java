package com.sarvodaya.patient.nav;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.widget.ListView;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.FAQ;
import com.sarvodayahospital.doctor.adapter.FaqAdapter;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FaqActivity extends BaseActivity {
	final Context context = this;
	ListView listView;
	private FaqAdapter adapter;
	List<FAQ> faqList = new ArrayList<FAQ>();
	
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	ProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faq);
		
		SpannableString s = new SpannableString("FAQs");
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
		            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		listView = (ListView) findViewById(R.id.listview);
		
		// Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());
				
		new HttpAsyncTaskFaq().execute(AppSettings.HOSPITAL_URL	+ "getFaq");
	}
//	public static String GET(String url) {
//		url = url.replaceAll("&", "%26");
//		url = url.replaceAll(" ", "%20");
//		InputStream inputStream = null;
//		String result = "";
//		try {
//
//			// create HttpClient
//			HttpClient httpclient = new DefaultHttpClient();
//
//			// make GET request to the given URL
//			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
//
//			// receive response as inputStream
//			inputStream = httpResponse.getEntity().getContent();
//
//			// convert inputstream to string
//			if (inputStream != null)
//				result = convertInputStreamToString(inputStream);
//			else
//				result = "Did not work!";
//
//		} catch (Exception e) {
//			Log.d("InputStream", e.getLocalizedMessage());
//		}
//
//		return result;
//	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}

	private class HttpAsyncTaskFaq extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(FaqActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(FaqActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if(result.contains("[")) {
					JSONParser parser = new JSONParser();
	 				String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
					Object obj = parser.parse(finalResult);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					faqList = sGson.fromJson(array.toString(), FAQ.getJsonArrayType());
					
					
					
					adapter = new FaqAdapter(FaqActivity.this, R.layout.list_row_faqs, faqList);
					listView.setAdapter(adapter);
					
				} else {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							context);

						// set title
						//alertDialogBuilder.setTitle("Your Title");

						// set dialog message
						alertDialogBuilder
							.setMessage("No record found!")
							.setCancelable(false)
							.setPositiveButton("OK",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// if this button is clicked, close
									// current activity
									FaqActivity.this.finish();
								}
							  });


							// create alert dialog
							AlertDialog alertDialog = alertDialogBuilder.create();

							// show it
							alertDialog.show();
				}
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
		
	}
}

