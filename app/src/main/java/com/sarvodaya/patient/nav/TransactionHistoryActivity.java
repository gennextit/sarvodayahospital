package com.sarvodaya.patient.nav;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.DatabaseHandler;
import com.sarvodaya.patient.MainActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.Contact;
import com.sarvodayahospital.beans.Members;
import com.sarvodayahospital.beans.TransactionHistory;
import com.sarvodayahospital.dialog.CustomDialogCancelAppointment;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionHistoryActivity extends BaseActivity implements CustomDialogCancelAppointment.IntimateTransactionHistory {
    final Context context = this;

    LinearLayout membersParent;
    TextView membersText;
    ProgressDialog dialog;
    List<Members> membersList;
    ListView dialog_ListView;
    List<TransactionHistory> transactionHistoryList;
    TableLayout past_table, todays_table, upcoming_table;
    SimpleDateFormat formatter;
    String[] listContent = {"SELECT MEMBER"};
    Map<String, String> membersMap = new HashMap<String, String>();
    Date date;

    ConnectionDetector cd;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        SpannableString s = new SpannableString("My Transactions");
        s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        getSupportActionBar().setTitle(s);
        membersParent = (LinearLayout) findViewById(R.id.membersParent);
        membersText = (TextView) findViewById(R.id.membersText);
        membersText.setText("SELECT MEMBER");
        past_table = (TableLayout) findViewById(R.id.past_table);
        todays_table = (TableLayout) findViewById(R.id.todays_table);
        upcoming_table = (TableLayout) findViewById(R.id.upcoming_table);

        // Check if Internet present
        cd = new ConnectionDetector(getApplicationContext());


        //select default member
        SharedPreferences app_preferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String member = app_preferences.getString("member", "");
        membersText.setText(member);

        membersParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(TransactionHistoryActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                showDialog(1);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        TaskLoadMembers();

    }

    private void TaskLoadMembers() {
        DatabaseHandler db = new DatabaseHandler(context);
        List<Contact> contactList = db.getAllContacts();
        new HttpAsyncTaskLoadMembers().execute(AppSettings.WEB_SERVICE_URL
                + "LoadPatientData?PatientId=&Mobile=" + contactList.get(0).getPhoneNumber() + "&Landline=");

    }

    @Override
    protected Dialog onCreateDialog(int id) {

        Dialog dialog = null;

        switch (id) {
            case 1:
                dialog = new Dialog(TransactionHistoryActivity.this);
                dialog.setContentView(R.layout.specialitylayout);
                dialog.setTitle("MEMBERS");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(FindDoctorActivity.this, "OnCancelListener",Toast.LENGTH_LONG).show();
                    }
                });

                dialog.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(BookAppointmentActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
                    }
                });

                // Prepare ListView in dialog
                dialog_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        R.layout.listlayout, listContent);
                dialog_ListView.setAdapter(adapter);
                dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        if (parent.getItemAtPosition(position).toString().contentEquals("SELECT MEMBER")) {
                            membersText.setText(parent.getItemAtPosition(position).toString());
                        } else {
                            membersText.setText(parent.getItemAtPosition(position).toString());
                            String patient_id = membersMap.get(membersText.getText());
                            new HttpAsyncTaskLoadAppointmentHistory().execute(AppSettings.WEB_SERVICE_URL
                                    + "LoadAppointmentHistory?Patient_Id=" + patient_id);
                        }
                        removeDialog(1);
                    }
                });

                break;
        }

        return dialog;
    }


    private class HttpAsyncTaskLoadMembers extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            showProgressDialog(TransactionHistoryActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(TransactionHistoryActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    membersList = sGson.fromJson(array.toString(), Members.getJsonArrayType());
                    listContent = new String[membersList.size() + 1];
                    Integer membersCount = 0;
                    membersMap.clear();
                    listContent[membersCount++] = "SELECT MEMBER";
                    for (Members member : membersList) {
                        String tempTitle = (member.getTitle() == null) ? "" : member.getTitle().trim();
                        String tempPName = (member.getPatientName() == null) ? "" : member.getPatientName().trim();

                        listContent[membersCount++] = tempTitle + " " + tempPName;
                        membersMap.put(tempTitle + " " + tempPName, member.getPatientId());
                    }

                    //loading appointment history of default member
                    String patient_id = membersMap.get(membersText.getText().toString().trim());
                    new HttpAsyncTaskLoadAppointmentHistory().execute(AppSettings.WEB_SERVICE_URL
                            + "LoadAppointmentHistory?Patient_Id=" + patient_id);

                } else {
                    RelativeLayout retry = showBaseServerErrorAlertBox(result);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            TaskLoadMembers();
                        }
                    });
                }
                hideProgressDialog();
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
        }
    }

    private class HttpAsyncTaskLoadAppointmentHistory extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(TransactionHistoryActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(TransactionHistoryActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                past_table.removeAllViews();
                todays_table.removeAllViews();
                upcoming_table.removeAllViews();
                past_table.setColumnStretchable(0, false);
                past_table.setColumnStretchable(1, true);
                past_table.setColumnStretchable(2, false);
                past_table.setColumnStretchable(3, true);
                todays_table.setColumnStretchable(0, false);
                todays_table.setColumnStretchable(1, true);
                todays_table.setColumnStretchable(2, false);
                todays_table.setColumnStretchable(3, true);
                upcoming_table.setColumnStretchable(0, false);
                upcoming_table.setColumnStretchable(1, true);
                upcoming_table.setColumnStretchable(2, false);
                upcoming_table.setColumnStretchable(3, true);
                if (!result.contains("Record Not Found")) {
                    if(result.contains("[")) {
                        JSONParser parser = new JSONParser();
                        String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                        Object obj = parser.parse(finalResult);
                        JSONArray array = (JSONArray) obj;
                        Gson sGson = new Gson();
                        transactionHistoryList = sGson.fromJson(array.toString(), TransactionHistory.getJsonArrayType());
                        formatter = new SimpleDateFormat("dd-MMM-yyyy");
                        SimpleDateFormat newFormatter = new SimpleDateFormat("dd MMM yyy, E");
                        for (final TransactionHistory th : transactionHistoryList) {

                            try {
                                date = formatter.parse(th.getAppDate());
                                Date todaysDate = setTimeToMidnight(new Date());
                                final String appId = th.getAppID();
                                final String doctorId = th.getDoctorId();
                                final String appTime = th.getAppTime();

                                if (date.equals(todaysDate)) { //today
                                    TableRow tr = new TableRow(context);
                                    tr.setGravity(Gravity.LEFT);
                                    tr.setLayoutParams(new LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            LayoutParams.WRAP_CONTENT));
                                    tr.setBackgroundResource(R.drawable.layout_border_top);

                                    TextView borderTV = new TextView(context);

                                    borderTV.setText("1");// TUESDAY - 2:00 PM - 4:00 PM");
                                    borderTV.setTextColor(Color.parseColor("#a6ad12"));
                                    borderTV.setPadding(0, 25, 0, 25);
                                    borderTV.setBackgroundColor(Color.parseColor("#a6ad12"));

                                    borderTV.setLayoutParams(new LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            LayoutParams.WRAP_CONTENT));
                                    tr.addView(borderTV);

                                    TextView labelTV = new TextView(context);

                                    labelTV.setText(th.getDoctorName());// TUESDAY - 2:00 PM - 4:00 PM");
                                    labelTV.setTextColor(Color.parseColor("#a6ad12"));
                                    labelTV.setGravity(Gravity.LEFT);
                                    labelTV.setMaxLines(3);
                                    labelTV.setTypeface(null, Typeface.BOLD);
                                    labelTV.setPadding(15, 15, 15, 15);

                                    labelTV.setLayoutParams(new LayoutParams(
                                            250,
                                            100));
                                    tr.addView(labelTV);

                                    TextView borderTV1 = new TextView(context);

                                    borderTV1.setText("'");// TUESDAY - 2:00 PM - 4:00 PM");
                                    borderTV1.setTextColor(Color.parseColor("#ffffff"));
                                    borderTV1.setGravity(Gravity.LEFT);
                                    borderTV1.setTypeface(null, Typeface.BOLD);
                                    borderTV1.setPadding(0, 25, 0, 25);
                                    borderTV1.setBackgroundResource(R.drawable.layout_border_row);


                                    borderTV1.setLayoutParams(new LayoutParams(
                                            LayoutParams.WRAP_CONTENT,
                                            LayoutParams.MATCH_PARENT));
                                    tr.addView(borderTV1);

                                    TextView dateTV = new TextView(context);

                                    if (th.getAmount() > 0) {
                                        dateTV.setText(newFormatter.format(date) + "\n" + th.getAppTime() + "\n ReceiptNo : " + th.getReceiptNo());// TUESDAY - 2:00 PM - 4:00 PM");
                                    } else {
                                        dateTV.setText(newFormatter.format(date) + "\n" + th.getAppTime());// TUESDAY - 2:00 PM - 4:00 PM");
                                    }
                                    dateTV.setTextColor(Color.parseColor("#373737"));
                                    dateTV.setGravity(Gravity.CENTER);
                                    dateTV.setPadding(10, 0, 15, 0);

                                    LayoutParams params = new LayoutParams(
                                            LayoutParams.WRAP_CONTENT,
                                            LayoutParams.MATCH_PARENT
                                    );
                                    params.setMargins(15, 0, 15, 0);
                                    dateTV.setLayoutParams(params);
                                    tr.addView(dateTV);

                                    todays_table.addView(tr, new TableLayout.LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            LayoutParams.WRAP_CONTENT));

                                    tr.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            CustomDialogCancelAppointment cdd;
                                            try {
                                                cdd = new CustomDialogCancelAppointment(TransactionHistoryActivity.this, context, doctorId, appId, formatter.parse(th.getAppDate()), appTime, TransactionHistoryActivity.this);
                                                cdd.show(getFragmentManager(), "dialog");
                                            } catch (java.text.ParseException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                } else if (date.before(todaysDate)) { //past
                                    TableRow tr = new TableRow(context);
                                    tr.setGravity(Gravity.LEFT);
                                    tr.setLayoutParams(new LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            LayoutParams.WRAP_CONTENT));
                                    tr.setBackgroundResource(R.drawable.layout_border_top);

                                    TextView borderTV = new TextView(context);

                                    borderTV.setText("1");// TUESDAY - 2:00 PM - 4:00 PM");
                                    borderTV.setTextColor(Color.parseColor("#ec1735"));
                                    borderTV.setPadding(0, 25, 0, 25);
                                    borderTV.setBackgroundColor(Color.parseColor("#ec1735"));

                                    borderTV.setLayoutParams(new LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            LayoutParams.WRAP_CONTENT));
                                    tr.addView(borderTV);

                                    TextView labelTV = new TextView(context);

                                    labelTV.setText(th.getDoctorName());// TUESDAY - 2:00 PM - 4:00 PM");
                                    labelTV.setTextColor(Color.parseColor("#ec1735"));
                                    labelTV.setGravity(Gravity.LEFT);
                                    labelTV.setMaxLines(3);
                                    labelTV.setTypeface(null, Typeface.BOLD);
                                    labelTV.setPadding(15, 15, 15, 15);

                                    labelTV.setLayoutParams(new LayoutParams(
                                            250,
                                            100));
                                    tr.addView(labelTV);

                                    TextView borderTV1 = new TextView(context);

                                    borderTV1.setText("'");// TUESDAY - 2:00 PM - 4:00 PM");
                                    borderTV1.setTextColor(Color.parseColor("#ffffff"));
                                    borderTV1.setGravity(Gravity.LEFT);
                                    borderTV1.setTypeface(null, Typeface.BOLD);
                                    borderTV1.setPadding(0, 25, 0, 25);
                                    borderTV1.setBackgroundResource(R.drawable.layout_border_row);


                                    borderTV1.setLayoutParams(new LayoutParams(
                                            LayoutParams.WRAP_CONTENT,
                                            LayoutParams.MATCH_PARENT));
                                    tr.addView(borderTV1);

                                    TextView dateTV = new TextView(context);

                                    if (th.getAmount() > 0) {
                                        dateTV.setText(newFormatter.format(date) + "\n" + th.getAppTime() + "\n ReceiptNo : " + th.getReceiptNo());// TUESDAY - 2:00 PM - 4:00 PM");
                                    } else {
                                        dateTV.setText(newFormatter.format(date) + "\n" + th.getAppTime());// TUESDAY - 2:00 PM - 4:00 PM");
                                    }
                                    dateTV.setTextColor(Color.parseColor("#373737"));
                                    dateTV.setGravity(Gravity.CENTER);
                                    dateTV.setPadding(10, 0, 15, 0);

                                    LayoutParams params = new LayoutParams(
                                            LayoutParams.WRAP_CONTENT,
                                            LayoutParams.MATCH_PARENT
                                    );
                                    params.setMargins(15, 0, 15, 0);
                                    dateTV.setLayoutParams(params);
                                    tr.addView(dateTV);

                                    past_table.addView(tr, new TableLayout.LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            LayoutParams.WRAP_CONTENT));

                                    tr.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //Toast.makeText(TransactionHistoryActivity.this, appId ,Toast.LENGTH_LONG).show();
                                        }
                                    });
                                } else { //upcoming
                                    TableRow tr = new TableRow(context);
                                    tr.setGravity(Gravity.LEFT);
                                    tr.setLayoutParams(new LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            LayoutParams.WRAP_CONTENT));
                                    tr.setBackgroundResource(R.drawable.layout_border_top);

                                    TextView borderTV = new TextView(context);

                                    borderTV.setText("1");// TUESDAY - 2:00 PM - 4:00 PM");
                                    borderTV.setTextColor(Color.parseColor("#f88324"));
                                    borderTV.setPadding(0, 25, 0, 25);
                                    borderTV.setBackgroundColor(Color.parseColor("#f88324"));

                                    borderTV.setLayoutParams(new LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            LayoutParams.WRAP_CONTENT));
                                    tr.addView(borderTV);

                                    TextView labelTV = new TextView(context);

                                    labelTV.setText(th.getDoctorName());// TUESDAY - 2:00 PM - 4:00 PM");
                                    labelTV.setTextColor(Color.parseColor("#f88324"));
                                    labelTV.setGravity(Gravity.LEFT);
                                    labelTV.setMaxLines(3);
                                    labelTV.setTypeface(null, Typeface.BOLD);
                                    labelTV.setPadding(15, 15, 15, 15);

                                    labelTV.setLayoutParams(new LayoutParams(
                                            250,
                                            100));
                                    tr.addView(labelTV);

                                    TextView borderTV1 = new TextView(context);

                                    borderTV1.setText("'");// TUESDAY - 2:00 PM - 4:00 PM");
                                    borderTV1.setTextColor(Color.parseColor("#ffffff"));
                                    borderTV1.setGravity(Gravity.LEFT);
                                    borderTV1.setTypeface(null, Typeface.BOLD);
                                    borderTV1.setPadding(0, 25, 0, 25);
                                    borderTV1.setBackgroundResource(R.drawable.layout_border_row);


                                    borderTV1.setLayoutParams(new LayoutParams(
                                            LayoutParams.WRAP_CONTENT,
                                            LayoutParams.MATCH_PARENT));
                                    tr.addView(borderTV1);

                                    TextView dateTV = new TextView(context);

                                    if (th.getAmount() > 0) {
                                        dateTV.setText(newFormatter.format(date) + "\n" + th.getAppTime() + "\n Receipt : " + th.getReceiptNo());// TUESDAY - 2:00 PM - 4:00 PM");
                                    } else {
                                        dateTV.setText(newFormatter.format(date) + "\n" + th.getAppTime());// TUESDAY - 2:00 PM - 4:00 PM");
                                    }
                                    dateTV.setTextColor(Color.parseColor("#373737"));
                                    dateTV.setGravity(Gravity.CENTER);
                                    dateTV.setPadding(10, 0, 15, 0);

                                    LayoutParams params = new LayoutParams(
                                            LayoutParams.WRAP_CONTENT,
                                            LayoutParams.MATCH_PARENT
                                    );
                                    params.setMargins(15, 0, 15, 0);
                                    dateTV.setLayoutParams(params);
                                    tr.addView(dateTV);

                                    upcoming_table.addView(tr, new TableLayout.LayoutParams(
                                            LayoutParams.MATCH_PARENT,
                                            LayoutParams.WRAP_CONTENT));

                                    tr.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            CustomDialogCancelAppointment cdd;
                                            try {
                                                cdd = new CustomDialogCancelAppointment(TransactionHistoryActivity.this, context, doctorId, appId, formatter.parse(th.getAppDate()), appTime, TransactionHistoryActivity.this);
                                                cdd.show(getFragmentManager(), "dialog");
                                            } catch (java.text.ParseException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                }

                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }
                            //System.out.println(date);
                            //System.out.println(formatter.format(date));


                        }
                    }else{
                        RelativeLayout retry=showBaseServerErrorAlertBox(result);
                        retry.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                hideBaseServerErrorAlertBox();
                                TaskLoadMembers();
                            }
                        });
                    }
                }
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
//			dialog.dismiss();
            hideProgressDialog();
        }
    }

    public static Date setTimeToMidnight(Date date) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TransactionHistoryActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        TransactionHistoryActivity.this.finish();
        startActivity(intent);
    }

    @Override
    public void callLoadMemeber() {
        DatabaseHandler db = new DatabaseHandler(context);
        List<Contact> contactList = db.getAllContacts();

        new HttpAsyncTaskLoadMembers().execute(AppSettings.WEB_SERVICE_URL
                + "LoadPatientData?PatientId=&Mobile=" + contactList.get(0).getPhoneNumber() + "&Landline=");
    }
}
