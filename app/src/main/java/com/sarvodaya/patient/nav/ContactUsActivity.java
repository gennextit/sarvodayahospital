package com.sarvodaya.patient.nav;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.DatabaseHandler;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.Contact;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class ContactUsActivity extends BaseActivity {
	final Context context = this;
	LinearLayout mobileHeadingParent, mobileParent;
	EditText nameText, emailText, mobileText, feedbackText, reasonText;
	ImageView submitButton;
	ProgressDialog dialog;
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_us);
		SpannableString s = new SpannableString("Contact Us");
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
		            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		
		
		mobileHeadingParent = (LinearLayout) findViewById(R.id.mobileHeadingParent);
		mobileParent = (LinearLayout) findViewById(R.id.mobileParent);
		nameText = (EditText) findViewById(R.id.nameText);
		emailText = (EditText) findViewById(R.id.emailText);
		mobileText = (EditText) findViewById(R.id.mobileText);
		feedbackText = (EditText) findViewById(R.id.feedbackText);
		reasonText = (EditText) findViewById(R.id.reasonText);
		submitButton = (ImageView) findViewById(R.id.submitButton);
		mobileHeadingParent.setVisibility(View.GONE);
		mobileParent.setVisibility(View.GONE);
		
		feedbackText.setHorizontallyScrolling(false);
		feedbackText.setMaxLines(Integer.MAX_VALUE);
		reasonText.setHorizontallyScrolling(false);
		reasonText.setMaxLines(Integer.MAX_VALUE);
		// Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());
				
		DatabaseHandler db = new DatabaseHandler(context);
		List<Contact> contactList = db.getAllContacts();
		nameText.setText(contactList.get(0).getName());
		mobileText.setText(contactList.get(0).getPhoneNumber());
		final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
		
		
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(nameText.getText().length() == 0) {
					Toast.makeText(getApplicationContext(), "Enter name.", Toast.LENGTH_LONG).show();
					nameText.requestFocusFromTouch();
			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE); 
			        lManager.showSoftInput(nameText, 0);
				} else if(mobileText.getText().length() == 0) {
					Toast.makeText(getApplicationContext(), "Enter mobile number.", Toast.LENGTH_LONG).show();
					mobileText.requestFocusFromTouch();
			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE); 
			        lManager.showSoftInput(mobileText, 0);
				} else if(emailText.getText().length() == 0) {
					Toast.makeText(getApplicationContext(), "Enter email.", Toast.LENGTH_LONG).show();
					emailText.requestFocusFromTouch();
			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE); 
			        lManager.showSoftInput(emailText, 0);
				} else if(!emailText.getText().toString().matches(emailPattern)) {
					Toast.makeText(getApplicationContext(), "Enter valid email.", Toast.LENGTH_LONG).show();
					emailText.requestFocusFromTouch();
			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE); 
			        lManager.showSoftInput(emailText, 0);
				} else if(feedbackText.getText().length() == 0) {
					Toast.makeText(getApplicationContext(), "Enter comments.", Toast.LENGTH_LONG).show();
					mobileText.requestFocusFromTouch();
			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE); 
			        lManager.showSoftInput(feedbackText, 0);
				} else {
					isInternetPresent = cd.isConnectingToInternet();
					if (!isInternetPresent) {
						// Internet Connection is not present
						alert.showAlertDialog(ContactUsActivity.this, "Internet Connection Error",
								"Please connect to working Internet connection", false);
						// stop executing code by return
						return;
					}
					
					String url = AppSettings.WEB_SERVICE_URL + "ContactUs?Name=" + nameText.getText().toString()
							+ "&Mobile=" + mobileText.getText().toString()
							+ "&EmailID=" + emailText.getText().toString().trim()
							+ "&Comments=" + feedbackText.getText().toString().trim()
							+ "&Reason=" + reasonText.getText().toString().trim();
					
					url = url.replace(" ", "%20");
					new HttpAsyncWriteFeedback().execute(url);
				}
			
			}
		});
		
		
	}
	
//	public static String GET(String url) {
//		InputStream inputStream = null;
//		String result = "";
//		try {
//
//			// create HttpClient
//			HttpClient httpclient = new DefaultHttpClient();
//
//			// make GET request to the given URL
//			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
//
//			// receive response as inputStream
//			inputStream = httpResponse.getEntity().getContent();
//
//			// convert inputstream to string
//			if (inputStream != null)
//				result = convertInputStreamToString(inputStream);
//			else
//				result = "Did not work!";
//
//		} catch (Exception e) {
//			Log.d("InputStream", e.getLocalizedMessage());
//		}
//
//		return result;
//	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}
	
	private class HttpAsyncWriteFeedback extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(ContactUsActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(ContactUsActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				DatabaseHandler db = new DatabaseHandler(context);
				if(result.contains("Mail Sent")) {
					Toast.makeText(getApplicationContext(), "Mail sent!", Toast.LENGTH_LONG).show();
					finish();
				} else {
					Toast.makeText(getApplicationContext(), "Unable to send feedback, Please try again!", Toast.LENGTH_LONG).show();
				}
			} catch (Exception pe) {
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
	}
	
}
