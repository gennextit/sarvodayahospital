package com.sarvodaya.patient.nav;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.HospitalLocation;
import com.sarvodayahospital.doctor.adapter.GetDirectionsAdapter;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import java.util.ArrayList;
import java.util.List;


public class GetDirectionsActivity extends BaseActivity {
	final Context context = this;
	ListView listView;
	private GetDirectionsAdapter adapter;
	List<HospitalLocation> locationList = new ArrayList<HospitalLocation>();

	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_directions);
		SpannableString s = new SpannableString("Get Directions");
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		listView = (ListView) findViewById(R.id.lv_main);

		// Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());

		HospitalLocation h1 = new HospitalLocation("Sarvodaya Hospital & Research Center", "Sector 8", 28.367358, 77.336149);
		HospitalLocation h2 = new HospitalLocation("Sarvodaya Hospital", "Sector 19", 28.422451, 77.316924);
		locationList.add(h1);
		locationList.add(h2);

		adapter = new GetDirectionsAdapter(GetDirectionsActivity.this, R.layout.list_row_get_directions, locationList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
									long id) {
				isInternetPresent = cd.isConnectingToInternet();
				if (!isInternetPresent) {
					// Internet Connection is not present
					alert.showAlertDialog(GetDirectionsActivity.this, "Internet Connection Error",
							"Please connect to working Internet connection", false);
					// stop executing code by return
					return;
				}
				Intent intent1 = new Intent(GetDirectionsActivity.this, GoogleMapActivity.class);
				intent1.putExtra("name", locationList.get(position).getName());
				intent1.putExtra("location", locationList.get(position).getLocation());
				intent1.putExtra("latitude", locationList.get(position).getLalitute());
				intent1.putExtra("longitude", locationList.get(position).getLongitude());
				startActivity(intent1);
			}
		});
	}
}
