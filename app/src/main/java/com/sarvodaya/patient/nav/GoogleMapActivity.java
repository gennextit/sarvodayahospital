package com.sarvodaya.patient.nav;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;

import java.util.ArrayList;
import java.util.List;

public class GoogleMapActivity extends BaseActivity implements OnMapReadyCallback {

	String name, locationName;
	Double latitude, longitude;
	private GoogleMap map;
	private FloatingActionButton fab;
	private PermissionListener onPermissionListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.frag_map_preview);

		name = getIntent().getStringExtra("name");
		locationName = getIntent().getStringExtra("location");
		latitude = getIntent().getDoubleExtra("latitude", 0);
		longitude = getIntent().getDoubleExtra("longitude", 0);
		fab = (FloatingActionButton)findViewById(R.id.fab);

		SupportMapFragment mapFragment =
				(SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

		onPermissionListener = new PermissionListener() {
			@Override
			public void onPermissionGranted() {
				if (ActivityCompat.checkSelfPermission(GoogleMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(GoogleMapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
					return;
				}
				showLocationOnMap(latitude,longitude,locationName);
			}

			@Override
			public void onPermissionDenied(List<String> deniedPermissions) {
				Toast.makeText(getApplicationContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();

			}

		};
	}

	private void showLocationOnMap(Double latitude,Double longitude,String address) {



//        double latitude= Double.parseDouble(lat);
//        double longitude=Double.parseDouble(lng);
		// create marker
		MarkerOptions marker = new MarkerOptions().position(
				new LatLng(latitude, longitude)).title(address);

//	    // Changing marker icon
//	    marker.icon(BitmapDescriptorFactory
//	            .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

		// adding marker
		map.addMarker(marker).showInfoWindow();
		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(new LatLng(latitude, longitude)).zoom(14).build();
		map.animateCamera(CameraUpdateFactory
				.newCameraPosition(cameraPosition));

		// Perform any camera updates here

	}


	@Override
	public void onMapReady(GoogleMap googleMap) {
		map = googleMap;
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        map.getUiSettings().setZoomControlsEnabled(true);
		map.getUiSettings().setMapToolbarEnabled(false);

		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				// Display a label at the location of Google's Sydney office
				openGoogleNavagation();
			}
		});

		TedPermission.with(GoogleMapActivity.this)
				.setPermissionListener(onPermissionListener)
				.setDeniedMessage(getString(R.string.permission_denied_explanation))
				.setRationaleMessage(getString(R.string.permission_rationale))
				.setGotoSettingButtonText("setting")
				.setPermissions(android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION)
				.check();
	}

	private void openGoogleNavagation() {
		String address=getValidUrl(name);
		String gmmIntentUri = "geo:0,0?q="+latitude+","+longitude+"("+address+"\n"+locationName+")";

		Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(gmmIntentUri));
		intent.setPackage("com.google.android.apps.maps");
		try {
			startActivity(intent);
		} catch (ActivityNotFoundException ex) {

			Toast.makeText(GoogleMapActivity.this, "Please install a maps application", Toast.LENGTH_LONG).show();

		}
	}

	private String getValidUrl(String locationName) {
		return locationName.replaceAll("&","And");
	}

}
