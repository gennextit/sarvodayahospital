package com.sarvodaya.patient;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.sarvodayahospital.util.AppUser;
import com.sarvodayahospital.util.L;
import com.sarvodayahospital.util.TypefaceSpan;
import com.sarvodayahospital.util.imageMaster.FileUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.List;

public class ImageMaster extends AppCompatActivity {

    private static final int IMAGE_CAMERA = 1;
    private static final int IMAGE_PICKER = 3;
    public static final int REQUEST_PROFILE = 101;
    private ImageView ivPreview;
    private Uri mImageUri;
    private Dialog dialog;
    private TextView tvUri;
    private ImageView ivPlus;
    private PermissionListener permissionlistener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_master);
        SpannableString s = new SpannableString("Profile");
        s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        getSupportActionBar().setTitle(s);
        initUi();
        setProfileImage();

//        OptionImageAlert(this);
//        ImageMasterPermissionsDispatcher.OptionImageAlertWithCheck(ImageMaster.this,this);
    }

    private void setProfileImage() {
        String encodedImage=AppUser.getProfileImage(getApplicationContext());
        if(!TextUtils.isEmpty(encodedImage)) {
            Bitmap bmp=FileUtils.convertEncodedStringToImageBitmap(encodedImage);
            ivPreview.setImageBitmap(bmp);
        } else {
            ivPreview.setImageResource(R.drawable.profile);
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mImageUri!=null) {
            outState.putString("mImageUri", mImageUri.toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mImageUri=Uri.parse(savedInstanceState.getString("mImageUri"));
    }


    private void initUi() {
        ivPreview = (ImageView) findViewById(R.id.iv_preview);
        ivPlus = (ImageView) findViewById(R.id.iv_plus);
        tvUri = (TextView) findViewById(R.id.tvUri);
        Button btnDone = (Button) findViewById(R.id.btn_done);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mImageUri != null) {
//                    tvUri.setText(mImageUri.toString());
                    startActivityForResult(mImageUri);
                } else {
                    tvUri.setText("No Image found");
                }
            }
        });
        ivPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionImageAlertWithCheck();
            }
        });
        ivPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionImageAlertWithCheck();
            }
        });
        permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                OptionImageAlert(ImageMaster.this);
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(getApplicationContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void startActivityForResult(Uri mImageUri) {
        Intent backIntent = new Intent();
        backIntent.setData(mImageUri);
        setResult(REQUEST_PROFILE, backIntent);
        finish();
    }

    public void OptionImageAlertWithCheck(){
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }
    public void OptionImageAlert(Activity Act) {
        tvUri.setText("");
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
        LayoutInflater inflater = Act.getLayoutInflater();
        View v = inflater.inflate(R.layout.alert_image_option, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.button1);
        Button button2 = (Button) v.findViewById(R.id.button2);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                mImageUri = FileUtils.getOutputMediaFileUri("ImageMaster");
                if (mImageUri != null) {
                    intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageUri);
                }
                try {
                    intent.putExtra("return-data", true);
                    /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);*/
                    startActivityForResult(intent, IMAGE_CAMERA);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent target = FileUtils.createGetContentIntent();
                // Create the chooser Intent
                Intent intent = Intent.createChooser(target, "Select Image");
                try {
                    startActivityForResult(intent, IMAGE_PICKER);
                } catch (ActivityNotFoundException e) {
                    // The reason for the existence of aFileChooser
                }

            }
        });
        dialog = dialogBuilder.create();
        dialog.show();

    }

    public void OpenFilePicker() {
        Intent target = FileUtils.createGetContentIntent();
        // Create the chooser Intent
        Intent intent = Intent.createChooser(target, "Select Image");
        try {
            startActivityForResult(intent, IMAGE_PICKER);
        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
        }
    }




    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                setImageAndStore(result.getUri());
//                mImageUri = result.getUri();
//                ivPreview.setImageURI(mImageUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, "Cropping failed: please select a valid Image" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case IMAGE_CAMERA:
                    startCropImageActivity(mImageUri);
                    break;

                case IMAGE_PICKER:
                    if (data != null) {
                        // Get the URI of the selected file
                        final Uri uri = data.getData();
                        try {
                            mImageUri = uri;
                            startCropImageActivity(uri);
                        } catch (Exception e) {
                            L.m("FileSelectorTestActivity" + "File select error" + e.toString());
                        }
                    }
                    break;
            }
        }
    }

    private void setImageAndStore(Uri uri) {
        mImageUri = uri;
        ivPreview.setImageURI(mImageUri);
        Bitmap photo = FileUtils.uriToBitmap(ImageMaster.this, mImageUri);

        String encodedImage = FileUtils.convertImageBitmapToString(photo);
        AppUser.setProfileImage(getApplicationContext(), encodedImage);
    }

    private void startCropImageActivity(Uri imageUri) {
        if(imageUri!=null) {
            CropImage.activity(imageUri)
                    .setAspectRatio(1, 1)
                    .setFixAspectRatio(true)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }else {
            Toast.makeText(ImageMaster.this, "Please Reselect image ", Toast.LENGTH_SHORT).show();
        }
    }
}
