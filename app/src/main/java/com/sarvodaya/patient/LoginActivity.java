package com.sarvodaya.patient;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ebs.android.sdk.PaymentRequest;
import com.google.gson.Gson;
import com.sarvodayahospital.beans.Contact;
import com.sarvodayahospital.beans.OTP;
import com.sarvodayahospital.beans.Patient;
import com.sarvodayahospital.dialog.CustomDialogOTP;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.PatientApp;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import java.util.List;

public class LoginActivity extends BaseActivity implements CustomDialogOTP.IntimateAddMember{
	final Context context = this;
	String[] titleList = {  "Mr.", "Mrs.","Ms.", "Dr.", "Baby", "Mast."};
	LinearLayout titleParent, titleHeadingParent, ageHeadingParent, ageParent, genderHeadingParent, genderParent;
	TextView titleText;
	ListView title_ListView;
	ImageView confirm_button;
	EditText nameText, ageText, mobileText;
	private RadioGroup radioSexGroup;
	private RadioButton radioSexButton;
	private RadioButton radioMale, radioFemale;
	ProgressDialog dialog;
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
	List<Patient> membersList;
	String otp = "";
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		setHeading("Sign Up");

		titleParent = (LinearLayout) findViewById(R.id.titleParent);
		titleHeadingParent = (LinearLayout) findViewById(R.id.titleHeadingParent);
		ageHeadingParent = (LinearLayout) findViewById(R.id.ageHeadingParent);
		ageParent = (LinearLayout) findViewById(R.id.ageParent);
		genderHeadingParent = (LinearLayout) findViewById(R.id.genderHeadingParent);
		genderParent = (LinearLayout) findViewById(R.id.genderParent);
		
		nameText = (EditText) findViewById(R.id.nameText);
		ageText = (EditText) findViewById(R.id.ageText);
		mobileText = (EditText) findViewById(R.id.mobileText);
		radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
		radioMale = (RadioButton) findViewById(R.id.radioMale);
		radioFemale = (RadioButton) findViewById(R.id.radioFemale);
		confirm_button = (ImageView) findViewById(R.id.confirm_button);
		confirm_button.setBackgroundResource(R.drawable.confirm_button_orange);
		titleText = (TextView) findViewById(R.id.titleText);
		titleText.setText("---Select title---");
		titleParent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(1);
				
			}
		});
		titleHeadingParent.setVisibility(View.GONE);
		titleParent.setVisibility(View.GONE);
		ageHeadingParent.setVisibility(View.GONE);
		ageParent.setVisibility(View.GONE);
		genderHeadingParent.setVisibility(View.GONE);
		genderParent.setVisibility(View.GONE);
		titleText.setText("Mr.");
		ageText.setText("0");
		radioMale.setChecked(true);
		// Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());
		
		
		confirm_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {	
//				if(titleText.getText().toString().contentEquals("---Select title---")) {
//					Toast.makeText(getApplicationContext(), "Select title.", Toast.LENGTH_LONG).show();
//				} else if(nameText.getText().length() == 0) {
//					Toast.makeText(getApplicationContext(), "Enter name of patient.", Toast.LENGTH_LONG).show();
//					nameText.requestFocusFromTouch();
//			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//			        lManager.showSoftInput(nameText, 0);
//				} else if(ageText.getText().length() == 0) {
//					Toast.makeText(getApplicationContext(), "Enter age.", Toast.LENGTH_LONG).show();
//					ageText.requestFocusFromTouch();
//			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//			        lManager.showSoftInput(ageText, 0);
//				} else if(mobileText.getText().length() == 0) {
//					Toast.makeText(getApplicationContext(), "Enter mobile number.", Toast.LENGTH_LONG).show();
//					mobileText.requestFocusFromTouch();
//			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//			        lManager.showSoftInput(mobileText, 0);
//				} else {
//					isInternetPresent = cd.isConnectingToInternet();
//					if (!isInternetPresent) {
//						// Internet Connection is not present
//						alert.showAlertDialog(LoginActivity.this, "Internet Connection Error",
//								"Please connect to working Internet connection", false);
//						// stop executing code by return
//						return;
//					}
//
//					String otpURL = AppSettings.WEB_SERVICE_URL + "GenerateOTP?Phone=" + mobileText.getText().toString();
//					new HttpAsyncGenerateOTP().execute(otpURL);
//				}

				executeTask();
			}
		});
		
	}

	private void executeTask() {
		if(nameText.getText().length() == 0) {
			Toast.makeText(getApplicationContext(), "Enter name of patient.", Toast.LENGTH_LONG).show();
			nameText.requestFocusFromTouch();
			InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			lManager.showSoftInput(nameText, 0);
		} else if(mobileText.getText().length() == 0) {
			Toast.makeText(getApplicationContext(), "Enter mobile number.", Toast.LENGTH_LONG).show();
			mobileText.requestFocusFromTouch();
			InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			lManager.showSoftInput(mobileText, 0);
		} else {
			isInternetPresent = cd.isConnectingToInternet();
			if (!isInternetPresent) {
				// Internet Connection is not present
				alert.showAlertDialog(LoginActivity.this, "Internet Connection Error",
						"Please connect to working Internet connection", false);
				// stop executing code by return
				return;
			}

			String otpURL = AppSettings.WEB_SERVICE_URL + "GenerateOTP?Phone=" + mobileText.getText().toString();
			new HttpAsyncGenerateOTP().execute(otpURL);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog dialog = null;

		switch (id) {
		case 1:
			dialog = new Dialog(LoginActivity.this);
			dialog.setContentView(R.layout.specialitylayout);
			dialog.setTitle("Title");

			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);

			dialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//Toast.makeText(FindDoctorActivity.this, "OnCancelListener",Toast.LENGTH_LONG).show();
				}
			});

			dialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
				}
			});

			// Prepare ListView in dialog
			title_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
			
			ArrayAdapter<String> adapterTitle = new ArrayAdapter<String>(this,
					R.layout.listlayout, titleList);
			title_ListView.setAdapter(adapterTitle);
			
			title_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					titleText.setText(parent.getItemAtPosition(position).toString());
					if(titleText.getText().toString().contentEquals("Mrs.") || titleText.getText().toString().contentEquals("Ms.")) {
						radioFemale.setChecked(true);
					} else {
						radioMale.setChecked(true);
					}
					removeDialog(1);
				}
			});

			break;
		}

		return dialog;
	}
	

	
	private class HttpAsyncTaskAddMember extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(LoginActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(LoginActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				DatabaseHandler db = new DatabaseHandler(context);
				if(result.contains("\"PatientID")) {
					Toast.makeText(getApplicationContext(), "Member added successfully.", Toast.LENGTH_LONG).show();
					String patientId = result.substring(result.lastIndexOf(":") + 2, result.lastIndexOf("\""));
					
					//saving to database
					db.addContact(new Contact(nameText.getText().toString().trim(), mobileText.getText().toString().trim(), patientId, titleText.getText().toString(), "", ageText.getText().toString(), "", "", "", radioSexButton.getText().toString()));
					
					//adding member to session
					SharedPreferences app_preferences = PreferenceManager
							.getDefaultSharedPreferences(context);

					SharedPreferences.Editor editor = app_preferences.edit();
					editor.putString("member", titleText.getText().toString().trim() + " " + nameText.getText().toString().trim());
					editor.commit();
					
					//starting new activity
					Intent i = new Intent(LoginActivity.this, MainActivity.class);
					startActivity(i);
					finish();
				} else {
					Toast.makeText(getApplicationContext(), "Login error.", Toast.LENGTH_LONG).show();
				}
			} catch (Exception pe) {
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
	}
	
	private class HttpAsyncGenerateOTP extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(LoginActivity.this, "Trying to Login, Keep patience!");
//            dialog= new ProgressDialog(LoginActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Trying to Login, Keep patience!");
//            dialog.show();
                             
        }@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				DatabaseHandler db = new DatabaseHandler(context);
				
				if(result.contains("Patient_Id")) {
					if(result.contains("[")) {
						JSONParser parser = new JSONParser();
						String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
						Object obj = parser.parse(finalResult);
						JSONArray array = (JSONArray) obj;
						Gson sGson = new Gson();
						membersList = sGson.fromJson(array.toString(), Patient.getJsonArrayType());

						//saving to database
						if (membersList.get(0).getMobile().length() == 10) {
							db.addContact(new Contact(membersList.get(0).getPatientName(), membersList.get(0).getMobile(), membersList.get(0).getPatientId(), membersList.get(0).getTitle(), membersList.get(0).getAddress(), membersList.get(0).getAge(), membersList.get(0).getCountry(), membersList.get(0).getCity(), membersList.get(0).getLocality(), membersList.get(0).getGender()));
						} else {
							db.addContact(new Contact(membersList.get(0).getPatientName(), membersList.get(0).getMobile().substring(1), membersList.get(0).getPatientId(), membersList.get(0).getTitle(), membersList.get(0).getAddress(), membersList.get(0).getAge(), membersList.get(0).getCountry(), membersList.get(0).getCity(), membersList.get(0).getLocality(), membersList.get(0).getGender()));
						}
						//adding member to session
						SharedPreferences app_preferences = PreferenceManager
								.getDefaultSharedPreferences(context);

						SharedPreferences.Editor editor = app_preferences.edit();
						String memName = membersList.get(0).getTitle() + " " + membersList.get(0).getPatientName();
						editor.putString("member", memName);
						editor.commit();
						PatientApp.setPatrintData(LoginActivity.this, membersList.get(0).getPatientId(), memName, membersList.get(0).getMobile());

						//starting new activity
						Intent i = new Intent(LoginActivity.this, MainActivity.class);
						startActivity(i);
						finish();
					}else{
//						showToast(result);
						RelativeLayout retry=showBaseServerErrorAlertBox(result);
						retry.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								hideBaseServerErrorAlertBox();
								executeTask();
							}
						});
					}
				} else {
					if(result.contains("[")) {
						JSONParser parser = new JSONParser();
						String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
						Object obj = parser.parse(finalResult);
						JSONArray array = (JSONArray) obj;
						Gson sGson = new Gson();
						List<OTP> otpList = sGson.fromJson(array.toString(), OTP.getJsonArrayType());
						otp = otpList.get(0).getOtp();
						CustomDialogOTP cdd = new CustomDialogOTP(LoginActivity.this, context, otp, LoginActivity.this, mobileText.getText().toString());
						cdd.show();
						cdd.setCancelable(false);
						cdd.setCanceledOnTouchOutside(false);
					}else{
//						showToast(result);
						RelativeLayout retry=showBaseServerErrorAlertBox(result);
						retry.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								hideBaseServerErrorAlertBox();
								executeTask();
							}
						});
					}
				}
			} catch (Exception pe) {
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
	}
	
	@Override
	public void callAddMember() {
		String url = AppSettings.WEB_SERVICE_URL + "AddMember?Title=" + titleText.getText().toString()
				+ "&PatientName=" + nameText.getText().toString().trim()
				+ "&Age=" + ageText.getText().toString() + "%20yrs"
				+ "&Mobile=" + mobileText.getText().toString();
		
		int selectedId = radioSexGroup.getCheckedRadioButtonId();
		radioSexButton = (RadioButton) findViewById(selectedId);
		
		url = url + "&Gender=" + radioSexButton.getText().toString();
		
		url = url + "&Landline=&Address=&Country=&City=&Location=&Relation=Self&RelationName=&Source=MA";
		url = url.replace(" ", "%20");
		new HttpAsyncTaskAddMember().execute(url);
	}
}
