package com.sarvodaya.patient.healthTips;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.HealthTips;
import com.sarvodayahospital.beans.HealthTipsAdapter;
import com.sarvodayahospital.doctor.adapter.GalleryViewAdapter;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.List;

public class HealthTipsActivity extends BaseActivity {
	final Context context = this;
	TableLayout timings_table;
	ProgressDialog dialog; 
	private ListView lvMain;
	private List<HealthTips> listMain;
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
//	private RecyclerView rvDirRefrell;
	private HealthTipsAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_health_tips);
		
		SpannableString s = new SpannableString("Health Tips");
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
		            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		timings_table = (TableLayout) findViewById(R.id.timings_table);
        
		lvMain=(ListView)findViewById(R.id.lv_main);
//		rvDirRefrell = (RecyclerView)findViewById(R.id.rv_main);
//		LinearLayoutManager horizontalManager = new LinearLayoutManager(HealthTipsActivity.this, LinearLayoutManager.VERTICAL, false);
//		rvDirRefrell.setLayoutManager(horizontalManager);
//		rvDirRefrell.setItemAnimator(new DefaultItemAnimator());
//
//		listMain=new ArrayList<>();
//		adapter = new HealthTipsAdapter(HealthTipsActivity.this, listMain);
//		rvDirRefrell.setAdapter(adapter);

//		lvMain.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
//				if(listMain!=null){
//					MasterImageView masterImageView=new MasterImageView();
//					masterImageView.setImage(listMain.get(pos).getImage_url().replaceAll(" ", "%20"));
//					FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
//					transaction.add(android.R.id.content, masterImageView,"masterImageView");
//					transaction.addToBackStack("masterImageView");
//					transaction.commit();
//				}
//			}
//		});
        // Check if Internet present
     	cd = new ConnectionDetector(getApplicationContext());
		TaskLoadHealthTips();
		
		
        
	}

	private void TaskLoadHealthTips() {
		new HttpAsyncTaskLoadHealthTips().execute(AppSettings.HOSPITAL_URL + "getHealthTips");
	}


	private class HttpAsyncTaskLoadHealthTips extends AsyncTask<String, Void, List<HealthTips>> {
		private String errorMsg;

		@Override
        protected void onPreExecute() {
			showProgressDialog(HealthTipsActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(HealthTipsActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected List<HealthTips> doInBackground(String... urls) {
			List<HealthTips> arrayList ;
			String result;//= GET(urls[0]);
			result= ApiCall.GET(urls[0]);
			if(result.contains("[")) {
				try {
					JSONParser parser = new JSONParser(); 
					Object obj;obj = parser.parse(result);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					arrayList = sGson.fromJson(array.toString(), HealthTips.getJsonArrayType());
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					errorMsg=e.toString();
					e.printStackTrace();
					return null;
				}
			}else{
				errorMsg="Invalid Response\n"+result; 
				return null;
			}
				
			return arrayList;
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(List<HealthTips> result) {

				if(result!=null) {  
//					listMain.addAll(result);
//					adapter.notifyDataSetChanged();


					GalleryViewAdapter galleryViewAdapter=new GalleryViewAdapter(HealthTipsActivity.this,R.layout.slot_error,result);
		            lvMain.setAdapter(galleryViewAdapter);
				} else {
					RelativeLayout retry = showBaseServerErrorAlertBox(errorMsg);
					retry.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							hideBaseServerErrorAlertBox();
							TaskLoadHealthTips();
						}
					});
				}
			hideProgressDialog();
		}
	}
	
	
}

