package com.sarvodaya.patient;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.sarvodaya.patient.appointment.MainAppointmentActivity;
import com.sarvodaya.patient.frag.AppNotification;
import com.sarvodaya.patient.global.RatingPopupAlert;
import com.sarvodaya.patient.nav.GetDirectionsActivity;
import com.sarvodaya.patient.appointment.UpdateProfileActivity;
import com.sarvodaya.patient.healthCheckup.HealthPackageActivity;
import com.sarvodaya.patient.healthOffers.HealthOfferActivity;
import com.sarvodaya.patient.healthTips.HealthTipsActivity;
import com.sarvodaya.patient.nav.ContactUsActivity;
import com.sarvodaya.patient.nav.FaqActivity;
import com.sarvodaya.patient.nav.TransactionHistoryActivity;
import com.sarvodaya.patient.nav.WriteFeedbackActivity;
import com.sarvodayahospital.beans.Contact;
import com.sarvodayahospital.beans.NotificationModel;
import com.sarvodayahospital.beans.SideMenu;
import com.sarvodayahospital.dialog.CustomDialogCallEmergency;
import com.sarvodayahospital.dialog.CustomDialogLabReports;
import com.sarvodayahospital.dialog.VersionUpdateDialog;
import com.sarvodayahospital.doctor.adapter.SideMenuAdapter;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.AppUser;
import com.sarvodayahospital.util.CircleTransform;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.Const;
import com.sarvodayahospital.util.DateTimeUtility;
import com.sarvodayahospital.util.JsonParser;
import com.sarvodayahospital.util.Rating;
import com.sarvodayahospital.util.Utility;
import com.sarvodayahospital.util.imageMaster.FileUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements OnClickListener {
    final Context context = this;
    ImageView profile_pic, img_health_tips, img_book_an_appointment, img_lab_reports, img_health_offers, img_book_health_check;
    ImageView img_emergency_sos;
    //	RoundImage roundedImage, roundImageProfileBackground;
    RelativeLayout part1, relativeLayoutProfile;
    TextView name, phoneNo;
    ConnectionDetector cd;
    List<SideMenu> sideMenuList;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    public ImageView menuRightButton;
    DrawerLayout dLayout;
    ListView dList;
    SideMenuAdapter adapter;
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();
    private UpdateAvailability updateTask;
    private PermissionListener permissionlistener;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

//	private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setHeading("sarvodaya");
        FirebaseApp.initializeApp(this);
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        initUi();
        setProfileImage();
        checkUpdateAvailability();
        checkRemoteConfig();
    }

    private void checkUpdateAvailability() {
        updateTask = new UpdateAvailability(MainActivity.this);
        updateTask.execute(AppSettings.APP_UPDATES_AVAILABILITY);
    }

    private void initUi() {
        part1 = (RelativeLayout) findViewById(R.id.part1);
        relativeLayoutProfile = (RelativeLayout) findViewById(R.id.relative_layout_profile);
        profile_pic = (ImageView) findViewById(R.id.img_profile);
//		profile_pic_background = (ImageView) findViewById(R.id.img_profile_background);

        //img_health_tips = (ImageView) findViewById(R.id.img_health_tips);
        img_book_an_appointment = (ImageView) findViewById(R.id.img_book_an_appointment);
        img_lab_reports = (ImageView) findViewById(R.id.img_lab_reports);
        img_emergency_sos = (ImageView) findViewById(R.id.img_emergency_sos);
        img_health_offers = (ImageView) findViewById(R.id.img_health_offers);
        img_book_health_check = (ImageView) findViewById(R.id.img_book_a_health_check);
        img_health_tips = (ImageView) findViewById(R.id.img_health_tips);
        name = (TextView) findViewById(R.id.name);
        phoneNo = (TextView) findViewById(R.id.phoneNo);
        // Slide the Panel
        menuRightButton = (ImageView) findViewById(R.id.menuViewButton);

        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dList = (ListView) findViewById(R.id.left_drawer);


        cd = new ConnectionDetector(getApplicationContext());

//      	SideMenu s1 = new SideMenu("Update Profile", R.drawable.update_profile_32_32);
        SideMenu s2 = new SideMenu("My Transactions", R.drawable.transaction_history_32_32);
        SideMenu s3 = new SideMenu("Get Directions", R.drawable.get_direction_32_32);
        SideMenu s4 = new SideMenu("Provide Feedback", R.drawable.feedback_32_32);
        SideMenu s5 = new SideMenu("Share With Friends", R.drawable.share_32_32);
        SideMenu s6 = new SideMenu("Rate This App", R.drawable.rate_us_32_32);
        SideMenu s7 = new SideMenu("Follow us on Facebook", R.drawable.facebook_32_32);
        SideMenu s8 = new SideMenu("Follow us on Twitter", R.drawable.twitter_32_32);
        SideMenu s9 = new SideMenu("FAQs", R.drawable.faq_32_32);
        SideMenu s10 = new SideMenu("Contact Us", R.drawable.contact_us_32_32);
        SideMenu s11 = new SideMenu("About Us", R.drawable.website_32_32);

        sideMenuList = new ArrayList<SideMenu>();
//      	sideMenuList.add(s1);
        sideMenuList.add(s2);
        sideMenuList.add(s3);
        sideMenuList.add(s4);
        sideMenuList.add(s5);
        sideMenuList.add(s6);
        sideMenuList.add(s7);
        sideMenuList.add(s8);
        sideMenuList.add(s9);
        sideMenuList.add(s10);
        sideMenuList.add(s11);

        adapter = new SideMenuAdapter(MainActivity.this, R.layout.list_row_side_menu, sideMenuList);
        //adapter = new ArrayAdapter<String>(this, R.layout.listlayout, menu);

        dList.setAdapter(adapter);
        dList.setSelector(android.R.color.holo_blue_dark);

        dList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

                dLayout.closeDrawers();
                Bundle args = new Bundle();
                args.putString("Menu", sideMenuList.get(position).getName());


                // Check if Internet present
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(MainActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }

                if (sideMenuList.get(position).getName().contentEquals("My Transactions")) {
                    Intent intent = new Intent(MainActivity.this, TransactionHistoryActivity.class);
                    startActivity(intent);
                } else if (sideMenuList.get(position).getName().contentEquals("Get Directions")) {
                    Intent intent = new Intent(MainActivity.this, GetDirectionsActivity.class);
                    startActivity(intent);
                } else if (sideMenuList.get(position).getName().contentEquals("Update Profile")) {
                    Intent intent = new Intent(MainActivity.this, UpdateProfileActivity.class);
                    startActivity(intent);
                } else if (sideMenuList.get(position).getName().contentEquals("Provide Feedback")) {
                    Intent intent = new Intent(MainActivity.this, WriteFeedbackActivity.class);
                    startActivity(intent);
                } else if (sideMenuList.get(position).getName().contentEquals("Follow us on Facebook")) {
                    Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Sarvodayafaridabad"));
                    startActivity(browserIntent1);
                } else if (sideMenuList.get(position).getName().contentEquals("Follow us on Twitter")) {
                    Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/sarvodaya_care"));
                    startActivity(browserIntent1);
                } else if (sideMenuList.get(position).getName().contentEquals("About Us")) {
                    Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(AppSettings.ABOUT_US));
                    startActivity(browserIntent1);
                } else if (sideMenuList.get(position).getName().contentEquals("Share With Friends")) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, "Check out this app!  https://play.google.com/store/apps/details?id=com.sarvodaya.patient");
                    startActivity(Intent.createChooser(intent, "Share With Fiends"));
                } else if (sideMenuList.get(position).getName().contentEquals("Rate This App")) {
                    Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.sarvodaya.patient"));
                    startActivity(browserIntent1);
                } else if (sideMenuList.get(position).getName().contentEquals("FAQs")) {
                    Intent intent = new Intent(MainActivity.this, FaqActivity.class);
                    startActivity(intent);
                } else if (sideMenuList.get(position).getName().contentEquals("Contact Us")) {
                    Intent intent = new Intent(MainActivity.this, ContactUsActivity.class);
                    startActivity(intent);
                }
            }

        });

        menuRightButton.setVisibility(View.GONE);
        menuRightButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean drawerOpen = dLayout.isDrawerOpen(dList);
                dLayout.openDrawer(dList);
            }
        });


        profile_pic.setOnClickListener(this);
        img_book_an_appointment.setOnClickListener(this);
        img_lab_reports.setOnClickListener(this);
        img_emergency_sos.setOnClickListener(this);
        img_health_offers.setOnClickListener(this);
        img_book_health_check.setOnClickListener(this);
        img_health_tips.setOnClickListener(this);

        // in Activity#onCreate
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                CustomDialogCallEmergency.newInstance(MainActivity.this, context)
                        .show(getFragmentManager(), "dialog");
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(getApplicationContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.img_profile:
                openImageMasterActivity();
                break;
            case R.id.img_book_an_appointment:
                // Check if Internet present
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(MainActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                intent = new Intent(MainActivity.this, MainAppointmentActivity.class);
                intent.putExtra("page", "book_appointment");
                startActivity(intent);
                break;
            case R.id.img_lab_reports:
                // Check if Internet present
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(MainActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                CustomDialogLabReports.newInstance(MainActivity.this, context)
                        .show(getFragmentManager(), "dialog");
                break;
            case R.id.img_emergency_sos:
                OptionCall();
                break;
            case R.id.img_health_offers:
                // Check if Internet present
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(MainActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                intent = new Intent(MainActivity.this, HealthOfferActivity.class);
                startActivity(intent);
                break;
            case R.id.img_book_a_health_check:
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(MainActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                intent = new Intent(MainActivity.this, HealthPackageActivity.class);
                startActivity(intent);
                break;
            case R.id.img_health_tips:
                // Check if Internet present
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(MainActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                intent = new Intent(MainActivity.this, HealthTipsActivity.class);
                startActivity(intent);
                break;
        }

    }

    private void openImageMasterActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent intent = new Intent(MainActivity.this, ImageMaster.class);
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(MainActivity.this, profile_pic, getSt(R.string.transition_image));
            startActivityForResult(intent, ImageMaster.REQUEST_PROFILE, options.toBundle());
            return;
        }
        Intent intent = new Intent(MainActivity.this, ImageMaster.class);
        startActivityForResult(intent, ImageMaster.REQUEST_PROFILE);
    }

    public void OptionCall() {
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.CALL_PHONE)
                .check();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem actionViewItem = menu.findItem(R.id.menu_refresh);
        // Retrieve the action-view from menu
        View v = MenuItemCompat.getActionView(actionViewItem);
        // Find the button within action-view
        ImageView imageView = (ImageView) v.findViewById(R.id.icon_menu_black);
        imageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean drawerOpen = dLayout.isDrawerOpen(dList);
                if (!drawerOpen) {
                    dLayout.openDrawer(dList);
                } else {
                    dLayout.closeDrawer(dList);
                }
//		    	dLayout.openDrawer(dList);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //fetching contact number
        DatabaseHandler db = new DatabaseHandler(context);
        List<Contact> contactList = db.getAllContacts();

        name.setText(contactList.get(0).getName());
        phoneNo.setText(contactList.get(0).getPhoneNumber());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                dLayout.openDrawer(dList);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == ImageMaster.REQUEST_PROFILE) {
            Uri uri = data.getData();
            if (uri != null) {
                setProfileImage();
            }
        } else {
            Toast.makeText(this, "ERROR-103 Image not found", Toast.LENGTH_LONG).show();
        }
    }

    private void setProfileImage() {
        String encodedImage = AppUser.getProfileImage(getApplicationContext());
        if (!TextUtils.isEmpty(encodedImage)) {
            Bitmap bmp = FileUtils.convertEncodedStringToImageBitmap(encodedImage);
            profile_pic.setImageBitmap(bmp);
        } else {
            profile_pic.setImageResource(R.drawable.profile);
        }
    }

    public boolean isShowingNotification() {
        return true;
    }


    private class UpdateAvailability extends AsyncTask<String, Void, NotificationModel> {

        private Context context;

        public void onDetach() {
            this.context = null;
        }

        public UpdateAvailability(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setSosButton(context);
        }

        @Override
        protected NotificationModel doInBackground(String... params) {
            int currentVersionCode = BuildConfig.VERSION_CODE;
            String response = ApiCall.GET(params[0]);
            return JsonParser.parseAppUpdates(context, response, currentVersionCode);
        }

        @Override
        protected void onPostExecute(NotificationModel res) {
            super.onPostExecute(res);
            if (context != null) {
                if (res.isUpdate()) {
                    VersionUpdateDialog.newInstance().show(getFragmentManager(), "versionUpdateDialog");
                }
                setSosButton(context);

//                String titleNoti="New Update Alert";
//                String description="There is a new update available on playstore.";
////                String imageUrl="https://cdn.pixabay.com/photo/2017/08/30/07/56/money-2696234_960_720.jpg";
//                String imageUrl="https://cdn.dribbble.com/users/1387536/screenshots/3185925/attachments/678208/3333.jpg";
                if (!TextUtils.isEmpty(res.getShowPopup()) && res.getShowPopup().toLowerCase().equals("yes")) {
                    if (isShowingNotification() && AppNotification.checkNeverShowAgain(MainActivity.this, res.getTitle())) {
                        addFragment(AppNotification.newInstance(res.getTitle(), res.getDescription(), res.getImageUrl()), "appNotification");
                    }
                }
            }
        }
    }



    private void setSosButton(Context context) {
        String currentDate = DateTimeUtility.getDateStamp();
        int mCurrDate = convertToTimestamp(currentDate);
        int mStartDate = convertToTimestamp(AppUser.getStartDate(context));
        int mEndDate = convertToTimestamp(AppUser.getEndDate(context));
        if (mStartDate == 0 || mEndDate == 0) {
            img_emergency_sos.setImageResource(R.drawable.emergency_sos);
            return;
        }
        if (mCurrDate >= mStartDate && mCurrDate <= mEndDate) {
            img_emergency_sos.setBackgroundResource(R.drawable.bg_menu_sos);
            if (!TextUtils.isEmpty(AppUser.getSosImageUrl(context))) {
                Picasso.get().load(AppUser.getSosImageUrl(context))
//                        .transform(new CircleTransform())
                        .placeholder(R.drawable.emergency_fair_sos)
                        .error(R.drawable.emergency_fair_sos)
                        .into(img_emergency_sos);
            } else {
                img_emergency_sos.setImageResource(R.drawable.emergency_fair_sos);
            }
        } else {
            AppUser.setSosNumber(context, "");
            img_emergency_sos.setImageResource(R.drawable.emergency_sos);
        }
    }

    private int convertToTimestamp(String value) {
        try {
            String res = value.replace("-", "");
            return Integer.parseInt(res);
        } catch (NumberFormatException e) {
            return 0;
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (updateTask != null) {
            updateTask.onDetach();
        }
    }


    private void checkRemoteConfig() {
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
//                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
//        long cacheExpiration = 3600; // 1 hour in seconds.
        long cacheExpiration = 0; // 1 hour in seconds.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();
                        }

                        String isRatingShow = mFirebaseRemoteConfig.getString(Const.RATING_STATUS);

                        if (isRatingShow.equals("true")) {
                            rateUs(MainActivity.this);
                        }
                    }
                });
    }

    private void rateUs(Context context) {
        if (!Rating.isRatingProvidedByUser(context)) {
            if (Rating.showRating(context)) {
                addFragment(new RatingPopupAlert(), "ratingPopupAlert");
            }
        }
    }

}
