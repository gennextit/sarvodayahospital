package com.sarvodaya.patient.healthOffers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.widget.ListView;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.HealthOffer;
import com.sarvodayahospital.doctor.adapter.HealthOfferAdapter;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;

public class HealthOfferActivity extends BaseActivity {
	final Context context = this;
	ListView listView;
	private HealthOfferAdapter adapter;
	List<HealthOffer> healthOfferList = new ArrayList<HealthOffer>();
	
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	ProgressDialog dialog;
//	private RecyclerView rvMain;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_health_tips);
		
		SpannableString s = new SpannableString("Health Offers");
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
		            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		listView = (ListView) findViewById(R.id.lv_main);

//		rvMain = (RecyclerView)findViewById(R.id.rv_main);
//
//		LinearLayoutManager horizontalManager = new LinearLayoutManager(HealthOfferActivity.this, LinearLayoutManager.VERTICAL, false);
//		rvMain.setLayoutManager(horizontalManager);
//		rvMain.setItemAnimator(new DefaultItemAnimator());
//
//		healthOfferList=new ArrayList<>();
//		adapter = new HealthOfferNewAdapter(HealthOfferActivity.this, healthOfferList);
//		rvMain.setAdapter(adapter);

		// Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());
				
		new HttpAsyncTaskFaq().execute(AppSettings.HOSPITAL_URL	+ "getHealthOffers");
	}



	private class HttpAsyncTaskFaq extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(HealthOfferActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(HealthOfferActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if(result.contains("[")) {
					JSONParser parser = new JSONParser();
	 				String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
					Object obj = parser.parse(finalResult);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					List<HealthOffer> tempList = sGson.fromJson(array.toString(), HealthOffer.getJsonArrayType());

					healthOfferList.addAll(tempList);
//					adapter.notifyDataSetChanged();


					healthOfferList=tempList;

					adapter = new HealthOfferAdapter(HealthOfferActivity.this, R.layout.list_row_health_offers, healthOfferList);
					listView.setAdapter(adapter);
					
				} else {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							context);

						// set title
						//alertDialogBuilder.setTitle("Your Title");

						// set dialog message
						alertDialogBuilder
							.setMessage("No record found!")
							.setCancelable(false)
							.setPositiveButton("OK",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// if this button is clicked, close
									// current activity
									HealthOfferActivity.this.finish();
								}
							  });


							// create alert dialog
							AlertDialog alertDialog = alertDialogBuilder.create();

							// show it
							alertDialog.show();
				}
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
		
	}
}
