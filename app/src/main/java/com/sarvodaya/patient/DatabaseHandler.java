package com.sarvodaya.patient;

import java.util.ArrayList;
import java.util.List;

import com.sarvodayahospital.beans.Contact;
 
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
 
public class DatabaseHandler extends SQLiteOpenHelper {

	 // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 5;
 
    // Database Name
    private static final String DATABASE_NAME = "contactsManager";
 
    // Contacts table name
    private static final String TABLE_CONTACTS = "contacts";
 
    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String PATIENT_ID = "patient_id";
    private static final String KEY_NAME = "name";
    private static final String KEY_PH_NO = "phone_number";
    private static final String TITLE = "title";
    private static final String ADDRESS = "address";
    private static final String AGE = "age";
    private static final String COUNTRY = "country";
    private static final String CITY = "city";
    private static final String LOCATION = "location";
    private static final String GENDER = "gender";
    
    
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_PH_NO + " TEXT," + PATIENT_ID + " TEXT,"
                + TITLE + " TEXT," + AGE + " TEXT,"
                + COUNTRY + " TEXT," + CITY + " TEXT,"
                + LOCATION + " TEXT," + GENDER + " TEXT," + ADDRESS + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
 
        // Create tables again
        onCreate(db);
    }
 
    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */
 
    // Adding new contact
    void addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName()); // Contact Name
        values.put(KEY_PH_NO, contact.getPhoneNumber()); // Mobile
        values.put(PATIENT_ID, contact.getPatientId()); // Patient Id
        values.put(TITLE, contact.getTitle()); // Title
        values.put(ADDRESS, contact.getAddress()); // Address
        values.put(AGE, contact.getAge()); // Age
        values.put(COUNTRY, contact.getCountry()); // Country
        values.put(CITY, contact.getCity()); // City
        values.put(LOCATION, contact.getLocation()); // Location
        values.put(GENDER, contact.getGender()); // Gender
        
        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }
 
    // Getting single contact
    Contact getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
 
        Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
                KEY_NAME, KEY_PH_NO }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
 
        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));
        // return contact
        return contact;
    }
    
    
     
    // Getting All Contacts
    public List<Contact> getAllContacts() {
        List<Contact> contactList = new ArrayList<Contact>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;
 
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                contact.setPatientId(cursor.getString(3));
                contact.setTitle(cursor.getString(4));
                contact.setAge(cursor.getString(5));
                contact.setCountry(cursor.getString(6));
                contact.setCity(cursor.getString(7));
                contact.setLocation(cursor.getString(8));
                contact.setGender(cursor.getString(9));
                contact.setAddress(cursor.getString(10));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
 
        // return contact list
        return contactList;
    }
 
    // Updating single contact
    public int updateContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName());
        values.put(KEY_PH_NO, contact.getPhoneNumber());
        values.put(TITLE, contact.getTitle()); // Title
        values.put(ADDRESS, contact.getAddress()); // Address
        values.put(AGE, contact.getAge()); // Age
        values.put(COUNTRY, contact.getCountry()); // Country
        values.put(CITY, contact.getCity()); // City
        values.put(LOCATION, contact.getLocation()); // Location
        values.put(GENDER, contact.getGender()); // Gender
        // updating row
        return db.update(TABLE_CONTACTS, values, PATIENT_ID + " = ?",
                new String[] { String.valueOf(contact.getPatientId()) });
    }
 
    // Deleting single contact
    public void deleteContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
        db.close();
    }
    
    public void deleteAllContact() {
    	SQLiteDatabase db = this.getWritableDatabase();
    	db.delete(TABLE_CONTACTS, null, null);
    }
 
 
    // Getting contacts Count
    public int getContactsCount() {
    	 SQLiteDatabase db = this.getReadableDatabase();
         int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_CONTACTS);
         return numRows;
    }
 

}
