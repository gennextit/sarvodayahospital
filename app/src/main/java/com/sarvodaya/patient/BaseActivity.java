package com.sarvodaya.patient;

/**
 * Created by Abhijit on 26-Oct-16.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sarvodayahospital.util.L;
import com.sarvodayahospital.util.TypefaceSpan;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class BaseActivity extends AppCompatActivity {
    boolean conn = false;
    Builder alertDialog;
    ProgressDialog progressDialog;
    AlertDialog dialog = null;
    ImageView ActionBack;
    TextView ActionBarHeading;

//    DrawerLayout dLayout;
//    ListView dList;
//    List<SideMenu> sideMenuList;
//    SideMenuAdapter slideMenuAdapter;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setHeading(String title) {

        SpannableString s = new SpannableString(title);
        s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        getSupportActionBar().setTitle(s);

    }

//    public SpannableString setActionBarTitle(String title) {
//        return setBoldFont(R.color.white,Typeface.BOLD,title);
//    }

    public SpannableString setBoldFont(int colorId, int typeFaceStyle, String title) {
        SpannableString s = new SpannableString(title);
        Typeface externalFont = Typeface.createFromAsset(BaseActivity.this.getAssets(), "fonts/segoeui.ttf");
        s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(getResources().getColor(colorId)), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(typeFaceStyle), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return s;
    }


//    public void setActionBarOption(String Title, final int Option) {
//        LinearLayout ActionBack;
//        ActionBack = (LinearLayout) findViewById(R.id.ll_actionbar_back);
//        TextView tvTitle = (TextView) findViewById(R.id.actionbar_title);
//
//        tvTitle.setText(Title);
//        ActionBack.setOnClickListener(new View.OnClickListener() {
//
//
//            @Override
//            public void onClick(View arg0) {
//                //mannager.popBackStack();
//                switch (Option) {
//                    case 1://FINISH_ACTIVITY:
//                        finish();
//                        break;
//                    case 2://FINISH_FRAGMENT
//                        FragmentManager manager = getSupportFragmentManager();
//                        manager.popBackStack();
//                        break;
//                }
//            }
//        });
//
//    }

//    public void showPDialog(Context context, String msg) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progressDialog.setMessage(msg);
//        progressDialog.setIndeterminate(false);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//    }

    public void showProgressDialog(Activity context, String Msg) {
        showProgressDialog(context, Msg, Gravity.CENTER);
    }

    public void showProgressDialog(Context context, String Msg, int gravity) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(Msg);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //>= API 21progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
                progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation,getApplicationContext().getTheme()));
            } else {
                progressDialog.setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.dialog_animation));
            }
            if (gravity == Gravity.BOTTOM) {
                progressDialog.getWindow().setGravity(Gravity.BOTTOM);
            }
            progressDialog.show();

        }
    }

    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void hideActionBar() {
        getSupportActionBar().hide();
    }

    public SpannableString setFont(String title) {
        SpannableString s = new SpannableString(title);
//		s.setSpan(new TypefaceSpan("CircularStd-Book.otf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return s;
    }

    public void hideKeybord(Activity act) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(act.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    // public void showToast(String txt,Boolean status){
    // // Inflate the Layout
    // LayoutInflater lInflater = (LayoutInflater)getSystemService(
    // Activity.LAYOUT_INFLATER_SERVICE);
    // //LayoutInflater inflater = context.getLayoutInflater();
    //
    // //View layout = lInflater.inflate(R.layout.custom_toast,(ViewGroup)
    // findViewById(R.id.custom_toast_layout_id));
    // View layout=lInflater.inflate(R.layout.custom_toast, null);
    // TextView a=(TextView)layout.findViewById(R.id.tv_toast_msg);
    // ImageView b=(ImageView)layout.findViewById(R.id.imageView1);
    // //layout.setBackgroundResource((status) ? R.drawable.toast_bg :
    // R.drawable.toast_bg_red);
    // b.setImageResource((status) ? R.drawable.success : R.drawable.fail);
    // a.setText(txt);
    // //a.setTextColor((status) ? getResources().getColor(R.color.icon_green) :
    // getResources().getColor(R.color.icon_red));
    // // Create Custom Toast
    // Toast toast = new Toast(BaseActivity.this);
    // toast.setGravity(Gravity.CENTER, 0, 0);
    // toast.setDuration(Toast.LENGTH_LONG);
    // toast.setView(layout);
    // toast.show();
    // }
    //
    // public void ExitAlertBox(Activity Act) {
    //
    // final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
    //
    // // ...Irrelevant code for customizing the buttons and title
    // LayoutInflater inflater = Act.getLayoutInflater();
    //
    // View v=inflater.inflate(R.layout.custom_dialog, null);
    // dialogBuilder.setView(v);
    // Button quitButton = (Button) v.findViewById(R.id.button1);
    // Button ResultButton = (Button) v.findViewById(R.id.button2);
    // Button cancelButton = (Button) v.findViewById(R.id.button3);
    // // if decline button is clicked, close the custom dialog
    // quitButton.setOnClickListener(new OnClickListener() {
    // @Override
    // public void onClick(View v) {
    // // Close dialog
    // dialog.dismiss();
    // finish();
    // }
    // });
    // ResultButton.setOnClickListener(new OnClickListener() {
    // @Override
    // public void onClick(View v) {
    // // Close dialog
    // dialog.dismiss();
    // }
    // });
    // cancelButton.setOnClickListener(new OnClickListener() {
    // @Override
    // public void onClick(View v) {
    // // Close dialog
    // dialog.dismiss();
    // }
    // });
    // dialog = dialogBuilder.create();
    // dialog.show();
    //
    // }

    public SpannableStringBuilder subScr(int text) {

        SpannableStringBuilder cs = new SpannableStringBuilder(String.valueOf(text));
        cs.setSpan(new SubscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        cs.setSpan(new RelativeSizeSpan(0.75f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return cs;
    }

    // public boolean isConnected() {
    //
    // if (isOnline() == false) {
    // showAlertInternet(getSt(R.string.internet_error_tag),
    // getSt(R.string.internet_error_msg), false);
    // return false;
    // } else {
    // return true;
    // }
    // }

    // ProgressDialog progressDialog; I have declared earlier.
//       public void showPDialog(String msg) {
//        progressDialog = new ProgressDialog(BaseActivity.this);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        // progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
//        progressDialog.setMessage(msg);
//        progressDialog.setIndeterminate(false);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//    }
//
//    public void dismissPDialog() {
//        if (progressDialog != null)
//            progressDialog.dismiss();
//    }


    public String LoadPref(String key) {
        return LoadPref(BaseActivity.this, key);
    }

    public String LoadPref(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String data = sharedPreferences.getString(key, "");
        return data;
    }

    public void SavePref(String key, String value) {
        SavePref(BaseActivity.this, key, value);
    }

    public void SavePref(Context context, String key, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public String convert(String res) {
        String UrlEncoding = null;
        try {
            UrlEncoding = URLEncoder.encode(res, "UTF-8");

        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return UrlEncoding;
    }

    public boolean isOnline() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(BaseActivity.this.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        if (haveConnectedWifi == true || haveConnectedMobile == true) {
            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
            conn = true;
        } else {
            L.m("Log-Wifi" + String.valueOf(haveConnectedWifi));
            L.m("Log-Mobile" + String.valueOf(haveConnectedMobile));
            conn = false;
        }

        return conn;
    }

    // public void showAlertInternet(String title, String message, Boolean
    // status) {
    // alertDialog = new AlertDialog.Builder(BaseActivity.this);
    //
    // // Setting Dialog Title
    // alertDialog.setTitle(title);
    //
    // // Setting Dialog Message
    // alertDialog.setMessage(message);
    //
    // if (status != null)
    // // Setting alert dialog icon
    // alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
    //
    // // On pressing Settings button
    // alertDialog.setPositiveButton("SETTING", new
    // DialogInterface.OnClickListener() {
    // public void onClick(DialogInterface dialog, int which) {
    // EnableMobileIntent();
    // }
    // });
    // // Showing Alert Message
    // alertDialog.show();
    // }

    public String getSt(int id) {

        return getResources().getString(id);
    }

    private void EnableMobileIntent() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.provider.Settings.ACTION_SETTINGS);
        startActivity(intent);

    }

    public String viewTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("h:mm a");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());

    }

    public String getDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String getTime12() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("h:mm a");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());

    }

    public String getTime24() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df1 = new SimpleDateFormat("H:mm:ss");
        df1.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df1.format(c.getTime());
    }

    public String getTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("H:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return df.format(c.getTime());
    }

    // give format like dd-MMM-yyyy,dd/MMM/yyyy
    public String viewDate(String format) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public String vDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    // Enter Format eg.dd-MMM-yyyy,dd/MM/yyyy
    public String viewFormatDate(String format) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String formattedDate = df.format(c.getTime());
        return formattedDate;

    }

    public void showToast(String txt) {
        // Inflate the Layout
        Toast toast = Toast.makeText(BaseActivity.this, txt, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
        toast.show();
    }


    // SpannableStringBuilder cs = new SpannableStringBuilder("X3 + X2");
    // cs.setSpan(new SubscriptSpan(), 1, 3,
    // Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    // cs.setSpan(new RelativeSizeSpan(0.75f), 1, 2,
    // Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    // cs.setSpan(new SubscriptSpan(), 6, 7,
    // Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    // cs.setSpan(new RelativeSizeSpan(0.75f), 6, 7,
    // Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//    protected void SetDrawer(final Activity act, LinearLayout iv) {
//        // TODO Auto-generated method stub
//        iv.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                boolean drawerOpen = dLayout.isDrawerOpen(dList);
//                if (!drawerOpen) {
//                    dLayout.openDrawer(dList);
//                } else {
//                    dLayout.closeDrawer(dList);
//                }
//
//            }
//        });
//
//        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        dList = (ListView) findViewById(R.id.left_drawer);
//        LayoutInflater inflater = getLayoutInflater();
//        View listHeaderView = inflater.inflate(R.layout.side_menu_header, null, false);
//        TextView tvName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
//        ImageView ivProfile = (ImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);
//
//        dList.addHeaderView(listHeaderView);
//
//        SideMenu s1 = new SideMenu("My Profile", R.mipmap.ic_launcher);
//        SideMenu s2 = new SideMenu("Order History", R.mipmap.ic_launcher);
//        SideMenu s3 = new SideMenu("Ticket History", R.mipmap.ic_launcher);
////        SideMenu s4 = new SideMenu("Invite Friends", R.mipmap.ic_launcher);
////        SideMenu s4 = new SideMenu("User guide/T&C", R.mipmap.ic_launcher);
//        // SideMenu s7 = new SideMenu("Logout", R.mipmap.menu8);
//
//        sideMenuList = new ArrayList<SideMenu>();
//        sideMenuList.add(s1);
//        sideMenuList.add(s2);
//        sideMenuList.add(s3);
////        sideMenuList.add(s4);
////        sideMenuList.add(s5);
//        // sideMenuList.add(s6);
//        // sideMenuList.add(s7);
//
//        slideMenuAdapter = new SideMenuAdapter(act, R.layout.side_menu_list_slot, sideMenuList);
//        // adapter = new ArrayAdapter<String>(this, R.layout.listlayout, menu);
//
//        dList.setAdapter(slideMenuAdapter);
//        // dList.setSelector(android.R.color.holo_blue_dark);
//
//        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
//
//                dLayout.closeDrawers();
//
//
//                Intent intent;
//                FragmentManager mannager;
//                FragmentTransaction transaction;
//
//                switch (position) {
//
//                    case 1:
//                        Intent i = new Intent(BaseActivity.this, UpdateProfileActivity.class);
//                        startActivity(i);
//                        break;
//                    case 2:
//                        OrderHistory orderHistory = new OrderHistory();
//                        mannager = getSupportFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, orderHistory, "orderHistory");
//                        transaction.addToBackStack("orderHistory");
//                        transaction.commit();
//                        break;
//                    case 3:
//                        TicketHistory ticketHistory = new TicketHistory();
//                        mannager = getSupportFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, ticketHistory, "ticketHistory");
//                        transaction.addToBackStack("ticketHistory");
//                        transaction.commit();
//                        break;
//                    case 4:
//
//                       /* try {
//                            Intent i = new Intent(Intent.ACTION_SEND);
//                            i.setType("text/plain");
//                            i.putExtra(Intent.EXTRA_SUBJECT, "sbuddy");
//                            i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.invite_friends_message));
//                            startActivity(Intent.createChooser(i, "choose one"));
//                        } catch (Exception e) { // e.toString();
//                        }*/
//                        break;
//                    case 5:
//                      /*  UserGuide userGuide = new UserGuide();
//                        mannager = getFragmentManager();
//                        transaction = mannager.beginTransaction();
//                        transaction.replace(android.R.id.content, userGuide, "userGuide");
//                        transaction.addToBackStack("userGuide");
//                        transaction.commit();*/
//
//                        break;
//
//                }
//
//            }
//
//        });
//    }

    public void hideBaseServerErrorAlertBox() {
        if (dialog != null)
            dialog.dismiss();
    }

    public RelativeLayout showBaseServerErrorAlertBox(String errorDetail) {
        return showBaseAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
    }

    public RelativeLayout showBaseServerErrorAlertBox() {
        return showBaseAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.server_time_out_msg), 2, null);
    }

    public RelativeLayout showBaseAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BaseActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog_custom, null);
        dialogBuilder.setView(v);
//        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        RelativeLayout button1 = (RelativeLayout) v.findViewById(R.id.relative_view_ok);
        RelativeLayout button2 = (RelativeLayout) v.findViewById(R.id.relative_view_cancel);
        TextView tvBtnOk = (TextView) v.findViewById(R.id.txt_ok);
        TextView tvTitle = (TextView) v.findViewById(R.id.txt_header);
        tvTitle.setText(title);

        final TextView tvDescription = (TextView) v.findViewById(R.id.txt_waiter_name);
        tvTitle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (errorMessage != null) {
                    tvDescription.setText(errorMessage);
                }
            }
        });

        //tvTitle.setText(title);
        tvDescription.setText(Description);

        tvBtnOk.setText("Retry");

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();
        return button1;
    }


    protected void replaceFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack("tag");
        transaction.commit();
    }

    protected void setFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragment(Fragment fragment, String tag) {
        addFragment(fragment, android.R.id.content, tag);
    }

    protected void addFragment(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.addToBackStack(tag);
        transaction.commit();
    }

    protected void addFragmentWithoutBackstack(Fragment fragment, String tag) {
        addFragmentWithoutBackstack(fragment,android.R.id.content, tag);
    }
    protected void addFragmentWithoutBackstack(Fragment fragment, int container, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(container, fragment, tag);
        transaction.commit();
    }

    // for 2 tabs
    public void setTabFragment(int containerId, Fragment addFragment, String fragTag
            , Fragment rFrag1, Fragment rFrag2, Fragment rFrag3) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (addFragment != null) {
            if (addFragment.isAdded()) { // if the fragment is already in container
                ft.show(addFragment);
            } else { // fragment needs to be added to frame container
                ft.add(containerId, addFragment, fragTag);
            }
        }

        // Hide 1st fragments
        if (rFrag1 != null && rFrag1.isAdded()) {
            ft.hide(rFrag1);
        }
        // Hide 2nd fragments
        if (rFrag2 != null && rFrag2.isAdded()) {
            ft.hide(rFrag2);
        }
        // Hide 2nd fragments
        if (rFrag3 != null && rFrag3.isAdded()) {
            ft.hide(rFrag3);
        }

        ft.commit();
    }

}