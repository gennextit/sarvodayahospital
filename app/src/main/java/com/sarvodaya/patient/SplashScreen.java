package com.sarvodaya.patient;

import com.sarvodaya.patient.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends Activity {
	final Context context = this;
	
	// Splash screen timer
	private static int SPLASH_TIME_OUT = 500;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				DatabaseHandler db = new DatabaseHandler(context);
				
				Integer contactsCount = db.getContactsCount();
				if(contactsCount > 1) {
					db.deleteAllContact();
					Intent i = new Intent(SplashScreen.this, LoginActivity.class);
					startActivity(i);
				} else if(contactsCount.equals(0)){
					Intent i = new Intent(SplashScreen.this, LoginActivity.class);
					startActivity(i);
				} else {
					Intent i = new Intent(SplashScreen.this, MainActivity.class);
					startActivity(i);
				}
				
				//Intent i = new Intent(SplashScreen.this, WriteFeedbackActivity.class);
				//startActivity(i);

				// close this activity
				finish();
			}
		}, SPLASH_TIME_OUT);
	}

}
