package com.sarvodaya.patient.healthCheckup;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.DatabaseHandler;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.Contact;
import com.sarvodayahospital.beans.HealthPackage;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.AppUser;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class PackageDetailsActivity extends BaseActivity {
    final Context context = this;
    ImageView bookAppointmentButton, callButton;
    TableLayout timings_table;
    String package_id, package_name;
    ProgressDialog dialog;
    ConnectionDetector cd;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();
    private PermissionListener permissionlistener;
    private TextView tvTitle;
    private ImageView ivActionBar;
    private String hpNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_details);
        package_id = getIntent().getStringExtra("package_id");
        package_name = getIntent().getStringExtra("package_name");
        hpNumber = AppUser.getHPNumber(this);

        SpannableString s = new SpannableString(package_name);
        s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTitle = (TextView) findViewById(R.id.tv_action_bar);
        tvTitle.setText(s);
        ivActionBar = (ImageView) findViewById(R.id.iv_action_bar);
        ivActionBar.setImageResource(R.drawable.health_care_image);

        bookAppointmentButton = (ImageView) findViewById(R.id.bookAppointmentButton);
        callButton = (ImageView) findViewById(R.id.callButton);
        bookAppointmentButton.setVisibility(View.GONE);
        timings_table = (TableLayout) findViewById(R.id.timings_table);

        // Check if Internet present
        cd = new ConnectionDetector(getApplicationContext());
        TaskLoadPackageDetails();

        bookAppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(PackageDetailsActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                DatabaseHandler db = new DatabaseHandler(context);
                List<Contact> contactList = db.getAllContacts();
                String url = AppSettings.WEB_SERVICE_URL + "Feedback?Name=" + contactList.get(0).getName()
                        + "&Mobile=" + contactList.get(0).getPhoneNumber()
                        + "&SenderEmailID=Dummy@test.com"
                        + "&FeedbackText=" + package_name;

                url = url.replace(" ", "%20");
                new HttpAsyncWriteFeedback().execute(url);
            }
        });

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OptionCall();
            }
        });

        permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                Uri number;
                if (TextUtils.isEmpty(hpNumber)) {
                    number = Uri.parse("tel:+918588851644");
                } else {
                    number = Uri.parse("tel:" + hpNumber);
                }
                Intent callIntent = new Intent(Intent.ACTION_CALL, number);
                if (ActivityCompat.checkSelfPermission(PackageDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(getApplicationContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();

            }



        };

    }

    public void OptionCall() {
        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setGotoSettingButtonText("setting")
                .setPermissions(android.Manifest.permission.CALL_PHONE)
                .check();
    }

    private void TaskLoadPackageDetails() {
        new HttpAsyncTaskLoadPackageDetails().execute(AppSettings.WEB_SERVICE_URL
                + "GetPackageDetail?PackageID=" + package_id);

    }


    private class HttpAsyncTaskLoadPackageDetails extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(PackageDetailsActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(PackageDetailsActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    timings_table.removeAllViews();
                    //timings_table.setColumnStretchable(0, false);
                    //timings_table.setColumnStretchable(1, true);
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    List<HealthPackage> arrayList = sGson.fromJson(array.toString(), HealthPackage.getJsonArrayType());
                    for (HealthPackage temp : arrayList) {
                        TableRow tr = new TableRow(context);
                        tr.setGravity(Gravity.LEFT);
                        tr.setLayoutParams(new LayoutParams(
                                LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT));
                        tr.setBackgroundResource(R.drawable.layout_border_top_package_details);

                        TextView labelTV = new TextView(context);

                        labelTV.setText(temp.getItemType()); // Package Service
                        labelTV.setTextColor(Color.parseColor("#a6ad12"));
                        labelTV.setGravity(Gravity.LEFT);
                        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Book.otf");
                        labelTV.setTypeface(customFont, Typeface.BOLD);
                        labelTV.setPadding(15, 15, 15, 15);

                        tr.addView(labelTV);


                        timings_table.addView(tr, new TableLayout.LayoutParams(
                                LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT));

                        String[] serviceNameArray = temp.getPkgService().split(",");
                        for (int i = 0; i < serviceNameArray.length; i++) {
                            TableRow tr1 = new TableRow(context);
                            tr1.setGravity(Gravity.LEFT);
                            tr1.setLayoutParams(new LayoutParams(
                                    LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT));
                            tr1.setBackgroundResource(R.drawable.layout_border_top_package_details);

                            TextView labelTV1 = new TextView(context);

                            String text = "<font color=#97dcfa>\t\t \u2022  </font> <font color=#000000>" + serviceNameArray[i].replace("&amp;", "&").substring(serviceNameArray[i].lastIndexOf(":") + 1) + "</font>";


                            labelTV1.setText(Html.fromHtml(text)); // Package Service
                            // labelTV1.setTextColor(Color.parseColor("#000000"));
                            labelTV1.setGravity(Gravity.LEFT);
                            Typeface customFont1 = Typeface.createFromAsset(context.getAssets(), "fonts/CircularStd-Book.otf");
                            labelTV1.setTypeface(customFont1, Typeface.NORMAL);
                            labelTV1.setPadding(15, 15, 15, 15);

                            labelTV1.setLayoutParams(new LayoutParams(
                                    LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT));
                            tr1.addView(labelTV1);

                            timings_table.addView(tr1, new TableLayout.LayoutParams(
                                    LayoutParams.FILL_PARENT,
                                    LayoutParams.WRAP_CONTENT));
                        }


                    }
                } else {
                    RelativeLayout retry = showBaseServerErrorAlertBox(result);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            TaskLoadPackageDetails();
                        }
                    });
                }
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
//			dialog.dismiss();
            hideProgressDialog();
        }
    }

    private class HttpAsyncWriteFeedback extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(PackageDetailsActivity.this, "Loading data, Keep patience!");
        }

        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                DatabaseHandler db = new DatabaseHandler(context);
                if (result.contains("Mail Sent")) {
                    Toast.makeText(getApplicationContext(), "Package booked!", Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Unable to book package, Please try again!", Toast.LENGTH_LONG).show();
                }
            } catch (Exception pe) {
                System.out.println(pe);
            }
//			dialog.dismiss();
            hideProgressDialog();
        }
    }
}
