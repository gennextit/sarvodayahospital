package com.sarvodaya.patient.healthCheckup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.HealthPackage;
import com.sarvodayahospital.doctor.adapter.HealthPackageAdapter;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;

public class HealthPackageActivity extends BaseActivity {
	final Context context = this;
	ListView listView;
	private HealthPackageAdapter adapter;
	List<HealthPackage> packageList = new ArrayList<HealthPackage>();
	
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	ProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_get_directions);
		
		SpannableString s = new SpannableString("Health Packages");
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
		            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		listView = (ListView) findViewById(R.id.lv_main);
		
		// Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());

		TaskHealthPackages();
	}

	private void TaskHealthPackages() {
		new HttpAsyncTaskHealthPackages().execute(AppSettings.WEB_SERVICE_URL
				+ "GetHealthPackage");
	}


	private class HttpAsyncTaskHealthPackages extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(HealthPackageActivity.this, "Loading data, Keep patience!");
        }
		
		@Override
		protected String doInBackground(String... urls) {

			return ApiCall.GET(urls[0]);
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				if(result.contains("[")) {
					JSONParser parser = new JSONParser();
	 				String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
					Object obj = parser.parse(finalResult);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					packageList = sGson.fromJson(array.toString(), HealthPackage.getJsonArrayType());
					
					
					
					adapter = new HealthPackageAdapter(HealthPackageActivity.this, R.layout.list_row_health_packages, packageList);
					listView.setAdapter(adapter);
//					listView.setOnItemClickListener(new OnItemClickListener() {
//			            @Override
//			            public void onItemClick(AdapterView<?> parent, View view, int position,
//			                    long id) {
//			            	isInternetPresent = cd.isConnectingToInternet();
//			            	if (!isInternetPresent) {
//								// Internet Connection is not present
//								alert.showAlertDialog(HealthPackageActivity.this, "Internet Connection Error",
//										"Please connect to working Internet connection", false);
//								// stop executing code by return
//								return;
//							}
//			            	Intent intent1 = new Intent(HealthPackageActivity.this, PackageDetailsActivity.class);
//			            	intent1.putExtra("package_id", packageList.get(position).getPackageID());
//			            	intent1.putExtra("package_name", packageList.get(position).getPackageName());
//							startActivity(intent1);
//			            }
//			        });
				} else {
					RelativeLayout retry=showBaseServerErrorAlertBox(result);
					retry.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							hideBaseServerErrorAlertBox();
							TaskHealthPackages();
						}
					});
				}
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
			hideProgressDialog();
		}
		
	}
}
