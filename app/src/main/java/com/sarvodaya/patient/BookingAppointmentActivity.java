package com.sarvodaya.patient;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sarvodaya.patient.appointment.DoctorProfileActivity;
import com.sarvodaya.patient.appointment.UpdateProfileActivity;
import com.sarvodaya.patient.nav.TermsConditionsActivity;
import com.sarvodayahospital.beans.BookingModel;
import com.sarvodayahospital.beans.Contact;
import com.sarvodayahospital.beans.Members;
import com.sarvodayahospital.dialog.PopupAlert;
import com.sarvodayahospital.dialog.PopupDialog;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.ApiCallError;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.Constants;
import com.sarvodayahospital.util.JsonParser;
import com.sarvodayahospital.util.MyTextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Abhijit-PC on 30-Mar-17.
 */

public class BookingAppointmentActivity extends BaseActivity implements View.OnClickListener,PopupDialog.DialogTaskListener,ApiCallError.ErrorTaskListener{
    private static final int DIALOG_UPDATE_PROFILE = 1;
    private static final int TASK_LOAD_DOCTOR_BY_DOCTORID = 1,TASK_GENERATE_APPOINTMENT=2,TASK_LOAD_MEMBERS_DETAIL=3;
    private ImageView profile_pic, profile_pic_background, pay_later, pay_now;
    private LinearLayout membersParent;
    private TextView membersText;
    private ListView dialog_ListView;
    private String doctor_id, app_date, app_time, day;
    private String[] listContent = { "SELECT MEMBER", "ADD A NEW MEMBER"};
    private RelativeLayout part1, relativeLayoutProfile;
    private TextView name, department, fees;
    private ProgressDialog dialog;
    private List<Members> membersList;
    private TextView title;
    private EditText descText;
    private CheckBox termsCheckBox;
    private Context context;

    private Map<String,String> membersMap = new HashMap<String,String>();
    private String startTime;
    private String msg = "";
    private String button_color = "grey";
    private String transactionRefNo = "";

    private ConnectionDetector cd;
    // flag for Internet connection status
    private Boolean isInternetPresent = false;
    // Alert Dialog Manager
    private AlertDialogManager alert = new AlertDialogManager();

    // Mandatory
    private static String HOST_NAME = "";

    private ArrayList<HashMap<String, String>> custom_post_parameters;

    private static final int ACC_ID = 18901; // Provided by EBS

    private static final String SECRET_KEY = "8f1c356fa9b1facfa52eee82eb429d2b";

    double totalamount;
    private String response;

    private String b_name, b_mobile, b_email, b_address, b_city, b_state, b_country, b_pincode, b_doctorname;
    //	private String patientId;
    private String userPhoneNo;
    private DatabaseHandler db;
    private AssignTask assignTask;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_appointment);
        db = new DatabaseHandler(context);
        context=this;
        setHeading("Book an Appointment");
        initUi();
        getBasicDetail();

        loadDoctorByDrId();
    }


    @Override
    protected void onResume() {
        super.onResume();
        loadMembersDetail();
    }

    private void loadMembersDetail() {
        List<Contact> contactList = db.getAllContacts();

        String url = AppSettings.LoadPatientData +"?PatientId=&Mobile=" + contactList.get(0).getPhoneNumber() + "&Landline=";
        executeTask(TASK_LOAD_MEMBERS_DETAIL,url);
    }

    private void loadDoctorByDrId() {
        String url = AppSettings.LoadDoctorByDoctorId +"?Doctor_ID=" + doctor_id;
        executeTask(TASK_LOAD_DOCTOR_BY_DOCTORID,url);
    }

    private void executeTask(int task,String url) {
        switch (task){
            case TASK_LOAD_DOCTOR_BY_DOCTORID:
                assignTask = new AssignTask(this,task);
                assignTask.execute(url);
                break;
            case TASK_LOAD_MEMBERS_DETAIL:
                assignTask = new AssignTask(this,task);
                assignTask.execute(url);
            break;

        }
    }

    private void getBasicDetail() {
        List<Contact> contactList = db.getAllContacts();
        userPhoneNo=contactList.get(0).getPhoneNumber();

        doctor_id = getIntent().getStringExtra("doctor_id");
        startTime = getIntent().getStringExtra("start_time");
        day = getIntent().getStringExtra("day");
        //doctor_id = "LSHHI155";
        if(startTime.contains("AM")) {
            app_time = startTime.substring(0, 5);
        } else {
            if(startTime.startsWith("12")) {
                app_time = startTime.substring(0,5);
            } else {
                Integer min = 720 + (Integer.parseInt(startTime.substring(0,2)) * 60) + Integer.parseInt(startTime.substring(3,5));
                DateFormat df = new SimpleDateFormat("HH:mm");
                Date date = new Date(0, 0, 0, min/60, min%60);
                app_time = df.format(date);
            }
        }
        if(day!=null && Constants.month!=null) {
            app_date = android.text.format.DateFormat.format("yyyy-MM", Constants.month) + "-" + day;
        }else{
            app_date=null;
        }
        //app_date = "2015-09-10";
        title.setText(startTime + ", " + day + " " + android.text.format.DateFormat.format("MMMM yyyy", Constants.month));


    }

    private void initUi() {
        profile_pic = (ImageView) findViewById(R.id.img_profile);
        profile_pic_background = (ImageView) findViewById(R.id.img_profile_background);
        name = (TextView) findViewById(R.id.name);
        department = (TextView) findViewById(R.id.department);
        fees = (TextView) findViewById(R.id.fees);
        title = (TextView) findViewById(R.id.title);
        descText = (EditText) findViewById(R.id.descText);
        membersParent = (LinearLayout) findViewById(R.id.membersParent);
        membersText = (TextView) findViewById(R.id.membersText);
        part1 = (RelativeLayout) findViewById(R.id.part1);
        relativeLayoutProfile = (RelativeLayout) findViewById(R.id.relative_layout_profile);
        pay_later = (ImageView) findViewById(R.id.pay_later);
        pay_now = (ImageView) findViewById(R.id.pay_now);
        MyTextView termsLink1 = (MyTextView)findViewById(R.id.termsLink1);
        termsCheckBox = (CheckBox)findViewById(R.id.termsCheckBox);

        descText.setHorizontallyScrolling(false);
        descText.setMaxLines(Integer.MAX_VALUE);

        membersText.setText("SELECT MEMBER");
        pay_later.setBackgroundResource(R.drawable.pay_later_button_grey);
        pay_now.setBackgroundResource(R.drawable.pay_now_button_grey);

        termsLink1.setOnClickListener(this);
        membersParent.setOnClickListener(this);
        pay_later.setOnClickListener(this);
        pay_now.setOnClickListener(this);
        part1.setOnClickListener(this);
        
        Constants.PaymentStatus = "";
        //init Hostname
        HOST_NAME = getResources().getString(R.string.hostname);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.termsLink1: 
                intent = new Intent(BookingAppointmentActivity.this, TermsConditionsActivity.class);
                startActivity(intent);
                break;
            case R.id.membersParent:
                showDialog(1);
                break;
            case R.id.pay_later:
                if(button_color.contentEquals("orange")) {
                    //fetching age
                    DatabaseHandler db = new DatabaseHandler(context);
                    List<Contact> contactList = db.getAllContacts();
                    Contact contact = contactList.get(0);
                    if(contact.getAge().contentEquals("0")) {
                        PopupDialog.newInstance(DIALOG_UPDATE_PROFILE,"Alert","Age can not be 0, please update your profile.",false,BookingAppointmentActivity.this)
                                .show(getSupportFragmentManager(),"dialog");
                    } else {
                        executeTask(TASK_GENERATE_APPOINTMENT,null);
                    }
                }
                break;
            case R.id.pay_now:

                break;
            case R.id.part1:
                intent = new Intent(BookingAppointmentActivity.this, DoctorProfileActivity.class);
                intent.putExtra("doctor_id", doctor_id);
                startActivity(intent);
                break;
            
        }
    }

    @Override
    public void onOkClick(DialogFragment dialog, int task,ArrayList<String> data) {
        switch (task){
            case DIALOG_UPDATE_PROFILE:
                Intent intent1 = new Intent(BookingAppointmentActivity.this, UpdateProfileActivity.class);
                startActivity(intent1);
                break;
        }
    }
    @Override
    public void onCancelClick(DialogFragment dialog, int task,ArrayList<String> data) {

    }



    @Override
    public void onErrorRetryClick(int task,String url) {
        executeTask(task,url);
    }

    @Override
    public void onErrorCancelClick(int task,String url) {

    }


    private class AssignTask extends AsyncTask<String, Void, BookingModel> {
        private Activity activity;
        private int task;
        private String url;

        public void onDetach() {
            // TODO Auto-generated method stub
            this.activity = null;
        }

        public AssignTask(Activity activity, int task) {
            this.activity = activity;
            this.task = task;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(activity, "Loading, please wait...");
        }

        @Override
        protected BookingModel doInBackground(String... urls) {
            String response;
            this.url=urls[0];
            if(task==TASK_LOAD_DOCTOR_BY_DOCTORID) {
                response = ApiCall.GET(url);
                return JsonParser.parseLoadDrByDrId(response);
            }else{
                return null;
            }
        }

        @Override
        protected void onPostExecute(BookingModel result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (activity != null) {
                hideProgressDialog();
                if (result != null) {
                    if (result.getOutput().equals("success")) {

                    } else if (result.getOutput().equals("failure")) {
                        PopupAlert.newInstance(result.getOutputMsg())
                                .show(getSupportFragmentManager(),"alert");
                    } else {
                        PopupAlert.newInstance(result.getOutputMsg())
                                .show(getSupportFragmentManager(),"alert");
                    }
                }else{
                    ApiCallError.newInstance(JsonParser.ERRORMESSAGE,task,url,BookingAppointmentActivity.this)
                            .show(getSupportFragmentManager(),"apiCallError");

                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(assignTask!=null){
            assignTask.onDetach();
        }
    }
}
