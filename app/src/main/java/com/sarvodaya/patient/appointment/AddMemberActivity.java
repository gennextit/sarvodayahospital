package com.sarvodaya.patient.appointment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.DatabaseHandler;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.Contact;
import com.sarvodayahospital.beans.Location;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.List;

public class AddMemberActivity extends BaseActivity {
    final Context context = this;
    LinearLayout relationsParent, titleParent, countryParent, cityParent, locationParent, mobileHeadingParent, mobileParent;
    TextView relationsText, titleText, countryText, cityText, locationText;
    ListView relations_ListView, title_ListView;
    ImageView confirm_button;
    EditText nameText, ageText, mobileText, landlineText, addressText;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    ProgressDialog dialog;

    String[] relationsList = {"---Select relation---", "Spouse", "Father", "Mother", "Child", "Sister", "Brother", "Others"};
    String[] titleList;
    String relation = "---Select relation---";

    String[] countryList = {};
    String[] cityList = {};
    String[] locationList = {};

    ConnectionDetector cd;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);

        SpannableString s = new SpannableString("Add a Member");
        s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        getSupportActionBar().setTitle(s);

        relationsParent = (LinearLayout) findViewById(R.id.relationsParent);
        titleParent = (LinearLayout) findViewById(R.id.titleParent);
        countryParent = (LinearLayout) findViewById(R.id.countryParent);
        cityParent = (LinearLayout) findViewById(R.id.cityParent);
        locationParent = (LinearLayout) findViewById(R.id.locationParent);
        mobileHeadingParent = (LinearLayout) findViewById(R.id.mobileHeadingParent);
        mobileParent = (LinearLayout) findViewById(R.id.mobileParent);
        nameText = (EditText) findViewById(R.id.nameText);
        ageText = (EditText) findViewById(R.id.ageText);
        mobileText = (EditText) findViewById(R.id.mobileText);
        landlineText = (EditText) findViewById(R.id.landlineText);
        addressText = (EditText) findViewById(R.id.addressText);
        countryText = (TextView) findViewById(R.id.countryText);
        cityText = (TextView) findViewById(R.id.cityText);
        locationText = (TextView) findViewById(R.id.locationText);
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        confirm_button = (ImageView) findViewById(R.id.confirm_button);
        confirm_button.setBackgroundResource(R.drawable.confirm_button_orange);

        relationsText = (TextView) findViewById(R.id.relationsText);
        relationsText.setText("---Select relation---");
        titleText = (TextView) findViewById(R.id.titleText);
        titleText.setText("---Select title---");
        locationText.setText("---Select location---");
        mobileHeadingParent.setVisibility(View.GONE);
        mobileParent.setVisibility(View.GONE);
        // Check if Internet present
        cd = new ConnectionDetector(getApplicationContext());

        //fetching contact number
        DatabaseHandler db = new DatabaseHandler(context);
        List<Contact> contactList = db.getAllContacts();
        mobileText.setText(contactList.get(0).getPhoneNumber());
        relationsParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(1);
            }
        });

        titleParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!relationsText.getText().toString().contentEquals("---Select relation---")) {
                    showDialog(2);
                }
            }
        });
        countryParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(3);
            }
        });
        cityParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(4);
            }
        });
        locationParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(5);
            }
        });

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMemberTask();
            }
        });

        TaskCountries();

    }

    private void TaskCountries() {
        new HttpAsyncTaskCountries().execute(AppSettings.WEB_SERVICE_URL
                + "LoadCountries");
    }

    private void AddMemberTask() {
        if (relationsText.getText().toString().contentEquals("---Select relation---")) {
            Toast.makeText(getApplicationContext(), "Select relation.", Toast.LENGTH_LONG).show();
        } else if (titleText.getText().toString().contentEquals("---Select title---")) {
            Toast.makeText(getApplicationContext(), "Select title.", Toast.LENGTH_LONG).show();
        } else if (nameText.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Enter name of patient.", Toast.LENGTH_LONG).show();
            nameText.requestFocusFromTouch();
            InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            lManager.showSoftInput(nameText, 0);
        } else if (ageText.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Enter age.", Toast.LENGTH_LONG).show();
            ageText.requestFocusFromTouch();
            InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            lManager.showSoftInput(ageText, 0);
        } else if (mobileText.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Enter mobile number.", Toast.LENGTH_LONG).show();
            mobileText.requestFocusFromTouch();
            InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            lManager.showSoftInput(mobileText, 0);
        } else if (countryText.getText().toString().contentEquals("---Select country---")) {
            Toast.makeText(getApplicationContext(), "Select country.", Toast.LENGTH_LONG).show();
        } else if (cityText.getText().toString().contentEquals("---Select city---")) {
            Toast.makeText(getApplicationContext(), "Select city.", Toast.LENGTH_LONG).show();
        } else if (locationText.getText().toString().contentEquals("---Select location---")) {
            Toast.makeText(getApplicationContext(), "Select location.", Toast.LENGTH_LONG).show();
        } else {
            isInternetPresent = cd.isConnectingToInternet();
            if (!isInternetPresent) {
                // Internet Connection is not present
                alert.showAlertDialog(AddMemberActivity.this, "Internet Connection Error",
                        "Please connect to working Internet connection", false);
                // stop executing code by return
                return;
            }
            String url = AppSettings.WEB_SERVICE_URL + "AddMember?Title=" + titleText.getText().toString()
                    + "&PatientName=" + nameText.getText().toString().trim()
                    + "&Age=" + ageText.getText().toString().trim() + "%20yrs"
                    + "&Mobile=" + mobileText.getText().toString();

            int selectedId = radioSexGroup.getCheckedRadioButtonId();
            radioSexButton = (RadioButton) findViewById(selectedId);

            url = url + "&Gender=" + radioSexButton.getText().toString();

            url = url + "&Landline=" + landlineText.getText().toString().trim();
            url = url + "&Address=" + addressText.getText().toString().trim();

            if (countryText.getText().toString().length() > 0 && !countryText.getText().toString().contentEquals("---Select country---")) {
                url = url + "&Country=" + countryText.getText().toString();
            } else {
                url = url + "&Country=";
            }
            if (cityText.getText().toString().length() > 0 && !cityText.getText().toString().contentEquals("---Select city---")) {
                url = url + "&City=" + cityText.getText().toString();
            } else {
                url = url + "&City=";
            }
            if (locationText.getText().toString().length() > 0 && !locationText.getText().toString().contentEquals("---Select location---")) {
                url = url + "&Location=" + locationText.getText().toString();
            } else {
                url = url + "&Location=";
            }
            url = url + "&Relation=" + relationsText.getText().toString() + "&RelationName=&Source=MA";
            url = url.replace(" ", "%20");
            new HttpAsyncTaskAddMember().execute(url);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        Dialog dialog = null;

        switch (id) {
            case 1:
                dialog = new Dialog(AddMemberActivity.this);
                dialog.setContentView(R.layout.specialitylayout);
                dialog.setTitle("Relations");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(FindDoctorActivity.this, "OnCancelListener",Toast.LENGTH_LONG).show();
                    }
                });

                dialog.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
                    }
                });

                // Prepare ListView in dialog
                relations_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        R.layout.listlayout, relationsList);
                relations_ListView.setAdapter(adapter);
                relations_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        relationsText.setText(parent.getItemAtPosition(position).toString());
                        if (relationsText.getText().toString().contentEquals("Spouse")) {
                            titleList = new String[3];
                            titleList[0] = "Mr.";
                            titleList[1] = "Mrs.";
                            titleList[2] = "Dr.";
                        } else if (relationsText.getText().toString().contentEquals("Father")) {
                            titleList = new String[2];
                            titleList[0] = "Mr.";
                            titleList[1] = "Dr.";
                        } else if (relationsText.getText().toString().contentEquals("Mother")) {
                            titleList = new String[2];
                            titleList[0] = "Mrs.";
                            titleList[1] = "Dr.";
                        } else if (relationsText.getText().toString().contentEquals("Child")) {
                            titleList = new String[7];
                            titleList[0] = "Mr.";
                            titleList[1] = "Mrs.";
                            titleList[2] = "Ms.";
                            titleList[3] = "Dr.";
                            titleList[4] = "Baby";
                            titleList[5] = "B/O";
                            titleList[6] = "Master";
                        } else if (relationsText.getText().toString().contentEquals("Sister")) {
                            titleList = new String[3];
                            titleList[0] = "Mrs.";
                            titleList[1] = "Ms.";
                            titleList[2] = "Dr.";
                        } else if (relationsText.getText().toString().contentEquals("Brother")) {
                            titleList = new String[2];
                            titleList[0] = "Mr.";
                            titleList[1] = "Dr.";
                        } else if (relationsText.getText().toString().contentEquals("Others")) {
                            titleList = new String[7];
                            titleList[0] = "Mr.";
                            titleList[1] = "Mrs.";
                            titleList[2] = "Ms.";
                            titleList[3] = "Dr.";
                            titleList[4] = "Baby";
                            titleList[5] = "B/O";
                            titleList[6] = "Master";
                        }
                        titleText.setText("---Select title---");
                        dismissDialog(1);
                    }
                });

                break;

            case 2:
                dialog = new Dialog(AddMemberActivity.this);
                dialog.setContentView(R.layout.specialitylayout);
                dialog.setTitle("Title");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(FindDoctorActivity.this, "OnCancelListener",Toast.LENGTH_LONG).show();
                    }
                });

                dialog.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
                    }
                });

                // Prepare ListView in dialog
                title_ListView = (ListView) dialog.findViewById(R.id.dialoglist);

                ArrayAdapter<String> adapterTitle = new ArrayAdapter<String>(this,
                        R.layout.listlayout, titleList);
                title_ListView.setAdapter(adapterTitle);

                title_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        titleText.setText(parent.getItemAtPosition(position).toString());
                        removeDialog(2);
                    }
                });

                break;
            case 3:
                dialog = new Dialog(AddMemberActivity.this);
                dialog.setContentView(R.layout.specialitylayout);
                dialog.setTitle("Country");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        removeDialog(3);
                    }
                });

                dialog.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
                    }
                });

                // Prepare ListView in dialog
                title_ListView = (ListView) dialog.findViewById(R.id.dialoglist);

                ArrayAdapter<String> adapterCountry = new ArrayAdapter<String>(this,
                        R.layout.listlayout, countryList);
                title_ListView.setAdapter(adapterCountry);

                title_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        countryText.setText(parent.getItemAtPosition(position).toString());
                        String country = parent.getItemAtPosition(position).toString().toString().replace(" ", "%20");
                        removeDialog(3);
                        new HttpAsyncTaskCities().execute(AppSettings.WEB_SERVICE_URL
                                + "LoadCityByCountry?Country=" + country);
                        cityText.setText("---Select city---");
                        locationText.setText("---Select location---");

                    }
                });

                break;
            case 4:
                dialog = new Dialog(AddMemberActivity.this);
                dialog.setContentView(R.layout.specialitylayout);
                dialog.setTitle("City");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        removeDialog(4);
                    }
                });

                dialog.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
                    }
                });

                // Prepare ListView in dialog
                title_ListView = (ListView) dialog.findViewById(R.id.dialoglist);

                ArrayAdapter<String> adapterCity = new ArrayAdapter<String>(this,
                        R.layout.listlayout, cityList);
                title_ListView.setAdapter(adapterCity);

                title_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        cityText.setText(parent.getItemAtPosition(position).toString());
                        String city = parent.getItemAtPosition(position).toString().replace(" ", "%20");
                        locationText.setText("---Select location---");
                        removeDialog(4);
                        new HttpAsyncTaskLocations().execute(AppSettings.WEB_SERVICE_URL
                                + "LoadLocationByCity?City=" + city);

                    }
                });

                break;
            case 5:
                dialog = new Dialog(AddMemberActivity.this);
                dialog.setContentView(R.layout.specialitylayout);
                dialog.setTitle("Location");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        removeDialog(5);
                    }
                });

                dialog.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
                    }
                });

                // Prepare ListView in dialog
                title_ListView = (ListView) dialog.findViewById(R.id.dialoglist);

                ArrayAdapter<String> adapterLocation = new ArrayAdapter<String>(this,
                        R.layout.listlayout, locationList);
                title_ListView.setAdapter(adapterLocation);

                title_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        locationText.setText(parent.getItemAtPosition(position).toString());
                        removeDialog(5);
                    }
                });

                break;
        }

        return dialog;
    }


    private class HttpAsyncTaskCountries extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if(result.contains("[")) {
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    List<Location> tempCountryList = sGson.fromJson(array.toString(), Location.getJsonArrayType());
                    countryList = new String[tempCountryList.size()];
                    Integer countryCount = 0;
                    for (Location location : tempCountryList) {
                        countryList[countryCount++] = location.getCountry().replace("&amp;", "&");
                    }

                    //selecting India by default
                    countryText.setText("INDIA");
                    //loading Faridabad city by default
                    new HttpAsyncTaskCities().execute(AppSettings.WEB_SERVICE_URL
                            + "LoadCityByCountry?Country=India");
                }else {
                    RelativeLayout retry=showBaseServerErrorAlertBox(result);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            TaskCountries();
                        }
                    });
                }

            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
        }
    }

    private class HttpAsyncTaskCities extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    List<Location> tempCityList = sGson.fromJson(array.toString(), Location.getJsonArrayType());
                    cityList = new String[tempCityList.size()];
                    Integer cityCount = 0;
                    for (Location location : tempCityList) {
                        cityList[cityCount++] = location.getCity().replace("&amp;", "&");

                        //selecting by default Faridabad
                        if (location.getCity().equalsIgnoreCase("Faridabad")) {
                            cityText.setText(location.getCity());

                            //loading location based on Faridabad
                            new HttpAsyncTaskLocations().execute(AppSettings.WEB_SERVICE_URL
                                    + "LoadLocationByCity?City=Faridabad");
                        }
                    }

                } else {
                    cityList = new String[0];
                }
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
        }
    }

    private class HttpAsyncTaskLocations extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(AddMemberActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(AddMemberActivity.this);
//            //dialog.setIndeterminate(true);
//            //dialog.setIndeterminateDrawable(getResources().getDrawable(R.anim.progress_dialog_anim));
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    List<Location> tempLocationList = sGson.fromJson(array.toString(), Location.getJsonArrayType());
                    locationList = new String[tempLocationList.size()];
                    Integer locationCount = 0;
                    for (Location location : tempLocationList) {
                        locationList[locationCount++] = location.getLocation().replace("&amp;", "&");
                    }
                } else {
                    locationList = new String[0];
                }
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
            hideProgressDialog();
//			dialog.dismiss();
        }
    }

    private class HttpAsyncTaskAddMember extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(AddMemberActivity.this, "Adding member, Keep patience!");
//            dialog= new ProgressDialog(AddMemberActivity.this);
//            //dialog.setIndeterminate(true);
//            //dialog.setIndeterminateDrawable(getResources().getDrawable(R.anim.progress_dialog_anim));
//            dialog.setCancelable(false);
//            dialog.setMessage("Adding member, Keep patience!");
//            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("\"PatientID")) {
                    Toast.makeText(getApplicationContext(), "Member added successfully.", Toast.LENGTH_LONG).show();

                    //adding member to session
                    SharedPreferences app_preferences = PreferenceManager
                            .getDefaultSharedPreferences(context);

                    SharedPreferences.Editor editor = app_preferences.edit();
                    editor.putString("member", titleText.getText().toString().trim() + " " + nameText.getText().toString());
                    editor.commit();
                } else if (result.contains("Enter appropriate value for Title")) {
                    Toast.makeText(getApplicationContext(), "API Error, Please contact Support Team.", Toast.LENGTH_LONG).show();
                    String url = AppSettings.WEB_SERVICE_URL + "Feedback?Name=" + nameText.getText().toString()
                            + "&Mobile=" + mobileText.getText().toString()
                            + "&SenderEmailID=test@yahoo.com"
                            + "&FeedbackText=Enter appropriate value for Title";

                    url = url.replace(" ", "%20");
                    new HttpAsyncWriteFeedback().execute(url);
                } else {
                    Toast.makeText(getApplicationContext(), "Member already exists.", Toast.LENGTH_LONG).show();
                }

            } catch (Exception pe) {
                System.out.println(pe);
            }
//			dialog.dismiss();
            hideProgressDialog();

            finish();
        }
    }

    private class HttpAsyncWriteFeedback extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result != null) {
                    DatabaseHandler db = new DatabaseHandler(context);
                    if (result.contains("Mail Sent")) {
                        Toast.makeText(getApplicationContext(), "Feedback sent!", Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Unable to send feedback, Please try again!", Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception pe) {
                System.out.println(pe);
            }
        }
    }

}
