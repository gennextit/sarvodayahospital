package com.sarvodaya.patient.appointment;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.AppointmentByDoctor;
import com.sarvodayahospital.beans.BlockTimeByDoctor;
import com.sarvodayahospital.beans.TempDoctor;
import com.sarvodayahospital.beans.TimeAvailable;
import com.sarvodayahospital.beans.TimeSlot;
import com.sarvodayahospital.doctor.adapter.SelectAppointmentAdapter;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.CircleTransform;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.Const;
import com.sarvodayahospital.util.DateTimeUtility;
import com.sarvodayahospital.util.GridViewScrollable;
import com.sarvodayahospital.util.RoundImage;
import com.sarvodayahospital.util.TypefaceSpan;
import com.squareup.picasso.Picasso;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TimeSelectionActivity extends BaseActivity {
	private final Context context = this;
	private GridViewScrollable gridview;
	private ImageView profile_pic, confirm_button;
	String day;
	private String doctor_id,doctor_name,doctor_pic,doctor_department;
	private SelectAppointmentAdapter adapter;
	String button_color = "grey";
	ProgressDialog dialog; 
	TextView name, department, fees;
	RelativeLayout previous, next, relativeLayoutProfile;
	LinearLayout part1;
	String startTime;
	TextView title;
	Map<String, String> bookedTimeSlots = new HashMap<String, String>();
	List<BlockTimeByDoctor> allBlockedTimeSlots = new ArrayList<BlockTimeByDoctor>();
	Map<String, String> blockedTimeSlots = new HashMap<String, String>();
	
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	TextView noRecordFoundTextView;
	ImageView bottom;
	String app_date;
	Integer avg_time;
	private String sltMonth;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_appointment);

		SpannableString s = new SpannableString("Book an Appointment");
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
		            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		profile_pic = (ImageView) findViewById(R.id.img_profile);
		part1 = (LinearLayout) findViewById(R.id.part1);
		relativeLayoutProfile = (RelativeLayout) findViewById(R.id.relative_layout_profile);
		gridview = (GridViewScrollable) findViewById(R.id.gridview);
		name = (TextView) findViewById(R.id.name);
	    department = (TextView) findViewById(R.id.department);
	    fees = (TextView) findViewById(R.id.fees);
		
	    title = (TextView) findViewById(R.id.title);
	    previous = (RelativeLayout) findViewById(R.id.previous);
	    next = (RelativeLayout) findViewById(R.id.next);
	    previous.setVisibility(View.GONE);
	    next.setVisibility(View.GONE);
	    noRecordFoundTextView = (TextView) findViewById(R.id.txt_no_record_found);
	    bottom = (ImageView) findViewById(R.id.bottom);
		noRecordFoundTextView.setVisibility(View.INVISIBLE);
		
	    // Check if Internet present
	 	cd = new ConnectionDetector(getApplicationContext());


		doctor_id = getIntent().getStringExtra(Const.DOCTOR_ID);
		doctor_name = getIntent().getStringExtra(Const.DOCTOR_NAME);
		doctor_pic = getIntent().getStringExtra(Const.DOCTOR_PIC);
		doctor_department = getIntent().getStringExtra(Const.DOCTOR_DEPARTMENT);
		setDoctorProfile(doctor_name,doctor_department,doctor_pic);

		day = getIntent().getStringExtra("day");
		app_date=sltMonth = getIntent().getStringExtra("appDate"); //yyyy-MM-dd
		if(app_date!=null){
			title.setText(DateTimeUtility.convertDateDDMMMMYYYY(app_date));
			app_date=DateTimeUtility.convertDateyyyyMMdd(app_date);

		}
//        app_date = android.text.format.DateFormat.format("yyyy-MM", Constants.month) + "-" + day;
        
        part1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				isInternetPresent = cd.isConnectingToInternet();
				if (!isInternetPresent) {
					// Internet Connection is not present
					alert.showAlertDialog(TimeSelectionActivity.this, "Internet Connection Error",
							"Please connect to working Internet connection", false);
					// stop executing code by return
					return;
				}
				startDoctorProfileActivity(doctor_id);
			}
		});

		TaskAppointmentByDoctor();

		TaskLoadListofBlockTimeByDoctor();

		TaskLoadTimeSot();

        loadDoctorByDrId();
	}

	private void setDoctorProfile(String doctor_name, String doctor_dep, String doctor_pic) {
		name.setText(doctor_name!=null?doctor_name:"");
		department.setText(doctor_dep!=null?doctor_dep:"");
		if(doctor_pic==null||doctor_pic.equals("")) {
			profile_pic.setImageResource(R.mipmap.all_doctors_pic);
		} else {
			Picasso.get().load(doctor_pic).transform(new CircleTransform())
					.placeholder(R.mipmap.all_doctors_pic)
					.error(R.mipmap.all_doctors_pic)
					.into(profile_pic);
		}
	}

	private void startDoctorProfileActivity(String doctorId) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Intent intent1 = new Intent(TimeSelectionActivity.this, DoctorProfileActivity.class);
			intent1.putExtra("doctor_id", doctorId);
			ActivityOptions options = ActivityOptions
					.makeSceneTransitionAnimation(TimeSelectionActivity.this, profile_pic,getSt(R.string.transition_profile));
			startActivity(intent1, options.toBundle());
			return;
		}
		Intent intent1 = new Intent(TimeSelectionActivity.this, DoctorProfileActivity.class);
		intent1.putExtra("doctor_id", doctor_id);
		startActivity(intent1);
	}

	// task 1
	private void TaskAppointmentByDoctor() {
		new HttpAsyncTaskAppointmentByDoctor().execute(AppSettings.WEB_SERVICE_URL
				+ "ViewAppointmentByDoctor?Doctorid=" + doctor_id + "&AppDate="+app_date);
	}
	//task 2
	private void TaskLoadListofBlockTimeByDoctor() {
		new HttpAsyncTaskLoadListofBlockTimeByDoctor().execute(AppSettings.WEB_SERVICE_URL
				+ "ListofBlockTimeByDoctor?Doctor_Id=" + doctor_id + "&MonthName=" + DateTimeUtility.convertDateMMMM(app_date));
	}

	//task 3
	private void TaskLoadTimeSot() {
		new HttpAsyncTaskLoadTimeSot().execute(AppSettings.WEB_SERVICE_URL
				+ "LoadTimeSlot?DoctorId=" + doctor_id + "&Date=" + app_date);
	}

	//task 4
	private void loadDoctorByDrId() {
		new HttpAsyncTaskLoadDoctorByDoctorId().execute(AppSettings.WEB_SERVICE_URL
				+ "LoadDoctorByDoctorId?Doctor_ID=" + doctor_id);
	}


	private class HttpAsyncTaskLoadTimeSot extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(TimeSelectionActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(SelectAppointmentActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if(!result.contains("Record Not Found")) {
					if(result.contains("[")) {
						Thread.sleep(200);
						JSONParser parser = new JSONParser();
						String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
						Object obj = parser.parse(finalResult);
						JSONArray array = (JSONArray) obj;
						Gson sGson = new Gson();
						List<TimeSlot> arrayList = sGson.fromJson(array.toString(), TimeSlot.getJsonArrayType());
						avg_time = Integer.parseInt(arrayList.get(0).getAvgTime());
						if (allBlockedTimeSlots.size() > 0) {
							for (BlockTimeByDoctor tempBlockTime : allBlockedTimeSlots) {
								getBlockedSlots(tempBlockTime.getFromTime(), tempBlockTime.getToTime(), avg_time);
							}
						}

						List<TimeAvailable> appointmentSlot = new ArrayList<TimeAvailable>();
						for (TimeSlot tempTimeSlot : arrayList) {
							appointmentSlot.addAll(getSlots(tempTimeSlot.getStartTime(), tempTimeSlot.getEndTime(), Integer.parseInt(tempTimeSlot.getAvgTime())));

						}
						gridview.setExpanded(true);
						adapter = new SelectAppointmentAdapter(context, appointmentSlot, doctor_id,doctor_name,doctor_department,doctor_pic, day, TimeSelectionActivity.this,profile_pic);
						if (appointmentSlot.size() > 0) {
							gridview.setAdapter(adapter);
							adapter.notifyDataSetChanged();
							noRecordFoundTextView.setVisibility(View.GONE);
							gridview.setVisibility(View.VISIBLE);
							bottom.setVisibility(View.VISIBLE);
						} else {
							noRecordFoundTextView.setVisibility(View.VISIBLE);
							gridview.setVisibility(View.INVISIBLE);
							bottom.setVisibility(View.INVISIBLE);
						}
					}else{
						showToast(result);
					}
			        
				} else {
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							context);

						// set title
						//alertDialogBuilder.setTitle("Your Title");

						// set dialog message
						alertDialogBuilder
							.setMessage("Record not found for selected doctor.")
							.setCancelable(false)
							.setPositiveButton("OK",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// if this button is clicked, close
									// current activity
									TimeSelectionActivity.this.finish();
								}
							  });


							// create alert dialog
							AlertDialog alertDialog = alertDialogBuilder.create();

							// show it
							alertDialog.show();
				}
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
		
		
		public List<TimeAvailable> getSlots(String startTime, String endTime, Integer avgTime) {
			String tempStart = "", tempEnd;
			
			Integer startMin, endMin;
			startMin = (Integer.parseInt(startTime.substring(0,2)) * 60) + Integer.parseInt(startTime.substring(3,5));
			endMin = (Integer.parseInt(endTime.substring(0,2)) * 60) + Integer.parseInt(endTime.substring(3,5));
			System.out.println(startMin);
			System.out.println(endMin);
			LinkedList<TimeAvailable> availableSlots = new LinkedList<TimeAvailable>();
			int j;
			Calendar cal = Calendar.getInstance();
	        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
	        SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
	        
	        //get current time in minutes
	        String currentTime =  sdfTime.format(cal.getTime());
	        int currentTimeInMinutes = (Integer.parseInt(currentTime.substring(0,2)) * 60) + Integer.parseInt(currentTime.substring(3,5));
			//get current day
	        String currentDay = sdfDay.format(cal.getTime());
	        for(int i = startMin; i < endMin; i = i + avgTime) {
				j = i + avgTime;
				if(currentDay.substring(0, 1).contains("0")) {
					currentDay = currentDay.substring(1, 2);
				}
				if(currentDay.contentEquals(day)) {
					if ((currentTimeInMinutes + 30) > i) {
						//do nothing
					} else {
						
						System.out.println(convert(i/60,i%60) + " " + convert(j/60,j%60));
						tempStart = convert(i/60, i%60);
						tempEnd = convert(j/60,j%60);
						if(bookedTimeSlots.containsKey(tempStart)) {
							availableSlots.add(new TimeAvailable(tempStart, tempEnd, "Booked"));
						} else if(blockedTimeSlots!=null && blockedTimeSlots.containsKey(tempStart)) {
							availableSlots.add(new TimeAvailable(tempStart, tempEnd, "Unavailable"));
						} else {
							availableSlots.add(new TimeAvailable(tempStart, tempEnd, "Available"));
						}	
					}
				} else {
					System.out.println(convert(i/60,i%60) + " " + convert(j/60,j%60));
					tempStart = convert(i/60, i%60);
					tempEnd = convert(j/60,j%60);
					if(bookedTimeSlots.containsKey(tempStart)) {
						availableSlots.add(new TimeAvailable(tempStart, tempEnd, "Booked"));
					} else if(blockedTimeSlots!=null && blockedTimeSlots.containsKey(tempStart)) {
						availableSlots.add(new TimeAvailable(tempStart, tempEnd, "Unavailable"));
					} else {
						availableSlots.add(new TimeAvailable(tempStart, tempEnd, "Available"));
					}
				}
				
				
			}
			return availableSlots;
		}
		
		
		public void getBlockedSlots(String startTime, String endTime, Integer avgTime) {
			String tempStart = "";
			
			Integer startMin, endMin;
			startMin = (Integer.parseInt(startTime.substring(0,2)) * 60) + Integer.parseInt(startTime.substring(3,5));
			endMin = (Integer.parseInt(endTime.substring(0,2)) * 60) + Integer.parseInt(endTime.substring(3,5));
			System.out.println(startMin);
			System.out.println(endMin);
			int j;
			Calendar cal = Calendar.getInstance();
	        SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
	        
	        //get current day
	        String currentDay = sdfDay.format(cal.getTime());
	        for(int i = startMin; i < endMin; i = i + avgTime) {
				j = i + avgTime;
				if(currentDay.substring(0, 1).contains("0")) {
					currentDay = currentDay.substring(1, 2);
				}
				System.out.println(convert(i/60,i%60) + " " + convert(j/60,j%60));
				tempStart = convert(i/60, i%60);
				
				blockedTimeSlots.put(tempStart,tempStart);
			}
			
		}
		
	}
	
	
	
	public String convert(Integer hours, Integer minutes)  {
		DateFormat df = new SimpleDateFormat("hh:mm a");
		Date date = new Date(0, 0, 0, hours, minutes);
		return df.format(date);
	}
	
	private class HttpAsyncTaskLoadDoctorByDoctorId extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if(result.contains("[")) {
					JSONParser parser = new JSONParser();
					String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
					Object obj = parser.parse(finalResult);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					List<TempDoctor> arrayList = sGson.fromJson(array.toString(), TempDoctor.getJsonArrayType());
					name.setText(arrayList.get(0).getDoctorName());
					String dept = arrayList.get(0).getDepartment().replace("&amp;", "&");
					department.setText(dept);
					fees.setText("Rs. " + arrayList.get(0).getDoctorFee());
					String imageUrl = "";
					doctor_name=arrayList.get(0).getDoctorName();
					doctor_department=dept;


					if (arrayList.get(0).getImage().contentEquals("YES")) {
						imageUrl = arrayList.get(0).getImageURL();
						doctor_pic=imageUrl;
						Picasso.get().load(imageUrl).transform(new CircleTransform())
								.placeholder(R.mipmap.all_doctors_pic)
								.error(R.mipmap.all_doctors_pic)
								.into(profile_pic);
					} else {
						profile_pic.setImageResource(R.mipmap.all_doctors_pic);
					}
//				Bitmap bm1 = BitmapFactory.decodeResource(getResources(),R.drawable.profile_pic_background);
//		        roundImageProfileBackground = new RoundImage(bm1);
//		        profile_pic_background.setImageDrawable(roundImageProfileBackground);
				}else{
					showToast(getSt(R.string.server_time_out_msg));
//					RelativeLayout retry=showBaseServerErrorAlertBox(result);
//					retry.setOnClickListener(new OnClickListener() {
//						@Override
//						public void onClick(View view) {
//							hideBaseServerErrorAlertBox();
//							loadDoctorByDrId();
//						}
//					});
				}
				
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
		}
	}
	
	private class HttpAsyncTaskAppointmentByDoctor extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if(result.contains("[")) {
					if(result.contains("Record Not Found")) {
						 
					} else {
						JSONParser parser = new JSONParser();
		 				String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
						Object obj = parser.parse(finalResult);
						JSONArray array = (JSONArray) obj;
						Gson sGson = new Gson();
						List<AppointmentByDoctor> arrayList = sGson.fromJson(array.toString(), AppointmentByDoctor.getJsonArrayType());
						
						 for(AppointmentByDoctor tempTimeSlot : arrayList) {
							Integer min = (Integer.parseInt(tempTimeSlot.getAppointmentTime().substring(0,2)) * 60) + Integer.parseInt(tempTimeSlot.getAppointmentTime().substring(3,5)); 
							String tempStart = convert(min/60, min%60);
							bookedTimeSlots.put(tempStart,tempStart);
						}
				       
					}
					
				} else {
					showToast(getSt(R.string.server_time_out_msg));

				}
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
		}
	}
	
	private class HttpAsyncTaskLoadListofBlockTimeByDoctor extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if(result.contains("[")) {
					if(result.contains("Record Not Found")) {
						 
					} else {
						JSONParser parser = new JSONParser();
		 				String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
						Object obj = parser.parse(finalResult);
						JSONArray array = (JSONArray) obj;
						Gson sGson = new Gson();
						List<BlockTimeByDoctor> arrayList = sGson.fromJson(array.toString(), BlockTimeByDoctor.getJsonArrayType());
						
						 for(BlockTimeByDoctor tempTimeSlot : arrayList) {
							if(tempTimeSlot.getBlockDate().contentEquals(app_date)) {
								allBlockedTimeSlots.add(tempTimeSlot);
							}
						}
				       
					}
					
				} else {
				}
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
		}
	}
	
}
