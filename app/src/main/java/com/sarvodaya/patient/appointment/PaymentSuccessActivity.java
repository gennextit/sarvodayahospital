package com.sarvodaya.patient.appointment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.ebs.android.sdk.PaymentRequest;
import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.BuildConfig;
import com.sarvodaya.patient.R;
import com.sarvodaya.patient.nav.TransactionHistoryActivity;
import com.sarvodayahospital.beans.AppointmentWithPayment;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.Constants;
import com.sarvodayahospital.util.PaymentManager;
import com.sarvodayahospital.util.RequestBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;

public class PaymentSuccessActivity extends BaseActivity {
	String payment_id;
	String PaymentId;
	String AccountId;
	String MerchantRefNo;
	String Amount;
	String DateCreated;
	String Description;
	String Mode;
	String IsFlagged;
	String BillingName;
	String BillingAddress;
	String BillingCity;
	String BillingState;
	String BillingPostalCode;
	String BillingCountry;
	String BillingPhone;
	String BillingEmail;
	String DeliveryName;
	String DeliveryAddress;
	String DeliveryCity;
	String DeliveryState;
	String DeliveryPostalCode;
	String DeliveryCountry;
	String DeliveryPhone;
	String PaymentStatus;
	String PaymentMode;
	String SecureHash;
	String ResponseCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_payment_success);

		Intent intent = getIntent();

		payment_id = intent.getStringExtra("payment_id");
		System.out.println("payment_id" + " " + payment_id);
		getJsonReport();

		Button btn_payment_success = (Button) findViewById(R.id.btn_payment_success);
		btn_payment_success.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				PaymentSuccessActivity.this.finish();
				if (Constants.PaymentStatus.contentEquals("Authorized")) {
					Toast.makeText(
							getApplicationContext(),
							"Your Payment Id :"
									+ PaymentRequest.getInstance().getPaymentId(),
							Toast.LENGTH_SHORT).show();

					generateAppointmentWithPayment(PaymentSuccessActivity.this,null);
				}

			}
		});

	}



	private void getJsonReport() {
		String response = payment_id;

		JSONObject jObject;
		try {
			jObject = new JSONObject(response.toString());
			PaymentRequest.getInstance().setPaymentResponse(response.toString());
			

			PaymentId = jObject.getString("PaymentId");

			AccountId = jObject.getString("AccountId");
			MerchantRefNo = jObject.getString("MerchantRefNo");
			Amount = jObject.getString("Amount");
			DateCreated = jObject.getString("DateCreated");
			Description = jObject.getString("Description");
			Mode = jObject.getString("Mode");
			IsFlagged = jObject.getString("IsFlagged");
			BillingName = jObject.getString("BillingName");

			BillingAddress = jObject.getString("BillingAddress");
			BillingCity = jObject.getString("BillingCity");
			BillingState = jObject.getString("BillingState");
			BillingPostalCode = jObject.getString("BillingPostalCode");
			BillingCountry = jObject.getString("BillingCountry");
			BillingPhone = jObject.getString("BillingPhone");
			BillingEmail = jObject.getString("BillingEmail");
			DeliveryName = jObject.getString("DeliveryName");
			DeliveryAddress = jObject.getString("DeliveryAddress");
			DeliveryCity = jObject.getString("DeliveryCity");
			DeliveryState = jObject.getString("DeliveryState");
			DeliveryPostalCode = jObject.getString("DeliveryPostalCode");
			DeliveryCountry = jObject.getString("DeliveryCountry");
			DeliveryPhone = jObject.getString("DeliveryPhone");
			PaymentStatus = jObject.getString("PaymentStatus");
			PaymentMode = jObject.getString("PaymentMode");
			SecureHash = jObject.getString("SecureHash");
			ResponseCode = jObject.getString("ResponseCode");
			PaymentRequest.getInstance().setPaymentId(PaymentId);
			Constants.PaymentStatus = PaymentStatus;
			Constants.PaymentMode = PaymentMode;

			TableLayout table_payment = (TableLayout) findViewById(R.id.table_payment);
			ArrayList<String> arrlist = new ArrayList<String>();
			arrlist.add("Payment Id");
			arrlist.add("Merchant Ref No");
			arrlist.add("Amount");
			arrlist.add("Date");
			arrlist.add("Payment Status");
			arrlist.add("Payment Mode");

			ArrayList<String> arrlist1 = new ArrayList<String>();
			arrlist1.add(PaymentId);
			arrlist1.add(MerchantRefNo);
			arrlist1.add(Amount);
			arrlist1.add(DateCreated);
			arrlist1.add(PaymentStatus);
			arrlist1.add(PaymentMode);

			for (int i = 0; i < arrlist.size(); i++) {
				TableRow row = new TableRow(this);

				TextView textH = new TextView(this);
				TextView textC = new TextView(this);
				TextView textV = new TextView(this);

				textH.setText(arrlist.get(i));
				textC.setText(":  ");
				textV.setText(arrlist1.get(i));
				textV.setTypeface(null, Typeface.BOLD);

				row.addView(textH);
				row.addView(textC);
				row.addView(textV);

				table_payment.addView(row);
			}


		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void generateAppointmentWithPayment(Context context,String errorMsg) {
		String message = null;
		String paymentRefNo=PaymentRequest.getInstance().getPaymentId();
		PaymentManager payMngg = PaymentManager.get(context);
		String appDate = payMngg.getAppDate();
		String patientId = payMngg.getPatientId();
		String doctorId = payMngg.getDoctorId();
		String appTime = payMngg.getAppTime();
		String transactionRefNo = payMngg.getTransactionRefNo();
		String userPhoneNo = payMngg.getUserPhoneNo();
//		String url = AppSettings.WEB_SERVICE_URL
//				+ "GenerateAppointmentWithPayment?PatientId=" + patientId + "&DoctorId=" + doctorId
//				+ "&AppDate=" + appDate + "&AppTime=" +appTime + "&Source=MA" + "&DeseaseDesc="
//				+ payMngg.getDeseaseDesc() + "&PaymentMode=" + Constants.PaymentMode  + "&Amount="
//				+ payMngg.getAmount() + "&PaymentRefNo=" + paymentRefNo + "&TransactionRefNo=" + transactionRefNo;

		String url = AppSettings.WEB_SERVICE_URL
				+ "GenerateAppointmentWithPayment";

		RequestBody requestBody = RequestBuilder.getPaymentSuccess(patientId, doctorId,appDate,appTime, "MA", payMngg.getDeseaseDesc()
				,Constants.PaymentMode , payMngg.getAmount(), paymentRefNo, transactionRefNo);

		if(appDate!=null && patientId!=null && doctorId!=null && appTime!=null){
			if(!TextUtils.isEmpty(paymentRefNo)) {
				new HttpAsyncTaskGenerateAppointmentWithPayment(requestBody, patientId,userPhoneNo, errorMsg).execute(url);
			}else{
				String errMsg="Reason: paymentRefNo null ,AppVersion:"+ BuildConfig.VERSION_NAME+",AppVersionCode:"+ String.valueOf(BuildConfig.VERSION_CODE )+"Phone No:"+userPhoneNo+" ,URL= "+url;
//				String errMsg="Reason: paymentRefNo null ,Phone No:"+userPhoneNo+" ,URL= "+url;
				new ErrorReportingTask(errMsg).execute();
				message="Sorry we could not book your appointment. Please contact Sarvodaya team using your payment Ref. No. "+paymentRefNo+" and transaction Ref. No. "+transactionRefNo;
				showAlert(context,message);
			}
		}else{
			//fetching contact number
			String errMsg="Reason: plz check dr id, app date or time not being null ,AppVersion:"+ BuildConfig.VERSION_NAME+",AppVersionCode:"+ String.valueOf(BuildConfig.VERSION_CODE )+"Phone No:"+userPhoneNo+" ,URL= "+url;
//			String errMsg="Reason: plz check dr id, app date or time not being null ,Phone No:"+userPhoneNo+" ,URL= "+url;
			new ErrorReportingTask(errMsg).execute();
			message="Sorry we could not get payment refrence details from the payment gateway. In case payment has been deducted" +
					" please contact Sarvodaya team advising your payment details";
			showAlert(context,message);
		}
	}


	private void showAlert(Context context,String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		alertDialogBuilder
				.setMessage(message)
				.setCancelable(true)
				.setPositiveButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();

					}
				});


		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	private class HttpAsyncTaskGenerateAppointmentWithPayment extends AsyncTask<String, Void, String> {
		private final String patient_id;
		private final String errorMsg;
		private final String userPhoneNo;
		private final RequestBody requestBody;

		public HttpAsyncTaskGenerateAppointmentWithPayment(RequestBody requestBody, String patient_id, String userPhoneNo, String errorMsg) {
			this.patient_id=patient_id;
			this.errorMsg=errorMsg;
			this.userPhoneNo=userPhoneNo;
			this.requestBody=requestBody;
		}

		@Override
		protected void onPreExecute() {
			showProgressDialog(PaymentSuccessActivity.this,"Generating appointment, Keep patience!");
		}

		@Override
		protected String doInBackground(String... urls) {

//			HttpReq httpReq=new HttpReq();
			if(errorMsg==null){
				return  ApiCall.POST(urls[0], requestBody,60);
			}else{
				ApiCall.POST(AppSettings.REPORT_SERVER, RequestBuilder.ErrorText("Phone:"+userPhoneNo+" Patient Id:"+patient_id+","+urls[0]+","+errorMsg));
				return ApiCall.POST(urls[0], requestBody,60);
			}
		}

		@Override
		protected void onPostExecute(final String result) {
			try {
				if(result.contains("AppointmentID") || result.contains("Doctor is not available for this particular time") || result.contains("More than two days future data can't be taken") || result.contains("Back time appointment cannot be taken")) {

					String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);

					Gson sGson = new Gson();
					List<AppointmentWithPayment> arrayList = sGson.fromJson(finalResult, AppointmentWithPayment.getJsonArrayType());

					String msg="";
					if(arrayList.size() > 0) {
						msg = "Your Apointment Id is " + arrayList.get(0).getAppointmentId() + "\nPayement Id : " + arrayList.get(0).getPaymentRefNo() + "\nTxn No : " + arrayList.get(0).getTransactionRefNo();

					}
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							PaymentSuccessActivity.this);

					alertDialogBuilder
							.setMessage(msg)
							.setCancelable(false)
							.setPositiveButton("OK",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									if(result.contains("AppointmentID")) {
										Intent intent = new Intent(PaymentSuccessActivity.this, TransactionHistoryActivity.class);
										intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										PaymentSuccessActivity.this.finish();
										startActivity(intent);
									}
								}
							});
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				} else {
					if (Constants.PaymentStatus.contentEquals("Authorized")) {
						RelativeLayout retry=showBaseServerErrorAlertBox(result);
						retry.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View view) {
								hideBaseServerErrorAlertBox();
								Log.e("Payment Response", PaymentRequest.getInstance()
										.getPaymentResponse());
								generateAppointmentWithPayment(PaymentSuccessActivity.this,result);
							}
						});
					}else{
						Toast.makeText(getApplicationContext(),"PaymentStatus UnAuthorized",Toast.LENGTH_LONG).show();
					}
				}
			} catch (Exception pe) {
				System.out.println(pe);
			}
			hideProgressDialog();
		}
	}


	private class ErrorReportingTask extends AsyncTask<Void, Void, String> {
		private final String errorMsg;

		public ErrorReportingTask(String errorMsg) {
			this.errorMsg=errorMsg;
		}

		@Override
		protected String doInBackground(Void... urls) {
			return ApiCall.POST(AppSettings.REPORT_SERVER, RequestBuilder.ErrorText(errorMsg));
		}
	}


}