package com.sarvodaya.patient.appointment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.Department;
import com.sarvodayahospital.beans.TempDoctor;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainAppointmentActivity extends BaseActivity {
	final Context context = this;
	AutoCompleteTextView doctorsTextView = null;
	LinearLayout specialityParent;
	TextView specialityText;
	ListView dialog_ListView;
	ImageView header;
	ImageView submitButton;
	String page;
	String[] listContent = { "Aesthetic and Reconstructive Surgery", "Audiology & Speech Therapy", "Ayurveda Medicine", "Cardiology",
			"Clinical Allergist", "Dental Care", "Dermatology/Trichology", "Diabetic Educator", "Dietician", "Endocrinology",
			"ENT", "Foetal Medicine", " Gastroenterology" , "General/ Laproscopic Surgery", "GI Surgery", "Internal Medicine"};

	String[] doctors = { "Dr. Rakesh Gupta", "Dr. SHRUTI KOHLI",
			"Dr. SUNIL GARG", "Dr. Arvind Singhal", "Dr. Sunny Jain",
			"Dr. Kundan singh chufal", "SUMIT NARANG" };
	
	Map<String,String> uniqueDoctor = new HashMap<String,String>();
	ProgressDialog dialog;
	SpannableString s; 
	
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_doctor);
		page = getIntent().getStringExtra("page");
		header = (ImageView) findViewById(R.id.header);
		
		if(page.contentEquals("book_appointment")) {
			header.setImageResource(R.drawable.book_appointment_top);
			s = new SpannableString("Book an Appointment");
		} else { 
			s = new SpannableString("Find a Doctor");
			
		}
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
		            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		doctorsTextView = (AutoCompleteTextView) findViewById(R.id.doctors);
		specialityParent = (LinearLayout) findViewById(R.id.specialityParent);
		specialityText = (TextView) findViewById(R.id.specialityText);
		submitButton = (ImageView) findViewById(R.id.submitButton);
		specialityText.setText("SELECT SPECIALITY");
		
		
	
		
		specialityParent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(1);
			}
		});
		
		// Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());
		
				
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				isInternetPresent = cd.isConnectingToInternet();
				if (!isInternetPresent) {
					// Internet Connection is not present
					alert.showAlertDialog(MainAppointmentActivity.this, "Internet Connection Error",
							"Please connect to working Internet connection", false);
					// stop executing code by return
					return;
				}
				
				Intent intent1 = new Intent(MainAppointmentActivity.this, AllDoctorsActivity.class);
				intent1.putExtra("speciality", specialityText.getText());
				intent1.putExtra("doctor_id", "0");
				boolean foundDoctor =  false;
				if(doctorsTextView.getText().length() > 0) {
					if (uniqueDoctor.containsValue(doctorsTextView.getText().toString())) {
			            for (Map.Entry<String,String> e : uniqueDoctor.entrySet()) {
			                if ((e.getValue().toString()).equalsIgnoreCase(doctorsTextView.getText().toString())) {
			                	Intent intent2 = new Intent(MainAppointmentActivity.this, DoctorProfileActivity.class);
			                	intent2.putExtra("doctor_id", e.getKey());
			                	foundDoctor = true;
			                	startActivity(intent2);
			                	return;
			                }
			            }
			        }  else {
			        	intent1.putExtra("doctor_id", "2"); //doctor not found
			        }
				} else {
					intent1.putExtra("doctor_id", "1");   //empty doctor field
				}
				startActivity(intent1);
			}
		});
		
		doctorsTextView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				in.hideSoftInputFromWindow(arg1.getApplicationWindowToken(), 0);
			}

		});


		TaskDepartments();
	}

	private void TaskDepartments() {
		new HttpAsyncTaskDepartments().execute(AppSettings.WEB_SERVICE_URL
				+ "LoadDepartment");
	}

	private void TaskDoctors() {
		new HttpAsyncTaskDoctors().execute(AppSettings.WEB_SERVICE_URL
				+ "LoadDoctor");
	}


	public void clearDoctor(View v) {
		doctorsTextView.setText("");
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog dialog = null;

		switch (id) {
		case 1:
			dialog = new Dialog(MainAppointmentActivity.this);
			dialog.setContentView(R.layout.specialitylayout);
			dialog.setTitle("SPECIALITIES");

			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);

			dialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//Toast.makeText(FindDoctorActivity.this, "OnCancelListener",Toast.LENGTH_LONG).show();
				}
			});

			dialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
				}
			});

			// Prepare ListView in dialog
			dialog_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					R.layout.listlayout, listContent);
			dialog_ListView.setAdapter(adapter);
			dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					doctorsTextView.setText("");
					specialityText.setText(parent.getItemAtPosition(position).toString());
					if(parent.getItemAtPosition(position).toString() != "SELECT SPECIALITY") {
						submitButton.performClick();
					}
					dismissDialog(1);
				}
			});

			break;
		}

		return dialog;
	}
	


	private class HttpAsyncTaskDepartments extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if (result.contains("[")) {
					JSONParser parser = new JSONParser();
					String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
					Object obj = parser.parse(finalResult);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					List<Department> departmentList = sGson.fromJson(array.toString(), Department.getJsonArrayType());
					listContent = new String[departmentList.size() + 1];
					Integer departmentCount = 0;
					listContent[departmentCount++] = "SELECT SPECIALITY";
					for (Department d : departmentList) {
						listContent[departmentCount++] = d.getDepartment().replace("&amp;", "&");
					}
					TaskDoctors();
				}else{
					RelativeLayout retry=showBaseServerErrorAlertBox(result);
					retry.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							hideBaseServerErrorAlertBox();
							TaskDepartments();
						}
					});
				}
				//System.out.println(arraylist.size());
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
		}
	}
	
	private class HttpAsyncTaskDoctors extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(MainAppointmentActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(FindDoctorActivity.this);
//            //dialog.setIndeterminate(true);
//            //dialog.setIndeterminateDrawable(getResources().getDrawable(R.anim.progress_dialog_anim));
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if (result.contains("[")) {
					JSONParser parser = new JSONParser();
					String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
					Object obj = parser.parse(finalResult);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					List<TempDoctor> doctorList = sGson.fromJson(array.toString(), TempDoctor.getJsonArrayType());

					for (TempDoctor tempDoctor : doctorList) {
						if (!uniqueDoctor.containsKey(tempDoctor.getDoctorId())) {
							if (tempDoctor.getDoctorName().contains("Dr")) {
								uniqueDoctor.put(tempDoctor.getDoctorId(), tempDoctor.getDoctorName());
							} else {
								uniqueDoctor.put(tempDoctor.getDoctorId(), "Dr. " + tempDoctor.getDoctorName());
							}
						}
					}


					doctors = new String[uniqueDoctor.size()];
					Integer doctorsCount = 0;
					for (Map.Entry<String, String> entry : uniqueDoctor.entrySet()) {
						doctors[doctorsCount++] = entry.getValue();
					}

					ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.dropdown_list_item, doctors);

					doctorsTextView.setThreshold(1);
					doctorsTextView.setAdapter(adapter);
				}else{
					RelativeLayout retry=showBaseServerErrorAlertBox(result);
					retry.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							hideBaseServerErrorAlertBox();
							TaskDoctors();
						}
					});
				}
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
	}
}
