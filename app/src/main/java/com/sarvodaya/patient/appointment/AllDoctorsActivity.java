package com.sarvodaya.patient.appointment;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.Doctor;
import com.sarvodayahospital.beans.TempDoctor;
import com.sarvodayahospital.doctor.adapter.AllDoctorAdapter;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.RequestBuilder;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AllDoctorsActivity extends BaseActivity {
    final Context context = this;
    ListView listView;
    private AllDoctorAdapter adapter;
    String speciality, doctor_id;
    Map<String, String> uniqueDoctor = new HashMap<String, String>();
    List<Doctor> doctorList = new ArrayList<Doctor>();
    ProgressDialog dialog;

    ConnectionDetector cd;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_doctors);

        SpannableString s = new SpannableString("Book an Appointment");
        s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        getSupportActionBar().setTitle(s);
        listView = (ListView) findViewById(R.id.listview);

        speciality = getIntent().getStringExtra("speciality");
        doctor_id = getIntent().getStringExtra("doctor_id");

        //it contains no doctor
       loadDoctors();
        // Check if Internet present
        cd = new ConnectionDetector(getApplicationContext());

    }

    private void loadDoctors() {
        if (doctor_id.contentEquals("1") && speciality.contains("SELECT SPECIALITY")) {   //load all doctors
            new HttpAsyncTaskAllDoctors().execute(AppSettings.WEB_SERVICE_URL
                    + "LoadDoctor");
        } else if (doctor_id.contentEquals("1") && !speciality.contains("SELECT SPECIALITY")) {   //load department wise doctor
            new HttpAsyncTaskDepartmentWiseDoctor().execute(AppSettings.WEB_SERVICE_URL
                    + "LoadDepartmentWiseDoctor", speciality);
//            new HttpAsyncTaskDepartmentWiseDoctor().execute(AppSettings.WEB_SERVICE_URL
//                    + "LoadDepartmentWiseDoctor?DepartmentName=" + speciality);
//
        } else if (doctor_id.contentEquals("2")) {   //doctor not found

        } else {    //load particular doctor
            new HttpAsyncTaskLoadDoctorByDoctorId().execute(AppSettings.WEB_SERVICE_URL
                    + "LoadDoctorByDoctorId?Doctor_ID=" + doctor_id);
        }
    }


    private class HttpAsyncTaskDepartmentWiseDoctor extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(AllDoctorsActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(AllDoctorsActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.POST(urls[0], RequestBuilder.LoadDepartmentWiseDoctor(urls[1]));
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    List<TempDoctor> arrayList = sGson.fromJson(array.toString(), TempDoctor.getJsonArrayType());

                    for (TempDoctor tempDoctor : arrayList) {
                        String image=tempDoctor.getImage();
                        if(image!=null) {
                            if (image.contentEquals("YES")) {
                                doctorList.add(new Doctor(tempDoctor.doctorId, tempDoctor.doctorName, speciality, "Faridabad", tempDoctor.getImageURL()));
                            } else {
                                doctorList.add(new Doctor(tempDoctor.doctorId, tempDoctor.doctorName, speciality, "Faridabad", null));
                            }
                        }else{
                            doctorList=null;
                        }
                    }

                    if(doctorList!=null) {
                        adapter = new AllDoctorAdapter(AllDoctorsActivity.this, R.layout.list_row_all_doctor, doctorList);
                        listView.setAdapter(adapter);
                    }else{
                        ArrayList<String> errorList = new ArrayList<>();
//                    errorList.add(getString(R.string.employee_not_found));
                        errorList.add("Record not found");
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(AllDoctorsActivity.this, R.layout.slot_empty,
                                R.id.tv_message, errorList);
                        listView.setAdapter(adapter);
                    }
                } else {
                    RelativeLayout retry=showBaseServerErrorAlertBox(result);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            loadDoctors();
//                            GenerateAppointment(result);
                        }
                    });
                }
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
//			dialog.dismiss();
            hideProgressDialog();
        }

    }



    private class HttpAsyncTaskAllDoctors extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(AllDoctorsActivity.this, "Loading data, Keep patience!");
        }

        @Override
        protected String doInBackground(String... urls) {

            return ApiCall.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    List<TempDoctor> arrayList = sGson.fromJson(array.toString(), TempDoctor.getJsonArrayType());

                    for (TempDoctor tempDoctor : arrayList) {
                        if (!uniqueDoctor.containsKey(tempDoctor.getDoctorId())) {
                            if (tempDoctor.getImage().contentEquals("YES")) {
                                doctorList.add(new Doctor(tempDoctor.doctorId, tempDoctor.doctorName, tempDoctor.department, "Faridabad", tempDoctor.getImageURL()));
                            } else {
                                doctorList.add(new Doctor(tempDoctor.doctorId, tempDoctor.doctorName, tempDoctor.department, "Faridabad", null));
                            }
                            uniqueDoctor.put(tempDoctor.getDoctorId(), tempDoctor.getDoctorName());
                        }
                    }
                    if(doctorList!=null&&doctorList.size()!=0) {
                        adapter = new AllDoctorAdapter(AllDoctorsActivity.this, R.layout.list_row_all_doctor, doctorList);
                        listView.setAdapter(adapter);
                    } else{
                        ArrayList<String> errorList = new ArrayList<>();
//                    errorList.add(getString(R.string.employee_not_found));
                        errorList.add("Record not found");
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(AllDoctorsActivity.this, R.layout.slot_empty,
                                R.id.tv_message, errorList);
                        listView.setAdapter(adapter);
                    }
                }else{
                    RelativeLayout retry=showBaseServerErrorAlertBox(result);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            loadDoctors();
                        }
                    });
                }
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
//			dialog.dismiss();
            hideProgressDialog();
        }
    }

    private class HttpAsyncTaskLoadDoctorByDoctorId extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(AllDoctorsActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(AllDoctorsActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    List<TempDoctor> arrayList = sGson.fromJson(array.toString(), TempDoctor.getJsonArrayType());


                    for (TempDoctor tempDoctor : arrayList) {
                        if (!uniqueDoctor.containsKey(tempDoctor.getDoctorId())) {
                            if (tempDoctor.getImage().contentEquals("YES")) {
                                doctorList.add(new Doctor(tempDoctor.doctorId, tempDoctor.doctorName, tempDoctor.department, "Faridabad", tempDoctor.getImageURL()));
                            } else {
                                doctorList.add(new Doctor(tempDoctor.doctorId, tempDoctor.doctorName, tempDoctor.department, "Faridabad", null));
                            }
                            uniqueDoctor.put(tempDoctor.getDoctorId(), tempDoctor.getDoctorName());
                        }
                    }
                    if(doctorList!=null&&doctorList.size()!=0) {
                        adapter = new AllDoctorAdapter(AllDoctorsActivity.this, R.layout.list_row_all_doctor, doctorList);
                        listView.setAdapter(adapter);
                    } else{
                        ArrayList<String> errorList = new ArrayList<>();
//                    errorList.add(getString(R.string.employee_not_found));
                        errorList.add("Record not found");
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(AllDoctorsActivity.this, R.layout.slot_empty,
                                R.id.tv_message, errorList);
                        listView.setAdapter(adapter);
                    }

//                    listView.setOnItemClickListener(new OnItemClickListener() {
//                        @Override
//                        public void onItemClick(AdapterView<?> parent, View view, int position,
//                                                long id) {
//                            isInternetPresent = cd.isConnectingToInternet();
//                            if (!isInternetPresent) {
//                                // Internet Connection is not present
//                                alert.showAlertDialog(AllDoctorsActivity.this, "Internet Connection Error",
//                                        "Please connect to working Internet connection", false);
//                                // stop executing code by return
//                                return;
//                            }
//
//                            Intent intent1 = new Intent(AllDoctorsActivity.this, DoctorProfileActivity.class);
//                            intent1.putExtra("doctor_id", doctorList.get(position).getDoctorId());
//                            startActivity(intent1);
//                        }
//                    });
                }else{
                    RelativeLayout retry=showBaseServerErrorAlertBox(result);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            loadDoctors();
                        }
                    });
                }
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
//			dialog.dismiss();
            hideProgressDialog();
        }
    }
}
