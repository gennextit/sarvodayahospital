package com.sarvodaya.patient.appointment;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ebs.android.sdk.Config.Encryption;
import com.ebs.android.sdk.Config.Mode;
import com.ebs.android.sdk.EBSPayment;
import com.ebs.android.sdk.PaymentRequest;
import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.BuildConfig;
import com.sarvodaya.patient.DatabaseHandler;
import com.sarvodaya.patient.R;
import com.sarvodaya.patient.nav.TermsConditionsActivity;
import com.sarvodaya.patient.nav.TransactionHistoryActivity;
import com.sarvodayahospital.beans.Contact;
import com.sarvodayahospital.beans.Members;
import com.sarvodayahospital.beans.TempDoctor;
import com.sarvodayahospital.dialog.CustomAddPaymentDetails;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.AppUser;
import com.sarvodayahospital.util.CircleTransform;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.Const;
import com.sarvodayahospital.util.Constants;
import com.sarvodayahospital.util.L;
import com.sarvodayahospital.util.MyTextView;
import com.sarvodayahospital.util.PaymentManager;
import com.sarvodayahospital.util.RequestBuilder;
import com.squareup.picasso.Picasso;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookAppointmentActivity extends BaseActivity implements OnClickListener, CustomAddPaymentDetails.IntimatePayment {
    private final Context context = this;
    private ImageView profile_pic, pay_later, pay_now;
    private LinearLayout membersParent;
    private TextView membersText;
    private ListView dialog_ListView;
    private String app_date, app_time, day;
    private String doctor_id,doctor_name,doctor_pic,doctor_dept;
    private String[] listContent = {"SELECT MEMBER", "ADD A NEW MEMBER"};
    private RelativeLayout  relativeLayoutProfile;
    private LinearLayout part1;
    private TextView name, department, fees;
    private ProgressDialog dialog;
    private List<Members> membersList;
    private TextView title;
    private EditText descText;
    private CheckBox termsCheckBox;

    Map<String, String> membersMap = new HashMap<String, String>();
    String startTime;
    String msg = "";
    String button_color = "grey";
    String transactionRefNo = "";

    ConnectionDetector cd;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();

    // Mandatory
    private static String HOST_NAME = "";

    ArrayList<HashMap<String, String>> custom_post_parameters;

    private static final int ACC_ID = 18901; // Provided by EBS

    private static final String SECRET_KEY = "8f1c356fa9b1facfa52eee82eb429d2b";

    double totalamount;
    private String response;

    /** Testing card detail **
     * Visa- 4111 1111 1111 1111
     * Exp-  07-2017
     * CSV-  123
     * NAME- test
     * */

    String b_name, b_mobile, b_email, b_address, b_city, b_state, b_country, b_pincode, b_doctorname;
    //	private String patientId;
    private String userPhoneNo;
    public Members selectedMemberModel;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_appointment);
        setHeading("Book an Appointment");
        profile_pic = (ImageView) findViewById(R.id.img_profile);
        name = (TextView) findViewById(R.id.name);
        department = (TextView) findViewById(R.id.department);
        fees = (TextView) findViewById(R.id.fees);
        title = (TextView) findViewById(R.id.title);
        descText = (EditText) findViewById(R.id.descText);

        descText.setHorizontallyScrolling(false);
        descText.setMaxLines(Integer.MAX_VALUE);

        DatabaseHandler db = new DatabaseHandler(context);
        List<Contact> contactList = db.getAllContacts();
        userPhoneNo = contactList.get(0).getPhoneNumber();

        membersParent = (LinearLayout) findViewById(R.id.membersParent);
        membersText = (TextView) findViewById(R.id.membersText);
        membersText.setText("SELECT MEMBER");
        part1 = (LinearLayout) findViewById(R.id.part1);
        relativeLayoutProfile = (RelativeLayout) findViewById(R.id.relative_layout_profile);

        pay_later = (ImageView) findViewById(R.id.pay_later);
        pay_now = (ImageView) findViewById(R.id.pay_now);
        pay_later.setBackgroundResource(R.drawable.pay_later_button_grey);
        pay_now.setBackgroundResource(R.drawable.pay_now_button_grey);

        MyTextView termsLink1 = (MyTextView) findViewById(R.id.termsLink1);
        termsCheckBox = (CheckBox) findViewById(R.id.termsCheckBox);

        termsLink1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(BookAppointmentActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                Intent intent1 = new Intent(BookAppointmentActivity.this, TermsConditionsActivity.class);
                startActivity(intent1);
            }
        });
        // Check if Internet present
        cd = new ConnectionDetector(getApplicationContext());

        //init Hostname
        HOST_NAME = getResources().getString(R.string.hostname);

        membersParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(BookAppointmentActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                showDialog(1);
            }
        });

        doctor_id = getIntent().getStringExtra(Const.DOCTOR_ID);
        doctor_name = getIntent().getStringExtra(Const.DOCTOR_NAME);
        doctor_dept = getIntent().getStringExtra(Const.DOCTOR_DEPARTMENT);
        doctor_pic = getIntent().getStringExtra(Const.DOCTOR_PIC);

        setDoctorProfile(doctor_name,doctor_dept,doctor_pic);

        startTime = getIntent().getStringExtra("start_time");
        day = getIntent().getStringExtra("day");
        //doctor_id = "LSHHI155";
        if (startTime.contains("AM")) {
            app_time = startTime.substring(0, 5);
        } else {
            if (startTime.startsWith("12")) {
                app_time = startTime.substring(0, 5);
            } else {
                Integer min = 720 + (Integer.parseInt(startTime.substring(0, 2)) * 60) + Integer.parseInt(startTime.substring(3, 5));
                DateFormat df = new SimpleDateFormat("HH:mm");
                Date date = new Date(0, 0, 0, min / 60, min % 60);
                app_time = df.format(date);
            }
        }
        if (day != null && Constants.month != null) {
            app_date = android.text.format.DateFormat.format("yyyy-MM", Constants.month) + "-" + day;
        } else {
            app_date = null;
        }
        //app_date = "2015-09-10";
        title.setText(startTime + ", " + day + " " + android.text.format.DateFormat.format("MMMM yyyy", Constants.month));

        pay_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (button_color.contentEquals("orange")) {
                    isInternetPresent = cd.isConnectingToInternet();
                    if (!isInternetPresent) {
                        // Internet Connection is not present
                        alert.showAlertDialog(BookAppointmentActivity.this, "Internet Connection Error",
                                "Please connect to working Internet connection", false);
                        // stop executing code by return
                        return;
                    }

                    //fetching age
                    DatabaseHandler db = new DatabaseHandler(context);
                    List<Contact> contactList = db.getAllContacts();
                    Contact contact = contactList.get(0);
                    if (selectedMemberModel != null) {
                        if (selectedMemberModel.getPatientId().equals("") || selectedMemberModel.getCountry().equals("") ||
                                selectedMemberModel.getCity().equals("") || selectedMemberModel.getLocality().equals("")) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                    context);

                            // set title
                            //alertDialogBuilder.setTitle("Your Title");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("please update your profile.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intent1 = new Intent(BookAppointmentActivity.this, UpdateProfileActivity.class);
                                            startActivity(intent1);
                                        }
                                    });


                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                        } else {
                            GenerateAppointment(null);
                        }
                    } else {
                        showToast("Please contact admin of sarvodaya hospital");
                    }
                }
            }


        });

        pay_now.setOnClickListener(this);

        part1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(BookAppointmentActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }

                startDoctorProfileActivity(doctor_id);
            }
        });

        TaskLoadDoctorByDoctorId();

        Constants.PaymentStatus = "";

    }

    private void setDoctorProfile(String doctor_name, String doctor_dep, String doctor_pic) {
        name.setText(doctor_name!=null?doctor_name:"");
        department.setText(doctor_dep!=null?doctor_dep:"");
        if(doctor_pic==null||doctor_pic.equals("")) {
            profile_pic.setImageResource(R.mipmap.all_doctors_pic);
        } else {
            Picasso.get().load(doctor_pic).transform(new CircleTransform())
                    .placeholder(R.mipmap.all_doctors_pic)
                    .error(R.mipmap.all_doctors_pic)
                    .into(profile_pic);
        }
    }

    private void startDoctorProfileActivity(String doctorId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent intent1 = new Intent(BookAppointmentActivity.this, DoctorProfileActivity.class);
            intent1.putExtra("doctor_id", doctorId);
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(BookAppointmentActivity.this, profile_pic,getSt(R.string.transition_profile));
            startActivity(intent1, options.toBundle());
            return;
        }
        Intent intent1 = new Intent(BookAppointmentActivity.this, DoctorProfileActivity.class);
        intent1.putExtra("doctor_id", doctorId);
        startActivity(intent1);
    }

    private void TaskLoadDoctorByDoctorId() {
        new HttpAsyncTaskLoadDoctorByDoctorId().execute(AppSettings.WEB_SERVICE_URL
                + "LoadDoctorByDoctorId?Doctor_ID=" + doctor_id);

    }

    private void GenerateAppointment(String errorMsg) {
//		String patient_id = membersMap.get(membersText.getText().toString().trim());
        String patient_id = AppUser.getSelectedPatientId(BookAppointmentActivity.this);
        String url = AppSettings.WEB_SERVICE_URL
                + "GenerateAppointment?Patient_Id=" + patient_id + "&DoctorId=" + doctor_id + "&AppDate=" + app_date + "&AppTime=" + app_time + "&Source=MA";
        url = url + "&DeseaseDesc=" + descText.getText().toString().trim();
        if (app_date != null && patient_id != null && doctor_id != null && app_time != null) {
            new HttpAsyncTaskGenerateAppointment(patient_id, errorMsg).execute(url);
        } else {
            String errMsg = "Reason: plz check patientId, dr id, app date or time not being null ,AppVersion:" + BuildConfig.VERSION_NAME + ",AppVersionCode:" + String.valueOf(BuildConfig.VERSION_CODE) + "Phone No:" + userPhoneNo + " ,URL= " + url;
            new ErrorReportingTask(errMsg).execute();
            String message = "Sorry we could not generate your appointment at this time. Please contact Sarvodaya team through telephone to book your appointment";
            showAlert(message);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        DatabaseHandler db = new DatabaseHandler(context);
        List<Contact> contactList = db.getAllContacts();

        new HttpAsyncTaskLoadMembers().execute(AppSettings.WEB_SERVICE_URL
                + "LoadPatientData?PatientId=&Mobile=" + contactList.get(0).getPhoneNumber() + "&Landline=");

    }


    private void showAlert(String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        alertDialogBuilder
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                    }
                });


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        Dialog dialog = null;

        switch (id) {
            case 1:
                dialog = new Dialog(BookAppointmentActivity.this);
                dialog.setContentView(R.layout.specialitylayout);
                dialog.setTitle("MEMBERS");

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);

                dialog.setOnCancelListener(new OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(FindDoctorActivity.this, "OnCancelListener",Toast.LENGTH_LONG).show();
                    }
                });

                dialog.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(BookAppointmentActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
                    }
                });

                // Prepare ListView in dialog
                dialog_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        R.layout.listlayout, listContent);
                dialog_ListView.setAdapter(adapter);
                dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        if (parent.getItemAtPosition(position).toString().contentEquals("SELECT MEMBER")) {
                            membersText.setText(parent.getItemAtPosition(position).toString());
                            pay_later.setBackgroundResource(R.drawable.pay_later_button_grey);
                            pay_now.setBackgroundResource(R.drawable.pay_now_button_grey);
                            button_color = "grey";
                        } else if (parent.getItemAtPosition(position).toString().contentEquals("ADD A NEW MEMBER")) {
                            isInternetPresent = cd.isConnectingToInternet();
                            if (!isInternetPresent) {
                                // Internet Connection is not present
                                alert.showAlertDialog(BookAppointmentActivity.this, "Internet Connection Error",
                                        "Please connect to working Internet connection", false);
                                // stop executing code by return
                                return;
                            }

                            Intent i = new Intent(BookAppointmentActivity.this, AddMemberActivity.class);
                            startActivity(i);
                        } else {
                            String patirntName = parent.getItemAtPosition(position).toString();
                            membersText.setText(patirntName);
                            String patient_id = membersMap.get(patirntName);
                            AppUser.setSelectedPatientId(BookAppointmentActivity.this, patient_id);
                            AppUser.setSelectedPatientDetail(BookAppointmentActivity.this, patirntName);
                            if (membersList != null)
                                for (Members obj : membersList) {
                                    if (obj.getPatientId().equalsIgnoreCase(patient_id)) {
                                        selectedMemberModel = obj;
                                    }
                                }
                            pay_later.setBackgroundResource(R.drawable.pay_later_button);
                            pay_now.setBackgroundResource(R.drawable.pay_now_button);
                            button_color = "orange";
                        }
                        removeDialog(1);
                    }
                });

                break;
        }

        return dialog;
    }

    private class HttpAsyncTaskLoadDoctorByDoctorId extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return ApiCall.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    List<TempDoctor> arrayList = sGson.fromJson(array.toString(), TempDoctor.getJsonArrayType());
                    name.setText(arrayList.get(0).getDoctorName());
                    b_doctorname = arrayList.get(0).getDoctorName();
                    String dept = arrayList.get(0).getDepartment().replace("&amp;", "&");
                    department.setText(dept);
                    fees.setText("Rs. " + arrayList.get(0).getDoctorFee());
                    //totalamount = 1.00;
                    totalamount = Double.valueOf(arrayList.get(0).getDoctorFee());
                    doctor_name=arrayList.get(0).getDoctorName();
                    doctor_dept=dept;

                    String imageUrl = "";

                    if (arrayList.get(0).getImage().contentEquals("YES")) {
                        imageUrl = arrayList.get(0).getImageURL();
                        doctor_pic=imageUrl;
                        Picasso.get().load(imageUrl).transform(new CircleTransform())
                                .placeholder(R.mipmap.all_doctors_pic)
                                .error(R.mipmap.all_doctors_pic)
                                .into(profile_pic);
                    } else {
                        profile_pic.setImageResource(R.mipmap.all_doctors_pic);
                    }
                } else {
                    RelativeLayout retry = showBaseServerErrorAlertBox(result);
                    retry.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            TaskLoadDoctorByDoctorId();
                        }
                    });
                }

            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
        }
    }

    private class HttpAsyncTaskLoadMembers extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(BookAppointmentActivity.this, "Loading data, Keep patience!");
        }

        @Override
        protected String doInBackground(String... urls) {

            return ApiCall.GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    membersList = sGson.fromJson(array.toString(), Members.getJsonArrayType());
                    String selectedId = AppUser.getSelectedPatientId(BookAppointmentActivity.this);
                    if (membersList != null) {
                        if (membersList.get(0).getErrorCode() != null) {
                            Toast.makeText(getApplicationContext(), membersList.get(0).getErrorMsg(), Toast.LENGTH_LONG).show();
                        } else {
                            listContent = new String[membersList.size() + 2];
                            Integer membersCount = 0;
                            membersMap.clear();
                            listContent[membersCount++] = "SELECT MEMBER";
                            for (Members member : membersList) {
                                listContent[membersCount++] = member.getTitle().trim() + " " + member.getPatientName().toString().trim();
                                membersMap.put(member.getTitle().trim() + " " + member.getPatientName().toString().trim(), member.getPatientId());
                                if (member.getPatientId().equalsIgnoreCase(selectedId)) {
                                    selectedMemberModel = member;
                                }
                            }
                            listContent[membersCount++] = "ADD A NEW MEMBER";

                        }
                    }
                }
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
//			dialog.dismiss();
            hideProgressDialog();
        }
    }

    private class HttpAsyncTaskGenerateAppointment extends AsyncTask<String, Void, String> {

        private final String patient_id;
        private final String errorMsg;

        public HttpAsyncTaskGenerateAppointment(String patient_id, String errorMsg) {
            this.patient_id = patient_id;
            this.errorMsg = errorMsg;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog(BookAppointmentActivity.this, "Generating appointment, Keep patience!");

        }

        @Override
        protected String doInBackground(String... urls) {
            if (errorMsg == null) {
                response = ApiCall.GET(urls[0]);
                return response;
            } else {
                response = ApiCall.POST(AppSettings.REPORT_SERVER, RequestBuilder.ErrorText("Phone:" + userPhoneNo + " Patient Id:" + patient_id + "," + urls[0] + "," + errorMsg));
                return ApiCall.GET(urls[0]);
            }
        }

        @Override
        protected void onPostExecute(final String result) {
            try {
                if (result.contains("AppointmentID") || result.contains("Doctor is not available for this particular time") || result.contains("More than two days future data can't be taken") || result.contains("Back time appointment cannot be taken")) {

                    if (result.contains("AppointmentID")) {
                        msg = "Your Apointment Id is " + result.substring(result.lastIndexOf(":") + 2, result.lastIndexOf("}]") - 1);

                        //adding member to session
//						SharedPreferences app_preferences = PreferenceManager
//								.getDefaultSharedPreferences(context);
//
//						SharedPreferences.Editor editor = app_preferences.edit();
//						editor.putString("member", membersText.getText().toString());
//						editor.commit();
                    } else if (result.contains("Doctor is not available for this particular time")) {
                        msg = "Doctor is not available for this particular time.";
                    } else if (result.contains("More than two days future data can't be taken")) {
                        msg = "You can only book appointment 2 days in advance.";
                    } else if (result.contains("Back time appointment cannot be taken")) {
                        msg = "Back time appointment cannot be taken.";
                    }
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            context);

                    // set title
                    //alertDialogBuilder.setTitle("Your Title");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage(msg)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, close
                                    // current activity
                                    if (result.contains("AppointmentID")) {
                                        Intent intent = new Intent(BookAppointmentActivity.this, TransactionHistoryActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        BookAppointmentActivity.this.finish();
                                        startActivity(intent);
                                    }
                                    //BookAppointmentActivity.this.finish();
                                }
                            });


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                } else {
                    RelativeLayout retry = showBaseServerErrorAlertBox(result);
                    retry.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            GenerateAppointment(result);
                        }
                    });
                }
            } catch (Exception pe) {
                System.out.println(pe);
            }
            hideProgressDialog();
//			dialog.dismiss();
        }
    }

//	private class HttpAsyncTaskGenerateAppointmentWithPayment extends AsyncTask<String, Void, String> {
//		private final String patient_id;
//		private final String errorMsg;
//
//		public HttpAsyncTaskGenerateAppointmentWithPayment(String patient_id, String errorMsg) {
//			this.patient_id=patient_id;
//			this.errorMsg=errorMsg;
//		}
//
//		@Override
//        protected void onPreExecute() {
//			showProgressDialog(BookAppointmentActivity.this,"Generating appointment, Keep patience!");
//        }
//
//		@Override
//		protected String doInBackground(String... urls) {
//
////			HttpReq httpReq=new HttpReq();
//			if(errorMsg==null){
//				return  ApiCall.GET(urls[0]);
//			}else{
////				List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
////				params.add(new BasicNameValuePair("errorText", "Patient Id:"+patient_id+","+urls[0]+","+errorMsg));
//				response = ApiCall.POST(AppSettings.REPORT_SERVER, RequestBuilder.ErrorText("Phone:"+userPhoneNo+" Patient Id:"+patient_id+","+urls[0]+","+errorMsg));
//				return ApiCall.GET(urls[0]);
////				httpReq.makeConnection(url,HttpReq.GET);
////				String res=httpReq.makeConnection(AppSettings.REPORT_SERVER,HttpReq.POST,params);
////				return httpReq.makeConnection(urls[0],HttpReq.GET);
////				return GET2(urls[0]);
//			}
//		}
//
//		@Override
//		protected void onPostExecute(final String result) {
//			try {
//
//				if(result.contains("AppointmentID") || result.contains("Doctor is not available for this particular time") || result.contains("More than two days future data can't be taken") || result.contains("Back time appointment cannot be taken")) {
//					JSONParser parser = new JSONParser();
//	 				String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
//					Object obj = parser.parse(finalResult);
//					JSONArray array = (JSONArray) obj;
//					Gson sGson = new Gson();
//					List<AppointmentWithPayment> arrayList = sGson.fromJson(array.toString(), AppointmentWithPayment.getJsonArrayType());
//
//					if(arrayList.size() > 0) {
//						msg = "Your Apointment Id is " + arrayList.get(0).getAppointmentId() + "\nPayement Id : " + arrayList.get(0).getPaymentRefNo() + "\nTxn No : " + arrayList.get(0).getTransactionRefNo();
//
//						//adding member to session
////						SharedPreferences app_preferences = PreferenceManager
////								.getDefaultSharedPreferences(context);
////
////						SharedPreferences.Editor editor = app_preferences.edit();
////						editor.putString("member", membersText.getText().toString());
////						editor.commit();
//					}
//					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//							context);
//
//						// set title
//						//alertDialogBuilder.setTitle("Your Title");
//
//						// set dialog message
//						alertDialogBuilder
//							.setMessage(msg)
//							.setCancelable(false)
//							.setPositiveButton("OK",new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog,int id) {
//									// if this button is clicked, close
//									// current activity
//									if(result.contains("AppointmentID")) {
//										Intent intent = new Intent(BookAppointmentActivity.this, TransactionHistoryActivity.class);
//										intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//										BookAppointmentActivity.this.finish();
//										startActivity(intent);
//									}
//									//BookAppointmentActivity.this.finish();
//								}
//							  });
//							// create alert dialog
//							AlertDialog alertDialog = alertDialogBuilder.create();
//
//							// show it
//							alertDialog.show();
//				} else {
//					if (Constants.PaymentStatus.contentEquals("Authorized")) {
//						RelativeLayout retry=showBaseServerErrorAlertBox(result);
//						retry.setOnClickListener(new OnClickListener() {
//							@Override
//							public void onClick(View view) {
//								hideBaseServerErrorAlertBox();
//								Log.e("Payment Response", PaymentRequest.getInstance()
//										.getPaymentResponse());
//								GenerateAppointmentWithPayment(patient_id,result);
//							}
//						});
//					}else{
//						Toast.makeText(getApplicationContext(),"PaymentStatus UnAuthorized",Toast.LENGTH_LONG).show();
//					}
//				}
//			} catch (Exception pe) {
//				System.out.println(pe);
//			}
//			hideProgressDialog();
////			dialog.dismiss();
//		}
//	}


    private class ErrorReportingTask extends AsyncTask<Void, Void, String> {
        private final String errorMsg;

        public ErrorReportingTask(String errorMsg) {
            this.errorMsg = errorMsg;
        }

        @Override
        protected String doInBackground(Void... urls) {
            response = ApiCall.POST(AppSettings.REPORT_SERVER, RequestBuilder.ErrorText(errorMsg));
            return response;
        }
    }


    private void callEbsKit(BookAppointmentActivity buyProduct) {
        /**
         * Set Parameters Before Initializing the EBS Gateway, All mandatory
         * values must be provided
         */
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyMMddhhmmss");

        /** Payment Amount Details */
        // Total Amount

        PaymentRequest.getInstance().setTransactionAmount(
                String.format("%.2f", totalamount));
        L.m("totalamount :" + totalamount);
        /** Mandatory */

        PaymentRequest.getInstance().setAccountId(ACC_ID);
        PaymentRequest.getInstance().setSecureKey(SECRET_KEY);

        // Reference No
        transactionRefNo = "AN" + formatter.format(today);

        L.m("transactionRefNo :" + transactionRefNo);
        PaymentRequest.getInstance().setReferenceNo(transactionRefNo);
        /** Mandatory */

        // Email Id
        L.m("b_email :" + b_email);
        PaymentRequest.getInstance().setBillingEmail(b_email);
        /** Mandatory */

        /**
         * Set failure id as 1 to display amount and reference number on failed
         * transaction page. set 0 to disable
         */
        PaymentRequest.getInstance().setFailureid("0");
        /** Mandatory */

        // Currency
        PaymentRequest.getInstance().setCurrency("INR");
        /** Mandatory */

        /** Optional */
        // Your Reference No or Order Id for this transaction
        PaymentRequest.getInstance().setTransactionDescription(
                "Appointment with " + b_doctorname);

        /** Billing Details */
        L.m("b_name :" + b_name);
        PaymentRequest.getInstance().setBillingName(b_name);
        /** Optional */
        L.m("b_address :" + b_address);
        PaymentRequest.getInstance().setBillingAddress(b_address);
        /** Optional */
        L.m("b_city :" + b_city);
        PaymentRequest.getInstance().setBillingCity(b_city);
        /** Optional */
        L.m("b_pincode :" + b_pincode);
        PaymentRequest.getInstance().setBillingPostalCode(b_pincode);
        /** Optional */
        L.m("b_state :" + b_state);
        PaymentRequest.getInstance().setBillingState(b_state);
        /** Optional */
        L.m("b_country :" + b_country.substring(0, 2));
        PaymentRequest.getInstance().setBillingCountry(b_country.substring(0, 2));
        // ** Optional */
        L.m("b_mobile :" + b_mobile);
        PaymentRequest.getInstance().setBillingPhone(b_mobile);
        /** Optional */
        /** set custom message for failed transaction */

        PaymentRequest.getInstance().setFailuremessage(
                getResources().getString(R.string.payment_failure_message));
        /** Optional */
        /** Shipping Details */
        PaymentRequest.getInstance().setShippingName(b_name);
        /** Optional */
        PaymentRequest.getInstance().setShippingAddress(b_address);
        /** Optional */
        PaymentRequest.getInstance().setShippingCity(b_city);
        /** Optional */
        PaymentRequest.getInstance().setShippingPostalCode(b_pincode);
        /** Optional */
        PaymentRequest.getInstance().setShippingState(b_state);
        /** Optional */
        PaymentRequest.getInstance().setShippingCountry(b_country.substring(0, 2));
        /** Optional */
        PaymentRequest.getInstance().setShippingEmail(b_email);
        /** Optional */
        PaymentRequest.getInstance().setShippingPhone(b_mobile);
        /** Optional */
        /* enable log by setting 1 and disable by setting 0 */
        PaymentRequest.getInstance().setLogEnabled("0");

        /**
         * Initialise parameters for dyanmic values sending from merchant custom
         * values from merchant
         */
        String patient_id = AppUser.getSelectedPatientId(BookAppointmentActivity.this);

        PaymentManager.set(buyProduct, patient_id, doctor_id, app_date, app_time, "MA"
                , descText.getText().toString().trim(), String.format("%.2f", totalamount), transactionRefNo, userPhoneNo);

        custom_post_parameters = new ArrayList<>();
        HashMap<String, String> hashpostvalues = new HashMap<>();
        hashpostvalues.put("account_details", "saving");
        hashpostvalues.put("merchant_type", "gold");
        custom_post_parameters.add(hashpostvalues);

        PaymentRequest.getInstance()
                .setCustomPostValues(custom_post_parameters);
        /** Optional-Set dyanamic values */

        // PaymentRequest.getInstance().setFailuremessage(getResources().getString(R.string.payment_failure_message));

        EBSPayment.getInstance().init(buyProduct, ACC_ID, SECRET_KEY,
                Mode.ENV_LIVE, Encryption.ALGORITHM_MD5, HOST_NAME);

        // EBSPayment.getInstance().init(context, accId, secretkey, environment,
        // algorithm, host_name);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.pay_now) {
            if (button_color.contentEquals("orange")) {
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(BookAppointmentActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                if (termsCheckBox.isChecked()) {
                    //fetching age

                    if(b_mobile==null||b_mobile.equals("")) {
                        DatabaseHandler db = new DatabaseHandler(context);
                        List<Contact> contactList = db.getAllContacts();
                        Contact contact = contactList.get(0);
                        b_mobile = contact.getPhoneNumber();
                    }
//                    String location = contact.getLocation();
                    if (selectedMemberModel != null) {
                        b_mobile=selectedMemberModel.getMobile();

                        if (selectedMemberModel.getPatientId().equals("") || selectedMemberModel.getCountry().equals("") ||
                                selectedMemberModel.getCity().equals("") || selectedMemberModel.getLocality().equals("")) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                    context);
                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("Please update your profile.")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent intent1 = new Intent(BookAppointmentActivity.this, UpdateProfileActivity.class);
                                            startActivity(intent1);
                                        }
                                    });


                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                        }else{
                            CustomAddPaymentDetails cpd = new CustomAddPaymentDetails(BookAppointmentActivity.this, context, BookAppointmentActivity.this, totalamount);
                            cpd.show();
                            cpd.setCancelable(false);
                            cpd.setCanceledOnTouchOutside(false);
                        }
                    }else{
                        showToast("Please contact admin of sarvodaya hospital");
                    }
//                    if (TextUtils.isEmpty(location)) {
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//                                context);
//
//                        // set dialog message
//                        alertDialogBuilder
//                                .setMessage("Please update your location.")
//                                .setCancelable(false)
//                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        Intent intent1 = new Intent(BookAppointmentActivity.this, UpdateProfileActivity.class);
//                                        startActivity(intent1);
//                                    }
//                                });
//
//
//                        // create alert dialog
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//
//                        // show it
//                        alertDialog.show();
//                    } else {
//
//                        CustomAddPaymentDetails cpd = new CustomAddPaymentDetails(BookAppointmentActivity.this, context, BookAppointmentActivity.this, totalamount);
//                        cpd.show();
//                        cpd.setCancelable(false);
//                        cpd.setCanceledOnTouchOutside(false);
//                    }


                } else {
                    Toast.makeText(getApplicationContext(), "Please accept to Hospital's Terms and Conditions", Toast.LENGTH_SHORT).show();
                }
            }


        }
    }

    @Override
    public void sendDetails(String name, String email, String address,
                            String city, String state, String country, String pincode) {
        b_name = name;
        b_email = email;
        b_address = address;
        b_city = city;
        b_state = state;
        b_country = country;
        b_pincode = pincode;
        callEbsKit(this);
    }
}
