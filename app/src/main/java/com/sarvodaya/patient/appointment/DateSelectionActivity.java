package com.sarvodaya.patient.appointment;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.CalendarDays;
import com.sarvodayahospital.beans.DaysAvailable;
import com.sarvodayahospital.beans.TempDoctor;
import com.sarvodayahospital.calendar.CalendarAdapter;
import com.sarvodayahospital.calendar.HeaderAdapter;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.CircleTransform;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.Const;
import com.sarvodayahospital.util.Constants;
import com.sarvodayahospital.util.GridViewScrollable;
import com.sarvodayahospital.util.RoundImage;
import com.sarvodayahospital.util.TypefaceSpan;
import com.squareup.picasso.Picasso;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class DateSelectionActivity extends BaseActivity {
	final Context context = this;
	public GregorianCalendar month;// calendar instances.

	public CalendarAdapter adapter;// adapter instance
	public HeaderAdapter headerAdapter;
	public Handler handler;// for grabbing some event values for showing the dot
							// marker.
	public ArrayList<String> items; // container to store calendar items which
									// needs showing the event marker
	ArrayList<String> event;
	ArrayList<String> date;
	ArrayList<String> desc;
	RelativeLayout previous, next, relativeLayoutProfile;
	LinearLayout part1;
	ImageView profile_pic;
	RoundImage roundImageProfileBackground;
	TextView name, department, fees;
	GridViewScrollable gridview;
	ProgressDialog dialog;

	private String doctor_id,doctor_name,doctor_pic,doctor_dept;

	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selection_date);
		
		SpannableString s = new SpannableString("Book an Appointment");
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
		            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		Locale.setDefault(Locale.US);
		profile_pic = (ImageView) findViewById(R.id.img_profile);
		part1 = (LinearLayout) findViewById(R.id.part1);
		relativeLayoutProfile = (RelativeLayout) findViewById(R.id.relative_layout_profile);
		name = (TextView) findViewById(R.id.name);
		department = (TextView) findViewById(R.id.department);
		fees = (TextView) findViewById(R.id.fees);



		//doctor_id = "LSHHI155";
        doctor_id = getIntent().getStringExtra(Const.DOCTOR_ID);
		doctor_name = getIntent().getStringExtra(Const.DOCTOR_NAME);
		doctor_dept = getIntent().getStringExtra(Const.DOCTOR_DEPARTMENT);
		doctor_pic = getIntent().getStringExtra(Const.DOCTOR_PIC);

		setDoctorProfile(doctor_name,doctor_dept,doctor_pic);

		month = (GregorianCalendar) GregorianCalendar.getInstance();
		Constants.month = month;
			
		headerAdapter =  new HeaderAdapter(this);

		gridview = (GridViewScrollable) findViewById(R.id.gridview);
		
		// Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());
				

		GridViewScrollable headerView = (GridViewScrollable) findViewById(R.id.headerview);
		headerView.setAdapter(headerAdapter);
		
		TextView title = (TextView) findViewById(R.id.title);
		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

		previous = (RelativeLayout) findViewById(R.id.previous);

		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setPreviousMonth();
				refreshCalendar();
				next.setVisibility(View.VISIBLE);
				previous.setVisibility(View.GONE);
			}
		});
		previous.setVisibility(View.GONE);

		next = (RelativeLayout) findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setNextMonth();
				refreshCalendar();
				previous.setVisibility(View.VISIBLE);
				next.setVisibility(View.GONE);
			}
		});
		
		part1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				isInternetPresent = cd.isConnectingToInternet();
				if (!isInternetPresent) {
					// Internet Connection is not present
					alert.showAlertDialog(DateSelectionActivity.this, "Internet Connection Error",
							"Please connect to working Internet connection", false);
					// stop executing code by return
					return;
				}
				startDoctorProfileActivity(doctor_id);
			}
		});

		
		new HttpAsyncTaskLoadDoctorByDoctorId().execute(AppSettings.WEB_SERVICE_URL
				+ "LoadDoctorByDoctorId?Doctor_ID=" + doctor_id);
	}

	private void setDoctorProfile(String doctor_name, String doctor_dep, String doctor_pic) {
		name.setText(doctor_name!=null?doctor_name:"");
		department.setText(doctor_dep!=null?doctor_dep:"");
		if(doctor_pic==null||doctor_pic.equals("")) {
			profile_pic.setImageResource(R.mipmap.all_doctors_pic);
		} else {
			Picasso.get().load(doctor_pic)
					.transform(new CircleTransform())
					.placeholder(R.mipmap.all_doctors_pic)
					.error(R.mipmap.all_doctors_pic)
					.into(profile_pic);
		}
	}

	private void startDoctorProfileActivity(String doctorId) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Intent intent1 = new Intent(DateSelectionActivity.this, DoctorProfileActivity.class);
			intent1.putExtra("doctor_id", doctorId);
			ActivityOptions options = ActivityOptions
					.makeSceneTransitionAnimation(DateSelectionActivity.this, profile_pic,getSt(R.string.transition_profile));
			startActivity(intent1, options.toBundle());
			return;
		}
		Intent intent1 = new Intent(DateSelectionActivity.this, DoctorProfileActivity.class);
		intent1.putExtra("doctor_id", doctor_id);
		startActivity(intent1);
	}

	protected void setNextMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMaximum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) + 1),
					month.getActualMinimum(GregorianCalendar.MONTH), 1);
		} else {
			 month.add((GregorianCalendar.MONTH), 1);
		}

	}

	protected void setPreviousMonth() {
		if (month.get(GregorianCalendar.MONTH) == month
				.getActualMinimum(GregorianCalendar.MONTH)) {
			month.set((month.get(GregorianCalendar.YEAR) - 1),
					month.getActualMaximum(GregorianCalendar.MONTH), 1);
		} else {
			 month.add((GregorianCalendar.MONTH), -1);
		}

	}

	

	public void refreshCalendar() {
		TextView title = (TextView) findViewById(R.id.title);

		new HttpAsyncTaskLoadDoctorByDoctorId().execute(AppSettings.WEB_SERVICE_URL
				+ "LoadDoctorByDoctorId?Doctor_ID=" + doctor_id);
		
		Constants.month = month;
		title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
	}


	private class HttpAsyncTaskLoadDoctorByDoctorId extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(DateSelectionActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(CalendarActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				JSONParser parser = new JSONParser();
				if(result.contains("[")){
					String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
					Object obj = parser.parse(finalResult);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					List<TempDoctor> arrayList = sGson.fromJson(array.toString(), TempDoctor.getJsonArrayType());
					name.setText(arrayList.get(0).getDoctorName());
					String dept = arrayList.get(0).getDepartment().replace("&amp;", "&");
					department.setText(dept);
					fees.setText("Rs. " + arrayList.get(0).getDoctorFee());
					doctor_name=arrayList.get(0).getDoctorName();
					doctor_dept=dept;

					String imageUrl = "";


					if(arrayList.get(0).getImage().contentEquals("YES")) {
						imageUrl = arrayList.get(0).getImageURL();
						doctor_pic=imageUrl;
						Picasso.get().load(imageUrl).transform(new CircleTransform())
								.placeholder(R.mipmap.all_doctors_pic)
								.error(R.mipmap.all_doctors_pic)
								.into(profile_pic);
					} else {
						profile_pic.setImageResource(R.mipmap.all_doctors_pic);
					}
//					Bitmap bm1 = BitmapFactory.decodeResource(getResources(),R.drawable.profile_pic_background);
//					roundImageProfileBackground = new RoundImage(bm1);
//					profile_pic_background.setImageDrawable(roundImageProfileBackground);


					Map<String, DaysAvailable> daysAvailableList = new HashMap<>();
					for(TempDoctor tempDoctor : arrayList) {
						daysAvailableList.put(tempDoctor.getDay(), new DaysAvailable(tempDoctor.getDay(), tempDoctor.getStartTime(), tempDoctor.getEndTime()));
					}

					List<CalendarDays> calendarDaysList ;
					calendarDaysList = getMonthDays(month.get(Calendar.MONTH), month.get(Calendar.YEAR), daysAvailableList);
					adapter = new CalendarAdapter(context, calendarDaysList, doctor_id,doctor_name,doctor_dept,doctor_pic, DateSelectionActivity.this,month,profile_pic);
					gridview.setAdapter(adapter);
				}else{
					Toast.makeText(DateSelectionActivity.this,"Server not responding please retry or try after sometime.",Toast.LENGTH_LONG).show();
				}
				
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
	}
	
	public int daysOfMonth(Integer month, Integer year) {
		GregorianCalendar  cal = new GregorianCalendar(); 
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, 4);
		cal.set(Calendar.YEAR, year);
		int numberOfDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		return numberOfDays;
	}
	
	public List<CalendarDays> getMonthDays(Integer month, Integer year, Map<String, DaysAvailable> daysAvailableList) {
		List<CalendarDays> arrayList = new ArrayList<>();
		GregorianCalendar  cal = new GregorianCalendar(); 
		int monthMaxDays = daysOfMonth(month, year);   //no of days in month
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.YEAR, year);
		
		String dayOfMonth = getFirstDay(cal);
		switch(dayOfMonth) {
		case "Sunday":
			break;
		case "Monday":
			arrayList.add(new CalendarDays("","Grey"));
			break;
		case "Tuesday":
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			break;
		case "Wednesday":
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			break;
		case "Thursday":
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			break;
		case "Friday":
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			break;
		case "Saturday":
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			arrayList.add(new CalendarDays("","Grey"));
			break;
		}
		Calendar c = Calendar.getInstance();
		Date todaysDate = setTimeToMidnight(new Date());
		for(Integer i = 1; i <= monthMaxDays; i++) {
			c.set(year, month, i);
			Date d = c.getTime();
			
			if(d.before(todaysDate)) {
				arrayList.add(new CalendarDays(i.toString(),"Grey"));
			} else {
				String selectedDay = getSelectedDay(c);
				if(daysAvailableList.containsKey(selectedDay)) {
					arrayList.add(new CalendarDays(i.toString(),"Green"));
				} else {
					arrayList.add(new CalendarDays(i.toString(),"Grey"));
				}
			}
		}
		
		return arrayList;
	}
	public String getFirstDay(Calendar cal){
		cal.set(Calendar.DAY_OF_MONTH, 1);
	    int day = cal.get(Calendar.DAY_OF_WEEK);
	    String dayOfMonth = "";
	    switch(day) {
	    case Calendar.SUNDAY : 
	    	dayOfMonth = "Sunday";
	    	break;
	    case Calendar.MONDAY : 
	    	dayOfMonth = "Monday";
	    	break;
	    case Calendar.TUESDAY : 
	    	dayOfMonth = "Tuesday";
	    	break;
	    case Calendar.WEDNESDAY : 
	    	dayOfMonth = "Wednesday";
	    	break;
	    case Calendar.THURSDAY : 
	    	dayOfMonth = "Thursday";
	    	break;
	    case Calendar.FRIDAY : 
	    	dayOfMonth = "Friday";
	    	break;
	    case Calendar.SATURDAY : 
	    	dayOfMonth = "Saturday";
	    	break;
	    }
	    return dayOfMonth;
	}
	
	public String getSelectedDay(Calendar cal){
		int day = cal.get(Calendar.DAY_OF_WEEK);
	    String dayOfMonth = "";
	    switch(day) {
	    case Calendar.SUNDAY : 
	    	dayOfMonth = "Sunday";
	    	break;
	    case Calendar.MONDAY : 
	    	dayOfMonth = "Monday";
	    	break;
	    case Calendar.TUESDAY : 
	    	dayOfMonth = "Tuesday";
	    	break;
	    case Calendar.WEDNESDAY : 
	    	dayOfMonth = "Wednesday";
	    	break;
	    case Calendar.THURSDAY : 
	    	dayOfMonth = "Thursday";
	    	break;
	    case Calendar.FRIDAY : 
	    	dayOfMonth = "Friday";
	    	break;
	    case Calendar.SATURDAY : 
	    	dayOfMonth = "Saturday";
	    	break;
	    }
	    return dayOfMonth;
	}
	
	public static Date setTimeToMidnight(Date date) {
	    Calendar calendar = Calendar.getInstance();

	    calendar.setTime( date );
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);

	    return calendar.getTime();
	}
}
