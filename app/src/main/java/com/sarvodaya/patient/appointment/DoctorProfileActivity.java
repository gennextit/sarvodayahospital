package com.sarvodaya.patient.appointment;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.TempDoctor;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.CircleTransform;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.Const;
import com.sarvodayahospital.util.RoundImage;
import com.sarvodayahospital.util.TypefaceSpan;
import com.squareup.picasso.Picasso;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class DoctorProfileActivity extends BaseActivity {
    final Context context = this;
    ImageView profile_pic;
    RoundImage roundImageProfileBackground;
    ImageView bookAppointmentButton;
    TableLayout timings_table;
    private String doctor_id, doctor_name, doctor_pic, doctor_department;
    TextView name, department;
    ProgressDialog dialog;

    ConnectionDetector cd;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    // Alert Dialog Manager
    AlertDialogManager alert = new AlertDialogManager();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_profile);

        SpannableString s = new SpannableString("Doctor Profile");
        s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_launcher);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        getSupportActionBar().setTitle(s);
        profile_pic = (ImageView) findViewById(R.id.img_profile);
        bookAppointmentButton = (ImageView) findViewById(R.id.bookAppointmentButton);
        name = (TextView) findViewById(R.id.name);
        department = (TextView) findViewById(R.id.department);

        doctor_id = getIntent().getStringExtra(Const.DOCTOR_ID);
        doctor_name = getIntent().getStringExtra(Const.DOCTOR_NAME);
        doctor_pic = getIntent().getStringExtra(Const.DOCTOR_PIC);

        setDoctorProfile(doctor_name, doctor_pic);
        timings_table = (TableLayout) findViewById(R.id.timings_table);

        // Check if Internet present
        cd = new ConnectionDetector(getApplicationContext());

        bookAppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                if (!isInternetPresent) {
                    // Internet Connection is not present
                    alert.showAlertDialog(DoctorProfileActivity.this, "Internet Connection Error",
                            "Please connect to working Internet connection", false);
                    // stop executing code by return
                    return;
                }
                startCalenderActivity(doctor_id, doctor_name, doctor_department, doctor_pic);
            }
        });
        TaskLoadDoctorByDoctorId();

    }


    private void setDoctorProfile(String doctor_name, final String doctor_pic) {

        name.setText(doctor_name != null ? doctor_name : "");
        if (doctor_pic == null || doctor_pic.equals("")) {
            profile_pic.setImageResource(R.mipmap.all_doctors_pic);
        } else {
            Picasso.get().load(doctor_pic).transform(new CircleTransform())
                    .placeholder(R.mipmap.all_doctors_pic)
                    .error(R.mipmap.all_doctors_pic)
                    .into(profile_pic);
        }

    }

    private void startCalenderActivity(String drId, String drName, String drDept, String drPic) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent intent = new Intent(DoctorProfileActivity.this, DateSelectionActivity.class);
            intent.putExtra(Const.DOCTOR_ID, drId);
            intent.putExtra(Const.DOCTOR_NAME, drName);
            intent.putExtra(Const.DOCTOR_DEPARTMENT, drDept);
            intent.putExtra(Const.DOCTOR_PIC, drPic);
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(DoctorProfileActivity.this, profile_pic, getSt(R.string.transition_profile));
            startActivity(intent, options.toBundle());
            return;
        }
        Intent intent = new Intent(DoctorProfileActivity.this, DateSelectionActivity.class);
        intent.putExtra(Const.DOCTOR_ID, drId);
        intent.putExtra(Const.DOCTOR_NAME, drName);
        intent.putExtra(Const.DOCTOR_DEPARTMENT, drDept);
        intent.putExtra(Const.DOCTOR_PIC, drPic);
        startActivity(intent);

    }

    private void TaskLoadDoctorByDoctorId() {
        new HttpAsyncTaskLoadDoctorByDoctorId().execute(AppSettings.WEB_SERVICE_URL
                + "LoadDoctorByDoctorId?Doctor_ID=" + doctor_id);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        doctor_id = getIntent().getStringExtra("doctor_id");
        new HttpAsyncTaskLoadDoctorByDoctorId().execute(AppSettings.WEB_SERVICE_URL
                + "LoadDoctorByDoctorId?Doctor_ID=" + doctor_id);
    }


    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    private class HttpAsyncTaskLoadDoctorByDoctorId extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            showProgressDialog(DoctorProfileActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(DoctorProfileActivity.this);
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();

        }

        @Override
        protected String doInBackground(String... urls) {

//			return GET(urls[0]);
            return ApiCall.GET(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.contains("[")) {
                    timings_table.removeAllViews();
                    JSONParser parser = new JSONParser();
                    String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
                    Object obj = parser.parse(finalResult);
                    JSONArray array = (JSONArray) obj;
                    Gson sGson = new Gson();
                    List<TempDoctor> arrayList = sGson.fromJson(array.toString(), TempDoctor.getJsonArrayType());
                    name.setText(arrayList.get(0).getDoctorName());
                    String dept = arrayList.get(0).getDepartment().replace("&amp;", "&");
                    department.setText(dept);
                    doctor_name = arrayList.get(0).getDoctorName();
                    doctor_department = dept;
                    String imageUrl = "";
                    if (arrayList.get(0).getImage().contentEquals("YES")) {
                        imageUrl = arrayList.get(0).getImageURL();
                        doctor_pic = imageUrl;
                        Picasso.get().load(imageUrl).transform(new CircleTransform())
                                .placeholder(R.mipmap.all_doctors_pic)
                                .error(R.mipmap.all_doctors_pic)
                                .into(profile_pic);
                    } else {
                        profile_pic.setImageResource(R.mipmap.all_doctors_pic);
                    }
                    for (TempDoctor tempDoctor : arrayList) {
                        TableRow tr = new TableRow(context);
//				        tr.setId(1);
                        tr.setGravity(Gravity.CENTER_HORIZONTAL);
                        tr.setLayoutParams(new LayoutParams(
                                LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT));

                        TextView labelTV = new TextView(context);
//				        labelTV.setId(100);
                        labelTV.setText(tempDoctor.getDay() + "\t");
                        labelTV.setTextColor(Color.WHITE);
                        labelTV.setGravity(Gravity.LEFT);
                        labelTV.setLayoutParams(new LayoutParams(
                                LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT));
                        tr.addView(labelTV);

                        // Create a TextView to house the name of the province
                        TextView labelTV1 = new TextView(context);
//				        labelTV1.setId(200);
                        String startTime = tempDoctor.getStartTime().substring(0, 2);
                        String endTime = tempDoctor.getEndTime().substring(0, 2);
                        if (Integer.parseInt(startTime) > 12) {
                            startTime = (12 - Integer.parseInt(startTime)) + ":" + tempDoctor.getStartTime().substring(3, 5) + " PM";
                        } else if (Integer.parseInt(startTime) == 12) {
                            startTime = Integer.parseInt(startTime) + ":" + tempDoctor.getStartTime().substring(3, 5) + " PM";
                        } else {
                            startTime = tempDoctor.getStartTime().substring(0, 5) + " AM";
                        }

                        if (Integer.parseInt(endTime) >= 12) {
                            endTime = (Integer.parseInt(endTime) - 12) + ":" + tempDoctor.getEndTime().substring(3, 5) + " PM";
                        } else {
                            endTime = tempDoctor.getEndTime().substring(0, 5) + " AM";
                        }


                        labelTV1.setText(startTime + " - " + endTime);// TUESDAY - 2:00 PM - 4:00 PM");

                        labelTV1.setTextColor(Color.WHITE);
                        labelTV1.setGravity(Gravity.CENTER_HORIZONTAL);

                        labelTV1.setLayoutParams(new LayoutParams(
                                LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT));
                        tr.addView(labelTV1);

                        timings_table.addView(tr, new TableLayout.LayoutParams(
                                LayoutParams.FILL_PARENT,
                                LayoutParams.WRAP_CONTENT));
                    }
                } else {
                    RelativeLayout retry = showBaseServerErrorAlertBox(result);
                    retry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            hideBaseServerErrorAlertBox();
                            TaskLoadDoctorByDoctorId();
                        }
                    });
                }
            } catch (ParseException pe) {
                System.out.println("position: " + pe.getPosition());
                System.out.println(pe);
            }
            hideProgressDialog();
        }
    }

}
