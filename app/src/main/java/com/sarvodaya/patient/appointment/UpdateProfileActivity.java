package com.sarvodaya.patient.appointment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sarvodaya.patient.BaseActivity;
import com.sarvodaya.patient.DatabaseHandler;
import com.sarvodaya.patient.R;
import com.sarvodayahospital.beans.Contact;
import com.sarvodayahospital.beans.Location;
import com.sarvodayahospital.util.AlertDialogManager;
import com.sarvodayahospital.util.ApiCall;
import com.sarvodayahospital.util.AppSettings;
import com.sarvodayahospital.util.ConnectionDetector;
import com.sarvodayahospital.util.TypefaceSpan;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.List;

public class UpdateProfileActivity extends BaseActivity {
	final Context context = this;
	
	LinearLayout titleParent, countryParent, cityParent, locationParent, mobileHeadingParent, mobileParent;
	TextView titleText, countryText, cityText, locationText;
	EditText nameText, ageText, mobileText, addressText;
	String[] titleList = { "---Select title---", "Mr.", "Mrs.","Ms.", "Dr."};
	private RadioGroup radioSexGroup;
	private RadioButton radioSexButton;
	ImageView confirm_button;
	private RadioButton radioMale, radioFemale;
	ListView title_ListView;
	
	String[] countryList = {};
	String[] cityList = {};
	String[] locationList = {};
	
	ConnectionDetector cd;
	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();
	ProgressDialog dialog;
	Contact contact;
	Boolean cityCheck = true, locationCheck = true;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_profile);
		
		SpannableString s = new SpannableString("Update Profile");
		s.setSpan(new TypefaceSpan(this, "CircularStd-Book.otf"), 0, s.length(),
		            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(Color.parseColor("#008fc5")), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);    
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_launcher);
		getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
		getSupportActionBar().setTitle(s);
		titleParent = (LinearLayout) findViewById(R.id.titleParent);
		countryParent = (LinearLayout) findViewById(R.id.countryParent);
		cityParent = (LinearLayout) findViewById(R.id.cityParent);
		locationParent = (LinearLayout) findViewById(R.id.locationParent);
		mobileHeadingParent = (LinearLayout) findViewById(R.id.mobileHeadingParent);
		mobileParent = (LinearLayout) findViewById(R.id.mobileParent);
		nameText = (EditText) findViewById(R.id.nameText);
		ageText = (EditText) findViewById(R.id.ageText);
		mobileText = (EditText) findViewById(R.id.mobileText);
		addressText = (EditText) findViewById(R.id.addressText);
		countryText = (TextView) findViewById(R.id.countryText);
		cityText = (TextView) findViewById(R.id.cityText);
		locationText = (TextView) findViewById(R.id.locationText);
		radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
		confirm_button = (ImageView) findViewById(R.id.confirm_button);
		confirm_button.setBackgroundResource(R.drawable.confirm_button_orange);
		radioMale = (RadioButton) findViewById(R.id.radioMale);
		radioFemale = (RadioButton) findViewById(R.id.radioFemale);
		
		titleText = (TextView) findViewById(R.id.titleText);
		cityText.setText("---Select city---");
		locationText.setText("---Select location---");
		mobileHeadingParent.setVisibility(View.GONE);
		mobileParent.setVisibility(View.GONE);
		// Check if Internet present
		cd = new ConnectionDetector(getApplicationContext());
		
		//fetching contact number
		DatabaseHandler db = new DatabaseHandler(context);
		List<Contact> contactList = db.getAllContacts();
		mobileText.setText(contactList.get(0).getPhoneNumber());
		contact = contactList.get(0);
		titleText.setText(contact.getTitle());
		nameText.setText(contact.getName());
		ageText.setText(contact.getAge());
		if(contact.getGender().contentEquals("Male")) {
			radioMale.setChecked(true);
		} else {
			radioFemale.setChecked(true);
		}
		addressText.setText(contact.getAddress());
		
		titleParent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(2);
			}
		});
		countryParent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(3);
			}
		});
		cityParent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(4);
			}
		});
		locationParent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(5);
			}
		});
		
		confirm_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(titleText.getText().toString().contentEquals("---Select title---")) {
					Toast.makeText(getApplicationContext(), "Select title.", Toast.LENGTH_LONG).show();
				} else if(nameText.getText().length() == 0) {
					Toast.makeText(getApplicationContext(), "Enter name of patient.", Toast.LENGTH_LONG).show();
					nameText.requestFocusFromTouch();
			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE); 
			        lManager.showSoftInput(nameText, 0);
				} else if(ageText.getText().length() == 0) {
					Toast.makeText(getApplicationContext(), "Enter age.", Toast.LENGTH_LONG).show();
					ageText.requestFocusFromTouch();
			        InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
			        lManager.showSoftInput(ageText, 0);
				} else if(ageText.getText().toString().equalsIgnoreCase("0")) {
					Toast.makeText(getApplicationContext(), "Enter age.", Toast.LENGTH_LONG).show();
					ageText.requestFocusFromTouch();
					InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
					lManager.showSoftInput(ageText, 0);
				} else if(ageText.getText().toString().equalsIgnoreCase("0 yrs")) {
					Toast.makeText(getApplicationContext(), "Enter age is invalid.", Toast.LENGTH_LONG).show();
					ageText.requestFocusFromTouch();
					InputMethodManager lManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
					lManager.showSoftInput(ageText, 0);
				} else if(countryText.getText().toString().contentEquals("---Select country---")) {
					Toast.makeText(getApplicationContext(), "Select country.", Toast.LENGTH_LONG).show();
				} else if(cityText.getText().toString().contentEquals("---Select city---")) {
					Toast.makeText(getApplicationContext(), "Select city.", Toast.LENGTH_LONG).show();
				} else if(locationText.getText().toString().contentEquals("---Select location---")) {
					Toast.makeText(getApplicationContext(), "Select location.", Toast.LENGTH_LONG).show();
				} else {
					isInternetPresent = cd.isConnectingToInternet();
					if (!isInternetPresent) {
						// Internet Connection is not present
						alert.showAlertDialog(UpdateProfileActivity.this, "Internet Connection Error",
								"Please connect to working Internet connection", false);
						// stop executing code by return
						return;
					}
					String url = AppSettings.WEB_SERVICE_URL + "UpdateProfile?Patient_Id=" + contact.getPatientId() + "&Title=" + titleText.getText().toString()
							+ "&PatientName=" + nameText.getText().toString()
							+ "&Age=" + ageText.getText().toString() + "%20yrs"
							+ "&Mobile=" + mobileText.getText().toString();
					
					int selectedId = radioSexGroup.getCheckedRadioButtonId();
					radioSexButton = (RadioButton) findViewById(selectedId);
					
					url = url + "&Gender=" + radioSexButton.getText().toString();
					
					url = url + "&Address=" + addressText.getText().toString();
					
					if(countryText.getText().toString().length() > 0 && !countryText.getText().toString().contentEquals("---Select country---")) {
						url = url + "&Country=" + countryText.getText().toString();
					} else {
						url = url + "&Country=";
					}
					if(cityText.getText().toString().length() > 0 && !cityText.getText().toString().contentEquals("---Select city---")) {
						url = url + "&City=" + cityText.getText().toString();
					} else {
						url = url + "&City=";
					}
					if(locationText.getText().toString().length() > 0 && !locationText.getText().toString().contentEquals("---Select location---")) {
						url = url + "&Location=" + locationText.getText().toString();
					} else {
						url = url + "&Location=";
					}
					url = url.replace(" ", "%20");
					new HttpAsyncTaskUpdateProfile().execute(url);
				}
			}
		});
		
		new HttpAsyncTaskCountries().execute(AppSettings.WEB_SERVICE_URL
				+ "LoadCountries");
		
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {

		Dialog dialog = null;

		switch (id) {
		case 2:
			dialog = new Dialog(UpdateProfileActivity.this);
			dialog.setContentView(R.layout.specialitylayout);
			dialog.setTitle("Title");

			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);

			dialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//Toast.makeText(FindDoctorActivity.this, "OnCancelListener",Toast.LENGTH_LONG).show();
				}
			});

			dialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
				}
			});

			// Prepare ListView in dialog
			title_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
			
			ArrayAdapter<String> adapterTitle = new ArrayAdapter<String>(this,
					R.layout.listlayout, titleList);
			title_ListView.setAdapter(adapterTitle);
			
			title_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					titleText.setText(parent.getItemAtPosition(position).toString());
					removeDialog(2);
				}
			});

			break;
		case 3 :
			dialog = new Dialog(UpdateProfileActivity.this);
			dialog.setContentView(R.layout.specialitylayout);
			dialog.setTitle("Country");

			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);

			dialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					removeDialog(3);
				}
			});

			dialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
				}
			});

			// Prepare ListView in dialog
			title_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
			
			ArrayAdapter<String> adapterCountry = new ArrayAdapter<String>(this,
					R.layout.listlayout, countryList);
			title_ListView.setAdapter(adapterCountry);
			
			title_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					countryText.setText(parent.getItemAtPosition(position).toString());
					String country = parent.getItemAtPosition(position).toString().toString().replace(" ", "%20");
					removeDialog(3);
					cityCheck = false;
					new HttpAsyncTaskCities().execute(AppSettings.WEB_SERVICE_URL
							+ "LoadCityByCountry?Country=" + country);
					cityText.setText("---Select city---");
					locationText.setText("---Select location---");
					

				}
			});

			break;
		case 4 :
			dialog = new Dialog(UpdateProfileActivity.this);
			dialog.setContentView(R.layout.specialitylayout);
			dialog.setTitle("City");

			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);

			dialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					removeDialog(4);
				}
			});

			dialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
				}
			});

			// Prepare ListView in dialog
			title_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
			
			ArrayAdapter<String> adapterCity = new ArrayAdapter<String>(this,
					R.layout.listlayout, cityList);
			title_ListView.setAdapter(adapterCity);
			
			title_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					cityText.setText(parent.getItemAtPosition(position).toString());
					String city = parent.getItemAtPosition(position).toString().replace(" ", "%20");
					locationCheck = false;
					locationText.setText("---Select location---");
					removeDialog(4);
					new HttpAsyncTaskLocations().execute(AppSettings.WEB_SERVICE_URL
							+ "LoadLocationByCity?City=" + city);
					
				}
			});

			break;
		case 5 :
			dialog = new Dialog(UpdateProfileActivity.this);
			dialog.setContentView(R.layout.specialitylayout);
			dialog.setTitle("Location");

			dialog.setCancelable(true);
			dialog.setCanceledOnTouchOutside(true);

			dialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					removeDialog(5);
				}
			});

			dialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//Toast.makeText(FindDoctorActivity.this,"OnDismissListener", Toast.LENGTH_LONG).show();
				}
			});

			// Prepare ListView in dialog
			title_ListView = (ListView) dialog.findViewById(R.id.dialoglist);
			
			ArrayAdapter<String> adapterLocation = new ArrayAdapter<String>(this,
					R.layout.listlayout, locationList);
			title_ListView.setAdapter(adapterLocation);
			
			title_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					locationText.setText(parent.getItemAtPosition(position).toString());
					removeDialog(5);
				}
			});

			break;
		}

		return dialog;
	}
	


	private class HttpAsyncTaskCountries extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
 				JSONParser parser = new JSONParser();
 				String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
				Object obj = parser.parse(finalResult);
				JSONArray array = (JSONArray) obj;
				Gson sGson = new Gson();
				List<Location> tempCountryList = sGson.fromJson(array.toString(), Location.getJsonArrayType());
				countryList = new String[tempCountryList.size()];
				Integer countryCount = 0;
				for(Location location : tempCountryList) {
					countryList[countryCount++] = location.getCountry().replace("&amp;", "&");
				}
				String country = "";
				if(contact.getCountry().length() > 0) {
					country = contact.getCountry();
				} else {
					country = "INDIA";
				}
				countryText.setText(country);
				cityCheck = true;
				new HttpAsyncTaskCities().execute(AppSettings.WEB_SERVICE_URL
						+ "LoadCityByCountry?Country=" + country);
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
		}
	}
	
	private class HttpAsyncTaskCities extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if(!result.contains("Record Not Found")) {
					JSONParser parser = new JSONParser();
	 				String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
					Object obj = parser.parse(finalResult);
					JSONArray array = (JSONArray) obj;
					Gson sGson = new Gson();
					List<Location> tempCityList = sGson.fromJson(array.toString(), Location.getJsonArrayType());
					Integer cityCount = 0;
					for(Location location : tempCityList) {
						if(location.getCity() != null)
							cityCount++; 
					}
					cityList = new String[cityCount];
					cityCount = 0;
					for(Location location : tempCityList) {
						if(location.getCity() != null)
							cityList[cityCount++] = location.getCity().replace("&amp;", "&");
						
					}
					String city = "";
					
					if(contact.getCity().length() > 0 && cityCheck) {
						city = contact.getCity();
						cityText.setText(city);
						locationCheck = true;
						new HttpAsyncTaskLocations().execute(AppSettings.WEB_SERVICE_URL
								+ "LoadLocationByCity?City=" + city);
					} else {
						city = "---Select city---";
					}
					
				} else {
					cityList = new String[0];
				}
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
		}
	}
	
	private class HttpAsyncTaskLocations extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(UpdateProfileActivity.this, "Loading data, Keep patience!");
//            dialog= new ProgressDialog(UpdateProfileActivity.this);
//            //dialog.setIndeterminate(true);
//            //dialog.setIndeterminateDrawable(getResources().getDrawable(R.anim.progress_dialog_anim));
//            dialog.setCancelable(false);
//            dialog.setMessage("Loading data, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if (result.contains("[")) {
					if (!result.contains("Record Not Found")) {
						JSONParser parser = new JSONParser();
						String finalResult = result.substring(result.indexOf('['), result.indexOf(']') + 1);
						Object obj = parser.parse(finalResult);
						JSONArray array = (JSONArray) obj;
						Gson sGson = new Gson();
						List<Location> tempLocationList = sGson.fromJson(array.toString(), Location.getJsonArrayType());
						locationList = new String[tempLocationList.size()];
						Integer locationCount = 0;
						for (Location location : tempLocationList) {
							locationList[locationCount++] = location.getLocation().replace("&amp;", "&");
						}
						String location = "";
						if (contact.getLocation() != null && contact.getLocation().length() > 0 && locationCheck) {
							location = contact.getLocation();
						} else {
							location = "---Select location---";
						}
						locationText.setText(location);
					} else {
						locationList = new String[0];
					}
				}else{
					showToast(result);
				}
			} catch (ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
		}
	}
	
	private class HttpAsyncTaskUpdateProfile extends AsyncTask<String, Void, String> {
		@Override
        protected void onPreExecute() {
			showProgressDialog(UpdateProfileActivity.this, "Updating profile, Keep patience!");
//            dialog= new ProgressDialog(UpdateProfileActivity.this);
//            //dialog.setIndeterminate(true);
//            //dialog.setIndeterminateDrawable(getResources().getDrawable(R.anim.progress_dialog_anim));
//            dialog.setCancelable(false);
//            dialog.setMessage("Updating profile, Keep patience!");
//            dialog.show();
                             
        }
		
		@Override
		protected String doInBackground(String... urls) {

//			return GET(urls[0]);
			return ApiCall.GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			try {
				if(result.contains("Updated Succefully")) {
					Toast.makeText(getApplicationContext(), "Profile updated successfully.", Toast.LENGTH_LONG).show();
					
					DatabaseHandler db = new DatabaseHandler(context);
					contact.setTitle(titleText.getText().toString());
					contact.setName(nameText.getText().toString());
					contact.setAge(ageText.getText().toString());
					contact.setGender(radioSexButton.getText().toString());
					if(addressText.getText().length() > 0) {
						contact.setAddress(addressText.getText().toString());
					} else {
						contact.setAddress("");
					}
					contact.setCountry(countryText.getText().toString());
					contact.setCity(cityText.getText().toString());
					contact.setLocation(locationText.getText().toString());
					db.updateContact(contact);
					
					//adding member to session
					SharedPreferences app_preferences = PreferenceManager
							.getDefaultSharedPreferences(context);

					SharedPreferences.Editor editor = app_preferences.edit();
					editor.putString("member", titleText.getText().toString().trim() + " " + nameText.getText().toString().trim());
					editor.commit();
				} else {
					Toast.makeText(getApplicationContext(), "Error in updating profile.", Toast.LENGTH_LONG).show();
				}
			} catch (Exception pe) {
				System.out.println(pe);
			}
//			dialog.dismiss();
			hideProgressDialog();
			finish();
		}
	}

	
}
