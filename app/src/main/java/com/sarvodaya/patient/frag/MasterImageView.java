package com.sarvodaya.patient.frag;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.sarvodaya.patient.R;
import com.sarvodayahospital.dialog.PopupDialog;
import com.sarvodayahospital.util.L;
import com.sarvodayahospital.util.Utility;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Abhijit on 15-Oct-16.
 */

public class MasterImageView extends CompactFragment implements View.OnClickListener,PopupDialog.DialogTaskListener{
//    ImageLoader imageLoader;
    private String imgUrl;
    private ImageView ivDownload,ivClose,ivShare;
    private ProgressDialog progressDialog;
    private InputStream inputStream;
    private int totalSize;
//    private LinearLayout progressBar;
//    private ImageLoader imageLoader;


    public void setImage(String imgUrl) {
        this.imgUrl = imgUrl;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_master_image_view, container, false);
//        imageLoader=new ImageLoader(getActivity());
        InitUI(v);
        return v;
    } 

    private void InitUI(View v) {
        ImageView imageView=(ImageView)v.findViewById(R.id.iv_image);
        ivDownload=(ImageView)v.findViewById(R.id.iv_download);
        ivShare=(ImageView)v.findViewById(R.id.iv_share);
        ivClose=(ImageView)v.findViewById(R.id.iv_close);
//        progressBar=(LinearLayout)v.findViewById(R.id.ll_progress);

        ivDownload.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        ivClose.setOnClickListener(this);

        Picasso.get().load(imgUrl).into(imageView);
//        imageLoader.DisplayImage(imgUrl,imageView,"blank",false);
         
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_download:
                AssignTask assignTask = new AssignTask(getActivity(),AssignTask.GET_SIZE);
                assignTask.execute(imgUrl,"Image");
                break;
            case R.id.iv_share:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "RWA event image");
                    i.putExtra(Intent.EXTRA_TEXT, imgUrl);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (Exception e) { // e.toString();
                }
                break;
            case R.id.iv_close:
                getFragmentManager().popBackStack();
                break;
        }

    }



    public class AssignTask extends AsyncTask<String, String, String> {

        private Activity activity;
        private static final int MEGABYTE = 1024 * 1024;
        private String fileName="Image";
        public static final int GET_SIZE = 1, DOWNLOAD = 2;
        private int task;
        String fileSize;
        private String url;
		private String ERRORMESSAGE;
        public AssignTask(Activity activity, int task) {
            onAttach(activity);
            this.task = task;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        public void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            switch (task) {
                case GET_SIZE:
                    showPDialog2(activity, "Processing, please wait... ");
                    break;
                case DOWNLOAD:
                    showPDialog(activity, "Downloading file, please wait... ");
                    break;
            }

        }

        @Override
        protected String doInBackground(String... urls) {
            String fileUrl = urls[0];
            this.url=urls[0];

            switch (task) {
                case GET_SIZE:
                    int count;
                    fileName = urls[1];
                    try {

                        URL url = new URL(fileUrl);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        //urlConnection.setRequestMethod("GET");
                        //urlConnection.setDoOutput(true);
                        urlConnection.connect();

                        inputStream = urlConnection.getInputStream();
                        totalSize = urlConnection.getContentLength();
                        fileSize = Utility.bytesIntoHumanReadable(totalSize);
                        if(fileSize!=null){
                            return "success";
                        }

                    } catch (MalformedURLException e) {
                        ERRORMESSAGE = e.toString();
                        return null;
                    } catch (IOException e) {
                        ERRORMESSAGE = e.toString();
                        return null;
                    }
                    break;
                case DOWNLOAD:
                    try {
                        File directory = Utility.getExternalDirectory(Utility.getNameWithTimeStamp(fileName) + ".png");
                        FileOutputStream fileOutputStream = new FileOutputStream(directory);
                        String fileSize = Utility.bytesIntoHumanReadable(totalSize);
                        byte[] buffer = new byte[MEGABYTE];
                        long total = 0;

                        int bufferLength = 0;
                        while ((bufferLength = inputStream.read(buffer)) > 0) {
                            total += bufferLength;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress("" + (int) ((total * 100) / totalSize));


                            fileOutputStream.write(buffer, 0, bufferLength);

                        }
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }

            return "success";
        }

        @Override
        protected void onProgressUpdate(String... progress) {
            super.onProgressUpdate(progress);
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String result) {

            if (activity != null) {
                dismissPDialog();
                ArrayList<String> data;
                if (result != null) {
                    if (result.equals("success")) {
                        switch(task){
                            case GET_SIZE:
                                data=new ArrayList<>();
                                data.add(url);
                                data.add(fileName);
                                PopupDialog.newInstance(GET_SIZE,data,fileName,"Are you sure to download this image \nFile size : "+fileSize,MasterImageView.this)
                                        .show(getFragmentManager(),"popupDialog");
//                                showBaseAlertBox(fileName, , fileName,GET_SIZE,url);
                                break;
                            case DOWNLOAD:
                                data=new ArrayList<>();
                                data.add(url);
                                data.add(fileName);
                                PopupDialog.newInstance(DOWNLOAD,data,"Download Complete","Image stored in Download/rwa folder",MasterImageView.this)
                                        .show(getFragmentManager(),"popupDialog");
//                                showBaseAlertBox("Download Complete", "Image stored in Download/rwa folder", "NA", fileName,DOWNLOAD, url);
                                break;
                        }
                    } else {
                        Toast.makeText(activity, result, Toast.LENGTH_LONG).show();
                        L.m(result);
                    }
                } else {
                    Toast.makeText(activity, ERRORMESSAGE, Toast.LENGTH_LONG).show();
                    L.m(ERRORMESSAGE);
                }
            }
        }
    }

    public void showPDialog(Context context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void showPDialog2(Context context, String msg) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissPDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void onOkClick(DialogFragment dialog, int task, ArrayList<String> data) {
        showBaseAlertBox(task,data.get(0),data.get(1));
    }

    @Override
    public void onCancelClick(DialogFragment dialog, int task, ArrayList<String> data) {

    }


    public void showBaseAlertBox( int task,String fileName, final String url) {

        switch (task) {
            case AssignTask.GET_SIZE:
                AssignTask assignTask = new AssignTask(getActivity(), AssignTask.DOWNLOAD);
                assignTask.execute(url, fileName);
                break;
            case AssignTask.DOWNLOAD:
//                button1.setText("OK");
//                button1.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View arg0) {
//                        dialog.dismiss();
////                        viewPDF(fileName, "", APP);
//                    }
//                });
//                button2.setText("Cancel");
//                button2.setVisibility(View.GONE);
//                button2.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // Close dialog
//                        dialog.dismiss();
//                    }
//                });
                break;
        }
    }

}
