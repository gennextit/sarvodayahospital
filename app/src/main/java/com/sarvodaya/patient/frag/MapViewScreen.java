package com.sarvodaya.patient.frag;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sarvodaya.patient.R;

public class MapViewScreen extends CompactFragment {

    // Google Map
    private MapView mMapView;
    private GoogleMap googleMap;
    private String name,locationName;
    private Double latitude,longitude;

    public static Fragment newInstance(String name, String locationName, Double latitude, Double longitude) {
        MapViewScreen fragment=new MapViewScreen();
        fragment.name=name;
        fragment.locationName=locationName;
        fragment.latitude=latitude;
        fragment.longitude=longitude;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.frag_map_view, container, false);
        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

//        googleMap=mMapView.getMap();

        if (!TextUtils.isEmpty(locationName)) {
            // Getting status
            int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
            // Showing status
            if (status == ConnectionResult.SUCCESS) {
                showLocationOnMap(latitude,longitude,locationName);
            } else {
                Toast.makeText(getActivity(),
                        "This app won't run without Google Map services, which are missing from your phone.",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Location not Available", Toast.LENGTH_SHORT).show();
        }

        return v;
    }

//    public static boolean checkPlayServices(Activity activity) {
//        final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
//        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
//        if (resultCode != ConnectionResult.SUCCESS) {
//            if (apiAvailability.isUserResolvableError(resultCode)) {
//                apiAvailability.getErrorDialog(activity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
//                        .show();
//            } else {
//                Log.e(TAG, "This device is not supported.");
//            }
//            return false;
//        }
//        return true;
//    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        this.googleMap = googleMap;
//    }

    private void showLocationOnMap(Double latitude,Double longitude,String address) {



//        double latitude= Double.parseDouble(lat);
//        double longitude=Double.parseDouble(lng);
        // create marker
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title(address);

//	    // Changing marker icon
//	    marker.icon(BitmapDescriptorFactory
//	            .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

        // adding marker
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude)).zoom(14).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        // Perform any camera updates here

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}