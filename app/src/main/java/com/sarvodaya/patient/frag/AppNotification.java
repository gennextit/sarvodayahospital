package com.sarvodaya.patient.frag;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.sarvodaya.patient.R;
import com.sarvodayahospital.util.AppAnimation;
import com.sarvodayahospital.util.AppUser;
import com.sarvodayahospital.util.CircleTransform;
import com.squareup.picasso.Picasso;

/**
 * Created by Admin on 11/20/2017.
 */

public class AppNotification extends CompactFragment {


    private TextView tvHeading,tvDescription;
    private ImageView ivImage;
    private String title,description,imageUrl;
    private Button btnAction;
    private CheckBox cbNeverShow;

    public static Fragment newInstance(String title, String description, String imageUrl) {
        AppNotification fragment=new AppNotification();
        fragment.title=title;
        fragment.description=description;
        fragment.imageUrl=imageUrl;
        AppAnimation.setFadeAnimation(fragment);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.notification_app,container,false);
        initUi(v);
        updateUi();
        return v;
    }

    private void updateUi() {
        if(!TextUtils.isEmpty(title)){
            tvHeading.setText(title);
        }else{
            tvHeading.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(description)){
            tvDescription.setText(description);
        }else{
            tvDescription.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(imageUrl)){
            Picasso.get().load(imageUrl)
//                    .placeholder(R.mipmap.all_doctors_pic)
//                    .error(R.mipmap.all_doctors_pic)
                    .into(ivImage);
        }
    }


    private void initUi(View v) {
        btnAction=(Button)v.findViewById(R.id.btn_action);
        cbNeverShow=(CheckBox)v.findViewById(R.id.cb_never_show);
        tvHeading=(TextView)v.findViewById(R.id.tv_header);
        tvDescription=(TextView)v.findViewById(R.id.tv_description);
        ivImage=(ImageView)v.findViewById(R.id.iv_image);

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbNeverShow.isChecked()){
                    AppUser.setNeverShowAgain(getContext(),title);
                }
                getActivity().onBackPressed();
            }
        });
    }

    public static boolean checkNeverShowAgain(Context context,String titleNoti) {
        if(AppUser.getNeverShowAgain(context).equals(titleNoti)){
            return false;
        }else{
            return true;
        }
    }
}
